/*
 * jUQuery UI Backport - Unified jQuery 1.2.1 and jQueryUI 1.0, namespaced for inclusion in Drupal 5
 * figured out with help from Scott Gonzo 
 * http://groups.google.com/group/jquery-ui/browse_thread/thread/a484731cae8e7385/966d6a7c0d3c1c50#966d6a7c0d3c1c50
 * http://dev.jquery.com/attachment/ticket/1774/1774.diff
 */
 
(function(){
/*
 * jQuery 1.2.1 - New Wave Javascript
 *
 * Copyright (c) 2007 John Resig (jquery.com)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * $Date$
 * $Rev: 3353 $
 */

// Map over jQuery in case of overwrite
if ( typeof window.jQuery != "undefined" ) {
  var _jQuery = window.jQuery;
}
var jQuery = window.jQuery = function(selector, context) {
  // If the context is a namespace object, return a new object
  return this instanceof jQuery ?
    this.init(selector, context) :
    new jQuery(selector, context);
};

// Map over the $ in case of overwrite
if ( typeof $ != "undefined" )
  var _$ = $;
  
// Map the jQuery namespace to the '$' one
window.$ = jQuery;

var quickExpr = /^[^<]*(<(.|\s)+>)[^>]*$|^#(\w+)$/;

jQuery.fn = jQuery.prototype = {
  init: function(selector, context) {
    // Make sure that a selection was provided
    selector = selector || document;

    // Handle HTML strings
    if ( typeof selector  == "string" ) {
      var m = quickExpr.exec(selector);
      if ( m && (m[1] || !context) ) {
        // HANDLE: $(html) -> $(array)
        if ( m[1] )
          selector = jQuery.clean( [ m[1] ], context );

        // HANDLE: $("#id")
        else {
          var tmp = document.getElementById( m[3] );
          if ( tmp )
            // Handle the case where IE and Opera return items
            // by name instead of ID
            if ( tmp.id != m[3] )
              return jQuery().find( selector );
            else {
              this[0] = tmp;
              this.length = 1;
              return this;
            }
          else
            selector = [];
        }

      // HANDLE: $(expr)
      } else
        return new jQuery( context ).find( selector );

    // HANDLE: $(function)
    // Shortcut for document ready
    } else if ( jQuery.isFunction(selector) )
      return new jQuery(document)[ jQuery.fn.ready ? "ready" : "load" ]( selector );

    return this.setArray(
      // HANDLE: $(array)
      selector.constructor == Array && selector ||

      // HANDLE: $(arraylike)
      // Watch for when an array-like object is passed as the selector
      (selector.jquery || selector.length && selector != window && !selector.nodeType && selector[0] != undefined && selector[0].nodeType) && jQuery.makeArray( selector ) ||

      // HANDLE: $(*)
      [ selector ] );
  },
  
  jquery: "1.2.1",

  size: function() {
    return this.length;
  },
  
  length: 0,

  get: function( num ) {
    return num == undefined ?

      // Return a 'clean' array
      jQuery.makeArray( this ) :

      // Return just the object
      this[num];
  },
  
  pushStack: function( a ) {
    var ret = jQuery(a);
    ret.prevObject = this;
    return ret;
  },
  
  setArray: function( a ) {
    this.length = 0;
    Array.prototype.push.apply( this, a );
    return this;
  },

  each: function( fn, args ) {
    return jQuery.each( this, fn, args );
  },

  index: function( obj ) {
    var pos = -1;
    this.each(function(i){
      if ( this == obj ) pos = i;
    });
    return pos;
  },

  attr: function( key, value, type ) {
    var obj = key;
    
    // Look for the case where we're accessing a style value
    if ( key.constructor == String )
      if ( value == undefined )
        return this.length && jQuery[ type || "attr" ]( this[0], key ) || undefined;
      else {
        obj = {};
        obj[ key ] = value;
      }
    
    // Check to see if we're setting style values
    return this.each(function(index){
      // Set all the styles
      for ( var prop in obj )
        jQuery.attr(
          type ? this.style : this,
          prop, jQuery.prop(this, obj[prop], type, index, prop)
        );
    });
  },

  css: function( key, value ) {
    return this.attr( key, value, "curCSS" );
  },

  text: function(e) {
    if ( typeof e != "object" && e != null )
      return this.empty().append( document.createTextNode( e ) );

    var t = "";
    jQuery.each( e || this, function(){
      jQuery.each( this.childNodes, function(){
        if ( this.nodeType != 8 )
          t += this.nodeType != 1 ?
            this.nodeValue : jQuery.fn.text([ this ]);
      });
    });
    return t;
  },

  wrapAll: function(html) {
    if ( this[0] )
      // The elements to wrap the target around
      jQuery(html, this[0].ownerDocument)
        .clone()
        .insertBefore(this[0])
        .map(function(){
          var elem = this;
          while ( elem.firstChild )
            elem = elem.firstChild;
          return elem;
        })
        .append(this);

    return this;
  },

  wrapInner: function(html) {
    return this.each(function(){
      jQuery(this).contents().wrapAll(html);
    });
  },

  wrap: function(html) {
    return this.each(function(){
      jQuery(this).wrapAll(html);
    });
  },

  append: function() {
    return this.domManip(arguments, true, 1, function(a){
      this.appendChild( a );
    });
  },

  prepend: function() {
    return this.domManip(arguments, true, -1, function(a){
      this.insertBefore( a, this.firstChild );
    });
  },
  
  before: function() {
    return this.domManip(arguments, false, 1, function(a){
      this.parentNode.insertBefore( a, this );
    });
  },

  after: function() {
    return this.domManip(arguments, false, -1, function(a){
      this.parentNode.insertBefore( a, this.nextSibling );
    });
  },

  end: function() {
    return this.prevObject || jQuery([]);
  },

  find: function(t) {
    var data = jQuery.map(this, function(a){ return jQuery.find(t,a); });
    return this.pushStack( /[^+>] [^+>]/.test( t ) || t.indexOf("..") > -1 ?
      jQuery.unique( data ) : data );
  },

  clone: function(events) {
    // Do the clone
    var ret = this.map(function(){
      return this.outerHTML ? jQuery(this.outerHTML)[0] : this.cloneNode(true);
    });

    // Need to set the expando to null on the cloned set if it exists
    // removeData doesn't work here, IE removes it from the original as well
    // this is primarily for IE but the data expando shouldn't be copied over in any browser
    var clone = ret.find("*").andSelf().each(function(){
      if ( this[ expando ] != undefined )
        this[ expando ] = null;
    });
    
    // Copy the events from the original to the clone
    if (events === true)
      this.find("*").andSelf().each(function(i) {
        var events = jQuery.data(this, "events");
        for ( var type in events )
          for ( var handler in events[type] )
            jQuery.event.add(clone[i], type, events[type][handler], events[type][handler].data);
      });

    // Return the cloned set
    return ret;
  },

  filter: function(t) {
    return this.pushStack(
      jQuery.isFunction( t ) &&
      jQuery.grep(this, function(el, index){
        return t.apply(el, [index]);
      }) ||

      jQuery.multiFilter(t,this) );
  },

  not: function(t) {
    return this.pushStack(
      t.constructor == String &&
      jQuery.multiFilter(t, this, true) ||

      jQuery.grep(this, function(a) {
        return ( t.constructor == Array || t.jquery )
          ? jQuery.inArray( a, t ) < 0
          : a != t;
      })
    );
  },

  add: function(t) {
    return this.pushStack( jQuery.merge(
      this.get(),
      t.constructor == String ?
        jQuery(t).get() :
        t.length != undefined && (!t.nodeName || jQuery.nodeName(t, "form")) ?
          t : [t] )
    );
  },

  is: function(expr) {
    return expr ? jQuery.multiFilter(expr,this).length > 0 : false;
  },

  hasClass: function(expr) {
    return this.is("." + expr);
  },
  
  val: function( val ) {
    if ( val == undefined ) {
      if ( this.length ) {
        var elem = this[0];
          
        // We need to handle select boxes special
        if ( jQuery.nodeName(elem, "select") ) {
          var index = elem.selectedIndex,
            a = [],
            options = elem.options,
            one = elem.type == "select-one";
          
          // Nothing was selected
          if ( index < 0 )
            return null;

          // Loop through all the selected options
          for ( var i = one ? index : 0, max = one ? index + 1 : options.length; i < max; i++ ) {
            var option = options[i];
            if ( option.selected ) {
              // Get the specifc value for the option
              var val = jQuery.browser.msie && !option.attributes["value"].specified ? option.text : option.value;
              
              // We don't need an array for one selects
              if ( one )
                return val;
              
              // Multi-Selects return an array
              a.push(val);
            }
          }
          
          return a;
          
        // Everything else, we just grab the value
        } else
          return this[0].value.replace(/\r/g, "");
      }
    } else
      return this.each(function(){
        if ( val.constructor == Array && /radio|checkbox/.test(this.type) )
          this.checked = (jQuery.inArray(this.value, val) >= 0 ||
            jQuery.inArray(this.name, val) >= 0);
        else if ( jQuery.nodeName(this, "select") ) {
          var tmp = val.constructor == Array ? val : [val];

          jQuery("option", this).each(function(){
            this.selected = (jQuery.inArray(this.value, tmp) >= 0 ||
            jQuery.inArray(this.text, tmp) >= 0);
          });

          if ( !tmp.length )
            this.selectedIndex = -1;
        } else
          this.value = val;
      });
  },
  
  html: function( val ) {
    return val == undefined ?
      ( this.length ? this[0].innerHTML : null ) :
      this.empty().append( val );
  },

  replaceWith: function( val ) {
    return this.after( val ).remove();
  },

  eq: function(i){
    return this.slice(i, i+1);
  },

  slice: function() {
    return this.pushStack( Array.prototype.slice.apply( this, arguments ) );
  },

  map: function(fn) {
    return this.pushStack(jQuery.map( this, function(elem,i){
      return fn.call( elem, i, elem );
    }));
  },

  andSelf: function() {
    return this.add( this.prevObject );
  },
  
  domManip: function(args, table, dir, fn) {
    var clone = this.length > 1, a; 

    return this.each(function(){
      if ( !a ) {
        a = jQuery.clean(args, this.ownerDocument);
        if ( dir < 0 )
          a.reverse();
      }

      var obj = this;

      if ( table && jQuery.nodeName(this, "table") && jQuery.nodeName(a[0], "tr") )
        obj = this.getElementsByTagName("tbody")[0] || this.appendChild(document.createElement("tbody"));

      jQuery.each( a, function(){
        var elem = clone ? this.cloneNode(true) : this;
        if ( !evalScript(0, elem) )
          fn.call( obj, elem );
      });
    });
  }
};

function evalScript(i, elem){
  var script = jQuery.nodeName(elem, "script");

  if ( script ) {
    if ( elem.src )
      jQuery.ajax({ url: elem.src, async: false, dataType: "script" });
    else
      jQuery.globalEval( elem.text || elem.textContent || elem.innerHTML || "" );
  
    if ( elem.parentNode )
      elem.parentNode.removeChild(elem);

  } else if ( elem.nodeType == 1 )
    jQuery("script", elem).each(evalScript);

  return script;
}

jQuery.extend = jQuery.fn.extend = function() {
  // copy reference to target object
  var target = arguments[0] || {}, a = 1, al = arguments.length, deep = false;

  // Handle a deep copy situation
  if ( target.constructor == Boolean ) {
    deep = target;
    target = arguments[1] || {};
  }

  // extend jQuery itself if only one argument is passed
  if ( al == 1 ) {
    target = this;
    a = 0;
  }

  var prop;

  for ( ; a < al; a++ )
    // Only deal with non-null/undefined values
    if ( (prop = arguments[a]) != null )
      // Extend the base object
      for ( var i in prop ) {
        // Prevent never-ending loop
        if ( target == prop[i] )
          continue;

        // Recurse if we're merging object values
        if ( deep && typeof prop[i] == 'object' && target[i] )
          jQuery.extend( target[i], prop[i] );

        // Don't bring in undefined values
        else if ( prop[i] != undefined )
          target[i] = prop[i];
      }

  // Return the modified object
  return target;
};

var expando = "jQuery" + (new Date()).getTime(), uuid = 0, win = {};

jQuery.extend({
  noConflict: function(deep) {
    window.$ = _$;
    if ( deep )
      window.jQuery = _jQuery;
    return jQuery;
  },

  // This may seem like some crazy code, but trust me when I say that this
  // is the only cross-browser way to do this. --John
  isFunction: function( fn ) {
    return !!fn && typeof fn != "string" && !fn.nodeName && 
      fn.constructor != Array && /function/i.test( fn + "" );
  },
  
  // check if an element is in a XML document
  isXMLDoc: function(elem) {
    return elem.documentElement && !elem.body ||
      elem.tagName && elem.ownerDocument && !elem.ownerDocument.body;
  },

  // Evalulates a script in a global context
  // Evaluates Async. in Safari 2 :-(
  globalEval: function( data ) {
    data = jQuery.trim( data );
    if ( data ) {
      if ( window.execScript )
        window.execScript( data );
      else if ( jQuery.browser.safari )
        // safari doesn't provide a synchronous global eval
        window.setTimeout( data, 0 );
      else
        eval.call( window, data );
    }
  },

  nodeName: function( elem, name ) {
    return elem.nodeName && elem.nodeName.toUpperCase() == name.toUpperCase();
  },
  
  cache: {},
  
  data: function( elem, name, data ) {
    elem = elem == window ? win : elem;

    var id = elem[ expando ];

    // Compute a unique ID for the element
    if ( !id ) 
      id = elem[ expando ] = ++uuid;

    // Only generate the data cache if we're
    // trying to access or manipulate it
    if ( name && !jQuery.cache[ id ] )
      jQuery.cache[ id ] = {};
    
    // Prevent overriding the named cache with undefined values
    if ( data != undefined )
      jQuery.cache[ id ][ name ] = data;
    
    // Return the named cache data, or the ID for the element 
    return name ? jQuery.cache[ id ][ name ] : id;
  },
  
  removeData: function( elem, name ) {
    elem = elem == window ? win : elem;

    var id = elem[ expando ];

    // If we want to remove a specific section of the element's data
    if ( name ) {
      if ( jQuery.cache[ id ] ) {
        // Remove the section of cache data
        delete jQuery.cache[ id ][ name ];

        // If we've removed all the data, remove the element's cache
        name = "";
        for ( name in jQuery.cache[ id ] ) break;
        if ( !name )
          jQuery.removeData( elem );
      }

    // Otherwise, we want to remove all of the element's data
    } else {
      // Clean up the element expando
      try {
        delete elem[ expando ];
      } catch(e){
        // IE has trouble directly removing the expando
        // but it's ok with using removeAttribute
        if ( elem.removeAttribute )
          elem.removeAttribute( expando );
      }

      // Completely remove the data cache
      delete jQuery.cache[ id ];
    }
  },

  // args is for internal usage only
  each: function( obj, fn, args ) {
    if ( args ) {
      if ( obj.length == undefined )
        for ( var i in obj )
          fn.apply( obj[i], args );
      else
        for ( var i = 0, ol = obj.length; i < ol; i++ )
          if ( fn.apply( obj[i], args ) === false ) break;

    // A special, fast, case for the most common use of each
    } else {
      if ( obj.length == undefined )
        for ( var i in obj )
          fn.call( obj[i], i, obj[i] );
      else
        for ( var i = 0, ol = obj.length, val = obj[0]; 
          i < ol && fn.call(val,i,val) !== false; val = obj[++i] ){}
    }

    return obj;
  },
  
  prop: function(elem, value, type, index, prop){
      // Handle executable functions
      if ( jQuery.isFunction( value ) )
        value = value.call( elem, [index] );
        
      // exclude the following css properties to add px
      var exclude = /z-?index|font-?weight|opacity|zoom|line-?height/i;

      // Handle passing in a number to a CSS property
      return value && value.constructor == Number && type == "curCSS" && !exclude.test(prop) ?
        value + "px" :
        value;
  },

  className: {
    // internal only, use addClass("class")
    add: function( elem, c ){
      jQuery.each( (c || "").split(/\s+/), function(i, cur){
        if ( !jQuery.className.has( elem.className, cur ) )
          elem.className += ( elem.className ? " " : "" ) + cur;
      });
    },

    // internal only, use removeClass("class")
    remove: function( elem, c ){
      elem.className = c != undefined ?
        jQuery.grep( elem.className.split(/\s+/), function(cur){
          return !jQuery.className.has( c, cur ); 
        }).join(" ") : "";
    },

    // internal only, use is(".class")
    has: function( t, c ) {
      return jQuery.inArray( c, (t.className || t).toString().split(/\s+/) ) > -1;
    }
  },

  swap: function(e,o,f) {
    for ( var i in o ) {
      e.style["old"+i] = e.style[i];
      e.style[i] = o[i];
    }
    f.apply( e, [] );
    for ( var i in o )
      e.style[i] = e.style["old"+i];
  },

  css: function(e,p) {
    if ( p == "height" || p == "width" ) {
      var old = {}, oHeight, oWidth, d = ["Top","Bottom","Right","Left"];

      jQuery.each( d, function(){
        old["padding" + this] = 0;
        old["border" + this + "Width"] = 0;
      });

      jQuery.swap( e, old, function() {
        if ( jQuery(e).is(':visible') ) {
          oHeight = e.offsetHeight;
          oWidth = e.offsetWidth;
        } else {
          e = jQuery(e.cloneNode(true))
            .find(":radio").removeAttr("checked").end()
            .css({
              visibility: "hidden", position: "absolute", display: "block", right: "0", left: "0"
            }).appendTo(e.parentNode)[0];

          var parPos = jQuery.css(e.parentNode,"position") || "static";
          if ( parPos == "static" )
            e.parentNode.style.position = "relative";

          oHeight = e.clientHeight;
          oWidth = e.clientWidth;

          if ( parPos == "static" )
            e.parentNode.style.position = "static";

          e.parentNode.removeChild(e);
        }
      });

      return p == "height" ? oHeight : oWidth;
    }

    return jQuery.curCSS( e, p );
  },

  curCSS: function(elem, prop, force) {
    var ret, stack = [], swap = [];

    // A helper method for determining if an element's values are broken
    function color(a){
      if ( !jQuery.browser.safari )
        return false;

      var ret = document.defaultView.getComputedStyle(a,null);
      return !ret || ret.getPropertyValue("color") == "";
    }

    if (prop == "opacity" && jQuery.browser.msie) {
      ret = jQuery.attr(elem.style, "opacity");
      return ret == "" ? "1" : ret;
    }
    
    if (prop.match(/float/i))
      prop = styleFloat;

    if (!force && elem.style[prop])
      ret = elem.style[prop];

    else if (document.defaultView && document.defaultView.getComputedStyle) {

      if (prop.match(/float/i))
        prop = "float";

      prop = prop.replace(/([A-Z])/g,"-$1").toLowerCase();
      var cur = document.defaultView.getComputedStyle(elem, null);

      if ( cur && !color(elem) )
        ret = cur.getPropertyValue(prop);

      // If the element isn't reporting its values properly in Safari
      // then some display: none elements are involved
      else {
        // Locate all of the parent display: none elements
        for ( var a = elem; a && color(a); a = a.parentNode )
          stack.unshift(a);

        // Go through and make them visible, but in reverse
        // (It would be better if we knew the exact display type that they had)
        for ( a = 0; a < stack.length; a++ )
          if ( color(stack[a]) ) {
            swap[a] = stack[a].style.display;
            stack[a].style.display = "block";
          }

        // Since we flip the display style, we have to handle that
        // one special, otherwise get the value
        ret = prop == "display" && swap[stack.length-1] != null ?
          "none" :
          document.defaultView.getComputedStyle(elem,null).getPropertyValue(prop) || "";

        // Finally, revert the display styles back
        for ( a = 0; a < swap.length; a++ )
          if ( swap[a] != null )
            stack[a].style.display = swap[a];
      }

      if ( prop == "opacity" && ret == "" )
        ret = "1";

    } else if (elem.currentStyle) {
      var newProp = prop.replace(/\-(\w)/g,function(m,c){return c.toUpperCase();});
      ret = elem.currentStyle[prop] || elem.currentStyle[newProp];

      // From the awesome hack by Dean Edwards
      // http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

      // If we're not dealing with a regular pixel number
      // but a number that has a weird ending, we need to convert it to pixels
      if ( !/^\d+(px)?$/i.test(ret) && /^\d/.test(ret) ) {
        var style = elem.style.left;
        var runtimeStyle = elem.runtimeStyle.left;
        elem.runtimeStyle.left = elem.currentStyle.left;
        elem.style.left = ret || 0;
        ret = elem.style.pixelLeft + "px";
        elem.style.left = style;
        elem.runtimeStyle.left = runtimeStyle;
      }
    }

    return ret;
  },
  
  clean: function(a, doc) {
    var r = [];
    doc = doc || document;

    jQuery.each( a, function(i,arg){
      if ( !arg ) return;

      if ( arg.constructor == Number )
        arg = arg.toString();
      
      // Convert html string into DOM nodes
      if ( typeof arg == "string" ) {
        // Fix "XHTML"-style tags in all browsers
        arg = arg.replace(/(<(\w+)[^>]*?)\/>/g, function(m, all, tag){
          return tag.match(/^(abbr|br|col|img|input|link|meta|param|hr|area)$/i)? m : all+"></"+tag+">";
        });

        // Trim whitespace, otherwise indexOf won't work as expected
        var s = jQuery.trim(arg).toLowerCase(), div = doc.createElement("div"), tb = [];

        var wrap =
          // option or optgroup
          !s.indexOf("<opt") &&
          [1, "<select>", "</select>"] ||
          
          !s.indexOf("<leg") &&
          [1, "<fieldset>", "</fieldset>"] ||
          
          s.match(/^<(thead|tbody|tfoot|colg|cap)/) &&
          [1, "<table>", "</table>"] ||
          
          !s.indexOf("<tr") &&
          [2, "<table><tbody>", "</tbody></table>"] ||
          
          // <thead> matched above
          (!s.indexOf("<td") || !s.indexOf("<th")) &&
          [3, "<table><tbody><tr>", "</tr></tbody></table>"] ||
          
          !s.indexOf("<col") &&
          [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"] ||

          // IE can't serialize <link> and <script> tags normally
          jQuery.browser.msie &&
          [1, "div<div>", "</div>"] ||
          
          [0,"",""];

        // Go to html and back, then peel off extra wrappers
        div.innerHTML = wrap[1] + arg + wrap[2];
        
        // Move to the right depth
        while ( wrap[0]-- )
          div = div.lastChild;
        
        // Remove IE's autoinserted <tbody> from table fragments
        if ( jQuery.browser.msie ) {
          
          // String was a <table>, *may* have spurious <tbody>
          if ( !s.indexOf("<table") && s.indexOf("<tbody") < 0 ) 
            tb = div.firstChild && div.firstChild.childNodes;
            
          // String was a bare <thead> or <tfoot>
          else if ( wrap[1] == "<table>" && s.indexOf("<tbody") < 0 )
            tb = div.childNodes;

          for ( var n = tb.length-1; n >= 0 ; --n )
            if ( jQuery.nodeName(tb[n], "tbody") && !tb[n].childNodes.length )
              tb[n].parentNode.removeChild(tb[n]);
  
          // IE completely kills leading whitespace when innerHTML is used  
          if ( /^\s/.test(arg) )  
            div.insertBefore( doc.createTextNode( arg.match(/^\s*/)[0] ), div.firstChild );

        }
        
        arg = jQuery.makeArray( div.childNodes );
      }

      if ( 0 === arg.length && (!jQuery.nodeName(arg, "form") && !jQuery.nodeName(arg, "select")) )
        return;

      if ( arg[0] == undefined || jQuery.nodeName(arg, "form") || arg.options )
        r.push( arg );
      else
        r = jQuery.merge( r, arg );

    });

    return r;
  },
  
  attr: function(elem, name, value){
    var fix = jQuery.isXMLDoc(elem) ? {} : jQuery.props;

    // Safari mis-reports the default selected property of a hidden option
    // Accessing the parent's selectedIndex property fixes it
    if ( name == "selected" && jQuery.browser.safari )
      elem.parentNode.selectedIndex;
    
    // Certain attributes only work when accessed via the old DOM 0 way
    if ( fix[name] ) {
      if ( value != undefined ) elem[fix[name]] = value;
      return elem[fix[name]];
    } else if ( jQuery.browser.msie && name == "style" )
      return jQuery.attr( elem.style, "cssText", value );

    else if ( value == undefined && jQuery.browser.msie && jQuery.nodeName(elem, "form") && (name == "action" || name == "method") )
      return elem.getAttributeNode(name).nodeValue;

    // IE elem.getAttribute passes even for style
    else if ( elem.tagName ) {

      if ( value != undefined ) {
        if ( name == "type" && jQuery.nodeName(elem,"input") && elem.parentNode )
          throw "type property can't be changed";
        elem.setAttribute( name, value );
      }

      if ( jQuery.browser.msie && /href|src/.test(name) && !jQuery.isXMLDoc(elem) ) 
        return elem.getAttribute( name, 2 );

      return elem.getAttribute( name );

    // elem is actually elem.style ... set the style
    } else {
      // IE actually uses filters for opacity
      if ( name == "opacity" && jQuery.browser.msie ) {
        if ( value != undefined ) {
          // IE has trouble with opacity if it does not have layout
          // Force it by setting the zoom level
          elem.zoom = 1; 
  
          // Set the alpha filter to set the opacity
          elem.filter = (elem.filter || "").replace(/alpha\([^)]*\)/,"") +
            (parseFloat(value).toString() == "NaN" ? "" : "alpha(opacity=" + value * 100 + ")");
        }
  
        return elem.filter ? 
          (parseFloat( elem.filter.match(/opacity=([^)]*)/)[1] ) / 100).toString() : "";
      }
      name = name.replace(/-([a-z])/ig,function(z,b){return b.toUpperCase();});
      if ( value != undefined ) elem[name] = value;
      return elem[name];
    }
  },
  
  trim: function(t){
    return (t||"").replace(/^\s+|\s+$/g, "");
  },

  makeArray: function( a ) {
    var r = [];

    // Need to use typeof to fight Safari childNodes crashes
    if ( typeof a != "array" )
      for ( var i = 0, al = a.length; i < al; i++ )
        r.push( a[i] );
    else
      r = a.slice( 0 );

    return r;
  },

  inArray: function( b, a ) {
    for ( var i = 0, al = a.length; i < al; i++ )
      if ( a[i] == b )
        return i;
    return -1;
  },

  merge: function(first, second) {
    // We have to loop this way because IE & Opera overwrite the length
    // expando of getElementsByTagName

    // Also, we need to make sure that the correct elements are being returned
    // (IE returns comment nodes in a '*' query)
    if ( jQuery.browser.msie ) {
      for ( var i = 0; second[i]; i++ )
        if ( second[i].nodeType != 8 )
          first.push(second[i]);
    } else
      for ( var i = 0; second[i]; i++ )
        first.push(second[i]);

    return first;
  },

  unique: function(first) {
    var r = [], done = {};

    try {
      for ( var i = 0, fl = first.length; i < fl; i++ ) {
        var id = jQuery.data(first[i]);
        if ( !done[id] ) {
          done[id] = true;
          r.push(first[i]);
        }
      }
    } catch(e) {
      r = first;
    }

    return r;
  },

  grep: function(elems, fn, inv) {
    // If a string is passed in for the function, make a function
    // for it (a handy shortcut)
    if ( typeof fn == "string" )
      fn = eval("false||function(a,i){return " + fn + "}");

    var result = [];

    // Go through the array, only saving the items
    // that pass the validator function
    for ( var i = 0, el = elems.length; i < el; i++ )
      if ( !inv && fn(elems[i],i) || inv && !fn(elems[i],i) )
        result.push( elems[i] );

    return result;
  },

  map: function(elems, fn) {
    // If a string is passed in for the function, make a function
    // for it (a handy shortcut)
    if ( typeof fn == "string" )
      fn = eval("false||function(a){return " + fn + "}");

    var result = [];

    // Go through the array, translating each of the items to their
    // new value (or values).
    for ( var i = 0, el = elems.length; i < el; i++ ) {
      var val = fn(elems[i],i);

      if ( val !== null && val != undefined ) {
        if ( val.constructor != Array ) val = [val];
        result = result.concat( val );
      }
    }

    return result;
  }
});

var userAgent = navigator.userAgent.toLowerCase();

// Figure out what browser is being used
jQuery.browser = {
  version: (userAgent.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1],
  safari: /webkit/.test(userAgent),
  opera: /opera/.test(userAgent),
  msie: /msie/.test(userAgent) && !/opera/.test(userAgent),
  mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent)
};

var styleFloat = jQuery.browser.msie ? "styleFloat" : "cssFloat";
  
jQuery.extend({
  // Check to see if the W3C box model is being used
  boxModel: !jQuery.browser.msie || document.compatMode == "CSS1Compat",
  
  styleFloat: jQuery.browser.msie ? "styleFloat" : "cssFloat",
  
  props: {
    "for": "htmlFor",
    "class": "className",
    "float": styleFloat,
    cssFloat: styleFloat,
    styleFloat: styleFloat,
    innerHTML: "innerHTML",
    className: "className",
    value: "value",
    disabled: "disabled",
    checked: "checked",
    readonly: "readOnly",
    selected: "selected",
    maxlength: "maxLength"
  }
});

jQuery.each({
  parent: "a.parentNode",
  parents: "jQuery.dir(a,'parentNode')",
  next: "jQuery.nth(a,2,'nextSibling')",
  prev: "jQuery.nth(a,2,'previousSibling')",
  nextAll: "jQuery.dir(a,'nextSibling')",
  prevAll: "jQuery.dir(a,'previousSibling')",
  siblings: "jQuery.sibling(a.parentNode.firstChild,a)",
  children: "jQuery.sibling(a.firstChild)",
  contents: "jQuery.nodeName(a,'iframe')?a.contentDocument||a.contentWindow.document:jQuery.makeArray(a.childNodes)"
}, function(i,n){
  jQuery.fn[ i ] = function(a) {
    var ret = jQuery.map(this,n);
    if ( a && typeof a == "string" )
      ret = jQuery.multiFilter(a,ret);
    return this.pushStack( jQuery.unique(ret) );
  };
});

jQuery.each({
  appendTo: "append",
  prependTo: "prepend",
  insertBefore: "before",
  insertAfter: "after",
  replaceAll: "replaceWith"
}, function(i,n){
  jQuery.fn[ i ] = function(){
    var a = arguments;
    return this.each(function(){
      for ( var j = 0, al = a.length; j < al; j++ )
        jQuery(a[j])[n]( this );
    });
  };
});

jQuery.each( {
  removeAttr: function( key ) {
    jQuery.attr( this, key, "" );
    this.removeAttribute( key );
  },
  addClass: function(c){
    jQuery.className.add(this,c);
  },
  removeClass: function(c){
    jQuery.className.remove(this,c);
  },
  toggleClass: function( c ){
    jQuery.className[ jQuery.className.has(this,c) ? "remove" : "add" ](this, c);
  },
  remove: function(a){
    if ( !a || jQuery.filter( a, [this] ).r.length ) {
      jQuery.removeData( this );
      this.parentNode.removeChild( this );
    }
  },
  empty: function() {
    // Clean up the cache
    jQuery("*", this).each(function(){ jQuery.removeData(this); });

    while ( this.firstChild )
      this.removeChild( this.firstChild );
  }
}, function(i,n){
  jQuery.fn[ i ] = function() {
    return this.each( n, arguments );
  };
});

jQuery.each( [ "Height", "Width" ], function(i,name){
  var n = name.toLowerCase();
  
  jQuery.fn[ n ] = function(h) {
    return this[0] == window ?
      jQuery.browser.safari && self["inner" + name] ||
      jQuery.boxModel && Math.max(document.documentElement["client" + name], document.body["client" + name]) ||
      document.body["client" + name] :
    
      this[0] == document ?
        Math.max( document.body["scroll" + name], document.body["offset" + name] ) :
        
        h == undefined ?
          ( this.length ? jQuery.css( this[0], n ) : null ) :
          this.css( n, h.constructor == String ? h : h + "px" );
  };
});

var chars = jQuery.browser.safari && parseInt(jQuery.browser.version) < 417 ?
    "(?:[\\w*_-]|\\\\.)" :
    "(?:[\\w\u0128-\uFFFF*_-]|\\\\.)",
  quickChild = new RegExp("^>\\s*(" + chars + "+)"),
  quickID = new RegExp("^(" + chars + "+)(#)(" + chars + "+)"),
  quickClass = new RegExp("^([#.]?)(" + chars + "*)");

jQuery.extend({
  expr: {
    "": "m[2]=='*'||jQuery.nodeName(a,m[2])",
    "#": "a.getAttribute('id')==m[2]",
    ":": {
      // Position Checks
      lt: "i<m[3]-0",
      gt: "i>m[3]-0",
      nth: "m[3]-0==i",
      eq: "m[3]-0==i",
      first: "i==0",
      last: "i==r.length-1",
      even: "i%2==0",
      odd: "i%2",

      // Child Checks
      "first-child": "a.parentNode.getElementsByTagName('*')[0]==a",
      "last-child": "jQuery.nth(a.parentNode.lastChild,1,'previousSibling')==a",
      "only-child": "!jQuery.nth(a.parentNode.lastChild,2,'previousSibling')",

      // Parent Checks
      parent: "a.firstChild",
      empty: "!a.firstChild",

      // Text Check
      contains: "(a.textContent||a.innerText||jQuery(a).text()||'').indexOf(m[3])>=0",

      // Visibility
      visible: '"hidden"!=a.type&&jQuery.css(a,"display")!="none"&&jQuery.css(a,"visibility")!="hidden"',
      hidden: '"hidden"==a.type||jQuery.css(a,"display")=="none"||jQuery.css(a,"visibility")=="hidden"',

      // Form attributes
      enabled: "!a.disabled",
      disabled: "a.disabled",
      checked: "a.checked",
      selected: "a.selected||jQuery.attr(a,'selected')",

      // Form elements
      text: "'text'==a.type",
      radio: "'radio'==a.type",
      checkbox: "'checkbox'==a.type",
      file: "'file'==a.type",
      password: "'password'==a.type",
      submit: "'submit'==a.type",
      image: "'image'==a.type",
      reset: "'reset'==a.type",
      button: '"button"==a.type||jQuery.nodeName(a,"button")',
      input: "/input|select|textarea|button/i.test(a.nodeName)",

      // :has()
      has: "jQuery.find(m[3],a).length",

      // :header
      header: "/h\\d/i.test(a.nodeName)",

      // :animated
      animated: "jQuery.grep(jQuery.timers,function(fn){return a==fn.elem;}).length"
    }
  },
  
  // The regular expressions that power the parsing engine
  parse: [
    // Match: [@value='test'], [@foo]
    /^(\[) *@?([\w-]+) *([!*$^~=]*) *('?"?)(.*?)\4 *\]/,

    // Match: :contains('foo')
    /^(:)([\w-]+)\("?'?(.*?(\(.*?\))?[^(]*?)"?'?\)/,

    // Match: :even, :last-chlid, #id, .class
    new RegExp("^([:.#]*)(" + chars + "+)")
  ],

  multiFilter: function( expr, elems, not ) {
    var old, cur = [];

    while ( expr && expr != old ) {
      old = expr;
      var f = jQuery.filter( expr, elems, not );
      expr = f.t.replace(/^\s*,\s*/, "" );
      cur = not ? elems = f.r : jQuery.merge( cur, f.r );
    }

    return cur;
  },

  find: function( t, context ) {
    // Quickly handle non-string expressions
    if ( typeof t != "string" )
      return [ t ];

    // Make sure that the context is a DOM Element
    if ( context && !context.nodeType )
      context = null;

    // Set the correct context (if none is provided)
    context = context || document;

    // Initialize the search
    var ret = [context], done = [], last;

    // Continue while a selector expression exists, and while
    // we're no longer looping upon ourselves
    while ( t && last != t ) {
      var r = [];
      last = t;

      t = jQuery.trim(t);

      var foundToken = false;

      // An attempt at speeding up child selectors that
      // point to a specific element tag
      var re = quickChild;
      var m = re.exec(t);

      if ( m ) {
        var nodeName = m[1].toUpperCase();

        // Perform our own iteration and filter
        for ( var i = 0; ret[i]; i++ )
          for ( var c = ret[i].firstChild; c; c = c.nextSibling )
            if ( c.nodeType == 1 && (nodeName == "*" || c.nodeName.toUpperCase() == nodeName.toUpperCase()) )
              r.push( c );

        ret = r;
        t = t.replace( re, "" );
        if ( t.indexOf(" ") == 0 ) continue;
        foundToken = true;
      } else {
        re = /^([>+~])\s*(\w*)/i;

        if ( (m = re.exec(t)) != null ) {
          r = [];

          var nodeName = m[2], merge = {};
          m = m[1];

          for ( var j = 0, rl = ret.length; j < rl; j++ ) {
            var n = m == "~" || m == "+" ? ret[j].nextSibling : ret[j].firstChild;
            for ( ; n; n = n.nextSibling )
              if ( n.nodeType == 1 ) {
                var id = jQuery.data(n);

                if ( m == "~" && merge[id] ) break;
                
                if (!nodeName || n.nodeName.toUpperCase() == nodeName.toUpperCase() ) {
                  if ( m == "~" ) merge[id] = true;
                  r.push( n );
                }
                
                if ( m == "+" ) break;
              }
          }

          ret = r;

          // And remove the token
          t = jQuery.trim( t.replace( re, "" ) );
          foundToken = true;
        }
      }

      // See if there's still an expression, and that we haven't already
      // matched a token
      if ( t && !foundToken ) {
        // Handle multiple expressions
        if ( !t.indexOf(",") ) {
          // Clean the result set
          if ( context == ret[0] ) ret.shift();

          // Merge the result sets
          done = jQuery.merge( done, ret );

          // Reset the context
          r = ret = [context];

          // Touch up the selector string
          t = " " + t.substr(1,t.length);

        } else {
          // Optimize for the case nodeName#idName
          var re2 = quickID;
          var m = re2.exec(t);
          
          // Re-organize the results, so that they're consistent
          if ( m ) {
             m = [ 0, m[2], m[3], m[1] ];

          } else {
            // Otherwise, do a traditional filter check for
            // ID, class, and element selectors
            re2 = quickClass;
            m = re2.exec(t);
          }

          m[2] = m[2].replace(/\\/g, "");

          var elem = ret[ret.length-1];

          // Try to do a global search by ID, where we can
          if ( m[1] == "#" && elem && elem.getElementById && !jQuery.isXMLDoc(elem) ) {
            // Optimization for HTML document case
            var oid = elem.getElementById(m[2]);
            
            // Do a quick check for the existence of the actual ID attribute
            // to avoid selecting by the name attribute in IE
            // also check to insure id is a string to avoid selecting an element with the name of 'id' inside a form
            if ( (jQuery.browser.msie||jQuery.browser.opera) && oid && typeof oid.id == "string" && oid.id != m[2] )
              oid = jQuery('[@id="'+m[2]+'"]', elem)[0];

            // Do a quick check for node name (where applicable) so
            // that div#foo searches will be really fast
            ret = r = oid && (!m[3] || jQuery.nodeName(oid, m[3])) ? [oid] : [];
          } else {
            // We need to find all descendant elements
            for ( var i = 0; ret[i]; i++ ) {
              // Grab the tag name being searched for
              var tag = m[1] == "#" && m[3] ? m[3] : m[1] != "" || m[0] == "" ? "*" : m[2];

              // Handle IE7 being really dumb about <object>s
              if ( tag == "*" && ret[i].nodeName.toLowerCase() == "object" )
                tag = "param";

              r = jQuery.merge( r, ret[i].getElementsByTagName( tag ));
            }

            // It's faster to filter by class and be done with it
            if ( m[1] == "." )
              r = jQuery.classFilter( r, m[2] );

            // Same with ID filtering
            if ( m[1] == "#" ) {
              var tmp = [];

              // Try to find the element with the ID
              for ( var i = 0; r[i]; i++ )
                if ( r[i].getAttribute("id") == m[2] ) {
                  tmp = [ r[i] ];
                  break;
                }

              r = tmp;
            }

            ret = r;
          }

          t = t.replace( re2, "" );
        }

      }

      // If a selector string still exists
      if ( t ) {
        // Attempt to filter it
        var val = jQuery.filter(t,r);
        ret = r = val.r;
        t = jQuery.trim(val.t);
      }
    }

    // An error occurred with the selector;
    // just return an empty set instead
    if ( t )
      ret = [];

    // Remove the root context
    if ( ret && context == ret[0] )
      ret.shift();

    // And combine the results
    done = jQuery.merge( done, ret );

    return done;
  },

  classFilter: function(r,m,not){
    m = " " + m + " ";
    var tmp = [];
    for ( var i = 0; r[i]; i++ ) {
      var pass = (" " + r[i].className + " ").indexOf( m ) >= 0;
      if ( !not && pass || not && !pass )
        tmp.push( r[i] );
    }
    return tmp;
  },

  filter: function(t,r,not) {
    var last;

    // Look for common filter expressions
    while ( t  && t != last ) {
      last = t;

      var p = jQuery.parse, m;

      for ( var i = 0; p[i]; i++ ) {
        m = p[i].exec( t );

        if ( m ) {
          // Remove what we just matched
          t = t.substring( m[0].length );

          m[2] = m[2].replace(/\\/g, "");
          break;
        }
      }

      if ( !m )
        break;

      // :not() is a special case that can be optimized by
      // keeping it out of the expression list
      if ( m[1] == ":" && m[2] == "not" )
        r = jQuery.filter(m[3], r, true).r;

      // We can get a big speed boost by filtering by class here
      else if ( m[1] == "." )
        r = jQuery.classFilter(r, m[2], not);

      else if ( m[1] == "[" ) {
        var tmp = [], type = m[3];
        
        for ( var i = 0, rl = r.length; i < rl; i++ ) {
          var a = r[i], z = a[ jQuery.props[m[2]] || m[2] ];
          
          if ( z == null || /href|src|selected/.test(m[2]) )
            z = jQuery.attr(a,m[2]) || '';

          if ( (type == "" && !!z ||
             type == "=" && z == m[5] ||
             type == "!=" && z != m[5] ||
             type == "^=" && z && !z.indexOf(m[5]) ||
             type == "$=" && z.substr(z.length - m[5].length) == m[5] ||
             (type == "*=" || type == "~=") && z.indexOf(m[5]) >= 0) ^ not )
              tmp.push( a );
        }
        
        r = tmp;

      // We can get a speed boost by handling nth-child here
      } else if ( m[1] == ":" && m[2] == "nth-child" ) {
        var merge = {}, tmp = [],
          test = /(\d*)n\+?(\d*)/.exec(
            m[3] == "even" && "2n" || m[3] == "odd" && "2n+1" ||
            !/\D/.test(m[3]) && "n+" + m[3] || m[3]),
          first = (test[1] || 1) - 0, last = test[2] - 0;

        for ( var i = 0, rl = r.length; i < rl; i++ ) {
          var node = r[i], parentNode = node.parentNode, id = jQuery.data(parentNode);

          if ( !merge[id] ) {
            var c = 1;

            for ( var n = parentNode.firstChild; n; n = n.nextSibling )
              if ( n.nodeType == 1 )
                n.nodeIndex = c++;

            merge[id] = true;
          }

          var add = false;

          if ( first == 1 ) {
            if ( last == 0 || node.nodeIndex == last )
              add = true;
          } else if ( (node.nodeIndex + last) % first == 0 )
            add = true;

          if ( add ^ not )
            tmp.push( node );
        }

        r = tmp;

      // Otherwise, find the expression to execute
      } else {
        var f = jQuery.expr[m[1]];
        if ( typeof f != "string" )
          f = jQuery.expr[m[1]][m[2]];

        // Build a custom macro to enclose it
        f = eval("false||function(a,i){return " + f + "}");

        // Execute it against the current filter
        r = jQuery.grep( r, f, not );
      }
    }

    // Return an array of filtered elements (r)
    // and the modified expression string (t)
    return { r: r, t: t };
  },

  dir: function( elem, dir ){
    var matched = [];
    var cur = elem[dir];
    while ( cur && cur != document ) {
      if ( cur.nodeType == 1 )
        matched.push( cur );
      cur = cur[dir];
    }
    return matched;
  },
  
  nth: function(cur,result,dir,elem){
    result = result || 1;
    var num = 0;

    for ( ; cur; cur = cur[dir] )
      if ( cur.nodeType == 1 && ++num == result )
        break;

    return cur;
  },
  
  sibling: function( n, elem ) {
    var r = [];

    for ( ; n; n = n.nextSibling ) {
      if ( n.nodeType == 1 && (!elem || n != elem) )
        r.push( n );
    }

    return r;
  }
});
/*
 * A number of helper functions used for managing events.
 * Many of the ideas behind this code orignated from 
 * Dean Edwards' addEvent library.
 */
jQuery.event = {

  // Bind an event to an element
  // Original by Dean Edwards
  add: function(element, type, handler, data) {
    // For whatever reason, IE has trouble passing the window object
    // around, causing it to be cloned in the process
    if ( jQuery.browser.msie && element.setInterval != undefined )
      element = window;

    // Make sure that the function being executed has a unique ID
    if ( !handler.guid )
      handler.guid = this.guid++;
      
    // if data is passed, bind to handler 
    if( data != undefined ) { 
            // Create temporary function pointer to original handler 
      var fn = handler; 

      // Create unique handler function, wrapped around original handler 
      handler = function() { 
        // Pass arguments and context to original handler 
        return fn.apply(this, arguments); 
      };

      // Store data in unique handler 
      handler.data = data;

      // Set the guid of unique handler to the same of original handler, so it can be removed 
      handler.guid = fn.guid;
    }

    // Namespaced event handlers
    var parts = type.split(".");
    type = parts[0];
    handler.type = parts[1];

    // Init the element's event structure
    var events = jQuery.data(element, "events") || jQuery.data(element, "events", {});
    
    var handle = jQuery.data(element, "handle", function(){
      // returned undefined or false
      var val;

      // Handle the second event of a trigger and when
      // an event is called after a page has unloaded
      if ( typeof jQuery == "undefined" || jQuery.event.triggered )
        return val;
      
      val = jQuery.event.handle.apply(element, arguments);
      
      return val;
    });

    // Get the current list of functions bound to this event
    var handlers = events[type];

    // Init the event handler queue
    if (!handlers) {
      handlers = events[type] = {}; 
      
      // And bind the global event handler to the element
      if (element.addEventListener)
        element.addEventListener(type, handle, false);
      else
        element.attachEvent("on" + type, handle);
    }

    // Add the function to the element's handler list
    handlers[handler.guid] = handler;

    // Keep track of which events have been used, for global triggering
    this.global[type] = true;
  },

  guid: 1,
  global: {},

  // Detach an event or set of events from an element
  remove: function(element, type, handler) {
    var events = jQuery.data(element, "events"), ret, index;

    // Namespaced event handlers
    if ( typeof type == "string" ) {
      var parts = type.split(".");
      type = parts[0];
    }

    if ( events ) {
      // type is actually an event object here
      if ( type && type.type ) {
        handler = type.handler;
        type = type.type;
      }
      
      if ( !type ) {
        for ( type in events )
          this.remove( element, type );

      } else if ( events[type] ) {
        // remove the given handler for the given type
        if ( handler )
          delete events[type][handler.guid];
        
        // remove all handlers for the given type
        else
          for ( handler in events[type] )
            // Handle the removal of namespaced events
            if ( !parts[1] || events[type][handler].type == parts[1] )
              delete events[type][handler];

        // remove generic event handler if no more handlers exist
        for ( ret in events[type] ) break;
        if ( !ret ) {
          if (element.removeEventListener)
            element.removeEventListener(type, jQuery.data(element, "handle"), false);
          else
            element.detachEvent("on" + type, jQuery.data(element, "handle"));
          ret = null;
          delete events[type];
        }
      }

      // Remove the expando if it's no longer used
      for ( ret in events ) break;
      if ( !ret ) {
        jQuery.removeData( element, "events" );
        jQuery.removeData( element, "handle" );
      }
    }
  },

  trigger: function(type, data, element, donative, extra) {
    // Clone the incoming data, if any
    data = jQuery.makeArray(data || []);

    // Handle a global trigger
    if ( !element ) {
      // Only trigger if we've ever bound an event for it
      if ( this.global[type] )
        jQuery("*").add([window, document]).trigger(type, data);

    // Handle triggering a single element
    } else {
      var val, ret, fn = jQuery.isFunction( element[ type ] || null ),
        // Check to see if we need to provide a fake event, or not
        evt = !data[0] || !data[0].preventDefault;
      
      // Pass along a fake event
      if ( evt )
        data.unshift( this.fix({ type: type, target: element }) );

      // Enforce the right trigger type
      data[0].type = type;

      // Trigger the event
      if ( jQuery.isFunction( jQuery.data(element, "handle") ) )
        val = jQuery.data(element, "handle").apply( element, data );

      // Handle triggering native .onfoo handlers
      if ( !fn && element["on"+type] && element["on"+type].apply( element, data ) === false )
        val = false;

      // Extra functions don't get the custom event object
      if ( evt )
        data.shift();

      // Handle triggering of extra function
      if ( extra && extra.apply( element, data ) === false )
        val = false;

      // Trigger the native events (except for clicks on links)
      if ( fn && donative !== false && val !== false && !(jQuery.nodeName(element, 'a') && type == "click") ) {
        this.triggered = true;
        element[ type ]();
      }

      this.triggered = false;
    }

    return val;
  },

  handle: function(event) {
    // returned undefined or false
    var val;

    // Empty object is for triggered events with no data
    event = jQuery.event.fix( event || window.event || {} ); 

    // Namespaced event handlers
    var parts = event.type.split(".");
    event.type = parts[0];

    var c = jQuery.data(this, "events") && jQuery.data(this, "events")[event.type], args = Array.prototype.slice.call( arguments, 1 );
    args.unshift( event );

    for ( var j in c ) {
      // Pass in a reference to the handler function itself
      // So that we can later remove it
      args[0].handler = c[j];
      args[0].data = c[j].data;

      // Filter the functions by class
      if ( !parts[1] || c[j].type == parts[1] ) {
        var tmp = c[j].apply( this, args );

        if ( val !== false )
          val = tmp;

        if ( tmp === false ) {
          event.preventDefault();
          event.stopPropagation();
        }
      }
    }

    // Clean up added properties in IE to prevent memory leak
    if (jQuery.browser.msie)
      event.target = event.preventDefault = event.stopPropagation =
        event.handler = event.data = null;

    return val;
  },

  fix: function(event) {
    // store a copy of the original event object 
    // and clone to set read-only properties
    var originalEvent = event;
    event = jQuery.extend({}, originalEvent);
    
    // add preventDefault and stopPropagation since 
    // they will not work on the clone
    event.preventDefault = function() {
      // if preventDefault exists run it on the original event
      if (originalEvent.preventDefault)
        originalEvent.preventDefault();
      // otherwise set the returnValue property of the original event to false (IE)
      originalEvent.returnValue = false;
    };
    event.stopPropagation = function() {
      // if stopPropagation exists run it on the original event
      if (originalEvent.stopPropagation)
        originalEvent.stopPropagation();
      // otherwise set the cancelBubble property of the original event to true (IE)
      originalEvent.cancelBubble = true;
    };
    
    // Fix target property, if necessary
    if ( !event.target && event.srcElement )
      event.target = event.srcElement;
        
    // check if target is a textnode (safari)
    if (jQuery.browser.safari && event.target.nodeType == 3)
      event.target = originalEvent.target.parentNode;

    // Add relatedTarget, if necessary
    if ( !event.relatedTarget && event.fromElement )
      event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;

    // Calculate pageX/Y if missing and clientX/Y available
    if ( event.pageX == null && event.clientX != null ) {
      var e = document.documentElement, b = document.body;
      event.pageX = event.clientX + (e && e.scrollLeft || b.scrollLeft || 0);
      event.pageY = event.clientY + (e && e.scrollTop || b.scrollTop || 0);
    }
      
    // Add which for key events
    if ( !event.which && (event.charCode || event.keyCode) )
      event.which = event.charCode || event.keyCode;
    
    // Add metaKey to non-Mac browsers (use ctrl for PC's and Meta for Macs)
    if ( !event.metaKey && event.ctrlKey )
      event.metaKey = event.ctrlKey;

    // Add which for click: 1 == left; 2 == middle; 3 == right
    // Note: button is not normalized, so don't use it
    if ( !event.which && event.button )
      event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
      
    return event;
  }
};

jQuery.fn.extend({
  bind: function( type, data, fn ) {
    return type == "unload" ? this.one(type, data, fn) : this.each(function(){
      jQuery.event.add( this, type, fn || data, fn && data );
    });
  },
  
  one: function( type, data, fn ) {
    return this.each(function(){
      jQuery.event.add( this, type, function(event) {
        jQuery(this).unbind(event);
        return (fn || data).apply( this, arguments);
      }, fn && data);
    });
  },

  unbind: function( type, fn ) {
    return this.each(function(){
      jQuery.event.remove( this, type, fn );
    });
  },

  trigger: function( type, data, fn ) {
    return this.each(function(){
      jQuery.event.trigger( type, data, this, true, fn );
    });
  },

  triggerHandler: function( type, data, fn ) {
    if ( this[0] )
      return jQuery.event.trigger( type, data, this[0], false, fn );
  },

  toggle: function() {
    // Save reference to arguments for access in closure
    var a = arguments;

    return this.click(function(e) {
      // Figure out which function to execute
      this.lastToggle = 0 == this.lastToggle ? 1 : 0;
      
      // Make sure that clicks stop
      e.preventDefault();
      
      // and execute the function
      return a[this.lastToggle].apply( this, [e] ) || false;
    });
  },

  hover: function(f,g) {
    
    // A private function for handling mouse 'hovering'
    function handleHover(e) {
      // Check if mouse(over|out) are still within the same parent element
      var p = e.relatedTarget;
  
      // Traverse up the tree
      while ( p && p != this ) try { p = p.parentNode; } catch(e) { p = this; };
      
      // If we actually just moused on to a sub-element, ignore it
      if ( p == this ) return false;
      
      // Execute the right function
      return (e.type == "mouseover" ? f : g).apply(this, [e]);
    }
    
    // Bind the function to the two event listeners
    return this.mouseover(handleHover).mouseout(handleHover);
  },
  
  ready: function(f) {
    // Attach the listeners
    bindReady();

    // If the DOM is already ready
    if ( jQuery.isReady )
      // Execute the function immediately
      f.apply( document, [jQuery] );
      
    // Otherwise, remember the function for later
    else
      // Add the function to the wait list
      jQuery.readyList.push( function() { return f.apply(this, [jQuery]); } );
  
    return this;
  }
});

jQuery.extend({
  /*
   * All the code that makes DOM Ready work nicely.
   */
  isReady: false,
  readyList: [],
  
  // Handle when the DOM is ready
  ready: function() {
    // Make sure that the DOM is not already loaded
    if ( !jQuery.isReady ) {
      // Remember that the DOM is ready
      jQuery.isReady = true;
      
      // If there are functions bound, to execute
      if ( jQuery.readyList ) {
        // Execute all of them
        jQuery.each( jQuery.readyList, function(){
          this.apply( document );
        });
        
        // Reset the list of functions
        jQuery.readyList = null;
      }
      // Remove event listener to avoid memory leak
      if ( jQuery.browser.mozilla || jQuery.browser.opera )
        document.removeEventListener( "DOMContentLoaded", jQuery.ready, false );
      
      // Remove script element used by IE hack
      if( !window.frames.length ) // don't remove if frames are present (#1187)
        jQuery(window).load(function(){ jQuery("#__ie_init").remove(); });
    }
  }
});

jQuery.each( ("blur,focus,load,resize,scroll,unload,click,dblclick," +
  "mousedown,mouseup,mousemove,mouseover,mouseout,change,select," + 
  "submit,keydown,keypress,keyup,error").split(","), function(i,o){
  
  // Handle event binding
  jQuery.fn[o] = function(f){
    return f ? this.bind(o, f) : this.trigger(o);
  };
});

var readyBound = false;

function bindReady(){
  if ( readyBound ) return;
  readyBound = true;

  // If Mozilla is used
  if ( jQuery.browser.mozilla || jQuery.browser.opera )
    // Use the handy event callback
    document.addEventListener( "DOMContentLoaded", jQuery.ready, false );
  
  // If IE is used, use the excellent hack by Matthias Miller
  // http://www.outofhanwell.com/blog/index.php?title=the_window_onload_problem_revisited
  else if ( jQuery.browser.msie ) {
  
    // Only works if you document.write() it
    document.write("<scr" + "ipt id=__ie_init defer=true " + 
      "src=//:><\/script>");
  
    // Use the defer script hack
    var script = document.getElementById("__ie_init");
    
    // script does not exist if jQuery is loaded dynamically
    if ( script ) 
      script.onreadystatechange = function() {
        if ( this.readyState != "complete" ) return;
        jQuery.ready();
      };
  
    // Clear from memory
    script = null;
  
  // If Safari  is used
  } else if ( jQuery.browser.safari )
    // Continually check to see if the document.readyState is valid
    jQuery.safariTimer = setInterval(function(){
      // loaded and complete are both valid states
      if ( document.readyState == "loaded" || 
        document.readyState == "complete" ) {
  
        // If either one are found, remove the timer
        clearInterval( jQuery.safariTimer );
        jQuery.safariTimer = null;
  
        // and execute any waiting functions
        jQuery.ready();
      }
    }, 10); 

  // A fallback to window.onload, that will always work
  jQuery.event.add( window, "load", jQuery.ready );
}
jQuery.fn.extend({
  load: function( url, params, callback ) {
    if ( jQuery.isFunction( url ) )
      return this.bind("load", url);

    var off = url.indexOf(" ");
    if ( off >= 0 ) {
      var selector = url.slice(off, url.length);
      url = url.slice(0, off);
    }

    callback = callback || function(){};

    // Default to a GET request
    var type = "GET";

    // If the second parameter was provided
    if ( params )
      // If it's a function
      if ( jQuery.isFunction( params ) ) {
        // We assume that it's the callback
        callback = params;
        params = null;

      // Otherwise, build a param string
      } else {
        params = jQuery.param( params );
        type = "POST";
      }

    var self = this;

    // Request the remote document
    jQuery.ajax({
      url: url,
      type: type,
      data: params,
      complete: function(res, status){
        // If successful, inject the HTML into all the matched elements
        if ( status == "success" || status == "notmodified" )
          // See if a selector was specified
          self.html( selector ?
            // Create a dummy div to hold the results
            jQuery("<div/>")
              // inject the contents of the document in, removing the scripts
              // to avoid any 'Permission Denied' errors in IE
              .append(res.responseText.replace(/<script(.|\s)*?\/script>/g, ""))

              // Locate the specified elements
              .find(selector) :

            // If not, just inject the full result
            res.responseText );

        // Add delay to account for Safari's delay in globalEval
        setTimeout(function(){
          self.each( callback, [res.responseText, status, res] );
        }, 13);
      }
    });
    return this;
  },

  serialize: function() {
    return jQuery.param(this.serializeArray());
  },
  serializeArray: function() {
    return this.map(function(){
      return jQuery.nodeName(this, "form") ?
        jQuery.makeArray(this.elements) : this;
    })
    .filter(function(){
      return this.name && !this.disabled && 
        (this.checked || /select|textarea/i.test(this.nodeName) || 
          /text|hidden|password/i.test(this.type));
    })
    .map(function(i, elem){
      var val = jQuery(this).val();
      return val == null ? null :
        val.constructor == Array ?
          jQuery.map( val, function(val, i){
            return {name: elem.name, value: val};
          }) :
          {name: elem.name, value: val};
    }).get();
  }
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( "ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","), function(i,o){
  jQuery.fn[o] = function(f){
    return this.bind(o, f);
  };
});

var jsc = (new Date).getTime();

jQuery.extend({
  get: function( url, data, callback, type ) {
    // shift arguments if data argument was ommited
    if ( jQuery.isFunction( data ) ) {
      callback = data;
      data = null;
    }
    
    return jQuery.ajax({
      type: "GET",
      url: url,
      data: data,
      success: callback,
      dataType: type
    });
  },

  getScript: function( url, callback ) {
    return jQuery.get(url, null, callback, "script");
  },

  getJSON: function( url, data, callback ) {
    return jQuery.get(url, data, callback, "json");
  },

  post: function( url, data, callback, type ) {
    if ( jQuery.isFunction( data ) ) {
      callback = data;
      data = {};
    }

    return jQuery.ajax({
      type: "POST",
      url: url,
      data: data,
      success: callback,
      dataType: type
    });
  },

  ajaxSetup: function( settings ) {
    jQuery.extend( jQuery.ajaxSettings, settings );
  },

  ajaxSettings: {
    global: true,
    type: "GET",
    timeout: 0,
    contentType: "application/x-www-form-urlencoded",
    processData: true,
    async: true,
    data: null
  },
  
  // Last-Modified header cache for next request
  lastModified: {},

  ajax: function( s ) {
    var jsonp, jsre = /=(\?|%3F)/g, status, data;

    // Extend the settings, but re-extend 's' so that it can be
    // checked again later (in the test suite, specifically)
    s = jQuery.extend(true, s, jQuery.extend(true, {}, jQuery.ajaxSettings, s));

    // convert data if not already a string
    if ( s.data && s.processData && typeof s.data != "string" )
      s.data = jQuery.param(s.data);

    // Handle JSONP Parameter Callbacks
    if ( s.dataType == "jsonp" ) {
      if ( s.type.toLowerCase() == "get" ) {
        if ( !s.url.match(jsre) )
          s.url += (s.url.match(/\?/) ? "&" : "?") + (s.jsonp || "callback") + "=?";
      } else if ( !s.data || !s.data.match(jsre) )
        s.data = (s.data ? s.data + "&" : "") + (s.jsonp || "callback") + "=?";
      s.dataType = "json";
    }

    // Build temporary JSONP function
    if ( s.dataType == "json" && (s.data && s.data.match(jsre) || s.url.match(jsre)) ) {
      jsonp = "jsonp" + jsc++;

      // Replace the =? sequence both in the query string and the data
      if ( s.data )
        s.data = s.data.replace(jsre, "=" + jsonp);
      s.url = s.url.replace(jsre, "=" + jsonp);

      // We need to make sure
      // that a JSONP style response is executed properly
      s.dataType = "script";

      // Handle JSONP-style loading
      window[ jsonp ] = function(tmp){
        data = tmp;
        success();
        complete();
        // Garbage collect
        window[ jsonp ] = undefined;
        try{ delete window[ jsonp ]; } catch(e){}
      };
    }

    if ( s.dataType == "script" && s.cache == null )
      s.cache = false;

    if ( s.cache === false && s.type.toLowerCase() == "get" )
      s.url += (s.url.match(/\?/) ? "&" : "?") + "_=" + (new Date()).getTime();

    // If data is available, append data to url for get requests
    if ( s.data && s.type.toLowerCase() == "get" ) {
      s.url += (s.url.match(/\?/) ? "&" : "?") + s.data;

      // IE likes to send both get and post data, prevent this
      s.data = null;
    }

    // Watch for a new set of requests
    if ( s.global && ! jQuery.active++ )
      jQuery.event.trigger( "ajaxStart" );

    // If we're requesting a remote document
    // and trying to load JSON or Script
    if ( !s.url.indexOf("http") && s.dataType == "script" ) {
      var head = document.getElementsByTagName("head")[0];
      var script = document.createElement("script");
      script.src = s.url;

      // Handle Script loading
      if ( !jsonp && (s.success || s.complete) ) {
        var done = false;

        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function(){
          if ( !done && (!this.readyState || 
              this.readyState == "loaded" || this.readyState == "complete") ) {
            done = true;
            success();
            complete();
            head.removeChild( script );
          }
        };
      }

      head.appendChild(script);

      // We handle everything using the script element injection
      return;
    }

    var requestDone = false;

    // Create the request object; Microsoft failed to properly
    // implement the XMLHttpRequest in IE7, so we use the ActiveXObject when it is available
    var xml = window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();

    // Open the socket
    xml.open(s.type, s.url, s.async);

    // Set the correct header, if data is being sent
    if ( s.data )
      xml.setRequestHeader("Content-Type", s.contentType);

    // Set the If-Modified-Since header, if ifModified mode.
    if ( s.ifModified )
      xml.setRequestHeader("If-Modified-Since",
        jQuery.lastModified[s.url] || "Thu, 01 Jan 1970 00:00:00 GMT" );

    // Set header so the called script knows that it's an XMLHttpRequest
    xml.setRequestHeader("X-Requested-With", "XMLHttpRequest");

    // Allow custom headers/mimetypes
    if ( s.beforeSend )
      s.beforeSend(xml);
      
    if ( s.global )
        jQuery.event.trigger("ajaxSend", [xml, s]);

    // Wait for a response to come back
    var onreadystatechange = function(isTimeout){
      // The transfer is complete and the data is available, or the request timed out
      if ( !requestDone && xml && (xml.readyState == 4 || isTimeout == "timeout") ) {
        requestDone = true;
        
        // clear poll interval
        if (ival) {
          clearInterval(ival);
          ival = null;
        }
        
        status = isTimeout == "timeout" && "timeout" ||
          !jQuery.httpSuccess( xml ) && "error" ||
          s.ifModified && jQuery.httpNotModified( xml, s.url ) && "notmodified" ||
          "success";

        if ( status == "success" ) {
          // Watch for, and catch, XML document parse errors
          try {
            // process the data (runs the xml through httpData regardless of callback)
            data = jQuery.httpData( xml, s.dataType );
          } catch(e) {
            status = "parsererror";
          }
        }

        // Make sure that the request was successful or notmodified
        if ( status == "success" ) {
          // Cache Last-Modified header, if ifModified mode.
          var modRes;
          try {
            modRes = xml.getResponseHeader("Last-Modified");
          } catch(e) {} // swallow exception thrown by FF if header is not available
  
          if ( s.ifModified && modRes )
            jQuery.lastModified[s.url] = modRes;

          // JSONP handles its own success callback
          if ( !jsonp )
            success();  
        } else
          jQuery.handleError(s, xml, status);

        // Fire the complete handlers
        complete();

        // Stop memory leaks
        if ( s.async )
          xml = null;
      }
    };
    
    if ( s.async ) {
      // don't attach the handler to the request, just poll it instead
      var ival = setInterval(onreadystatechange, 13); 

      // Timeout checker
      if ( s.timeout > 0 )
        setTimeout(function(){
          // Check to see if the request is still happening
          if ( xml ) {
            // Cancel the request
            xml.abort();
  
            if( !requestDone )
              onreadystatechange( "timeout" );
          }
        }, s.timeout);
    }
      
    // Send the data
    try {
      xml.send(s.data);
    } catch(e) {
      jQuery.handleError(s, xml, null, e);
    }
    
    // firefox 1.5 doesn't fire statechange for sync requests
    if ( !s.async )
      onreadystatechange();
    
    // return XMLHttpRequest to allow aborting the request etc.
    return xml;

    function success(){
      // If a local callback was specified, fire it and pass it the data
      if ( s.success )
        s.success( data, status );

      // Fire the global callback
      if ( s.global )
        jQuery.event.trigger( "ajaxSuccess", [xml, s] );
    }

    function complete(){
      // Process result
      if ( s.complete )
        s.complete(xml, status);

      // The request was completed
      if ( s.global )
        jQuery.event.trigger( "ajaxComplete", [xml, s] );

      // Handle the global AJAX counter
      if ( s.global && ! --jQuery.active )
        jQuery.event.trigger( "ajaxStop" );
    }
  },

  handleError: function( s, xml, status, e ) {
    // If a local callback was specified, fire it
    if ( s.error ) s.error( xml, status, e );

    // Fire the global callback
    if ( s.global )
      jQuery.event.trigger( "ajaxError", [xml, s, e] );
  },

  // Counter for holding the number of active queries
  active: 0,

  // Determines if an XMLHttpRequest was successful or not
  httpSuccess: function( r ) {
    try {
      return !r.status && location.protocol == "file:" ||
        ( r.status >= 200 && r.status < 300 ) || r.status == 304 ||
        jQuery.browser.safari && r.status == undefined;
    } catch(e){}
    return false;
  },

  // Determines if an XMLHttpRequest returns NotModified
  httpNotModified: function( xml, url ) {
    try {
      var xmlRes = xml.getResponseHeader("Last-Modified");

      // Firefox always returns 200. check Last-Modified date
      return xml.status == 304 || xmlRes == jQuery.lastModified[url] ||
        jQuery.browser.safari && xml.status == undefined;
    } catch(e){}
    return false;
  },

  httpData: function( r, type ) {
    var ct = r.getResponseHeader("content-type");
    var xml = type == "xml" || !type && ct && ct.indexOf("xml") >= 0;
    var data = xml ? r.responseXML : r.responseText;

    if ( xml && data.documentElement.tagName == "parsererror" )
      throw "parsererror";

    // If the type is "script", eval it in global context
    if ( type == "script" )
      jQuery.globalEval( data );

    // Get the JavaScript object, if JSON is used.
    if ( type == "json" )
      data = eval("(" + data + ")");

    return data;
  },

  // Serialize an array of form elements or a set of
  // key/values into a query string
  param: function( a ) {
    var s = [];

    // If an array was passed in, assume that it is an array
    // of form elements
    if ( a.constructor == Array || a.jquery )
      // Serialize the form elements
      jQuery.each( a, function(){
        s.push( encodeURIComponent(this.name) + "=" + encodeURIComponent( this.value ) );
      });

    // Otherwise, assume that it's an object of key/value pairs
    else
      // Serialize the key/values
      for ( var j in a )
        // If the value is an array then the key names need to be repeated
        if ( a[j] && a[j].constructor == Array )
          jQuery.each( a[j], function(){
            s.push( encodeURIComponent(j) + "=" + encodeURIComponent( this ) );
          });
        else
          s.push( encodeURIComponent(j) + "=" + encodeURIComponent( a[j] ) );

    // Return the resulting serialization
    return s.join("&").replace(/%20/g, "+");
  }

});
jQuery.fn.extend({
  show: function(speed,callback){
    return speed ?
      this.animate({
        height: "show", width: "show", opacity: "show"
      }, speed, callback) :
      
      this.filter(":hidden").each(function(){
        this.style.display = this.oldblock ? this.oldblock : "";
        if ( jQuery.css(this,"display") == "none" )
          this.style.display = "block";
      }).end();
  },
  
  hide: function(speed,callback){
    return speed ?
      this.animate({
        height: "hide", width: "hide", opacity: "hide"
      }, speed, callback) :
      
      this.filter(":visible").each(function(){
        this.oldblock = this.oldblock || jQuery.css(this,"display");
        if ( this.oldblock == "none" )
          this.oldblock = "block";
        this.style.display = "none";
      }).end();
  },

  // Save the old toggle function
  _toggle: jQuery.fn.toggle,
  
  toggle: function( fn, fn2 ){
    return jQuery.isFunction(fn) && jQuery.isFunction(fn2) ?
      this._toggle( fn, fn2 ) :
      fn ?
        this.animate({
          height: "toggle", width: "toggle", opacity: "toggle"
        }, fn, fn2) :
        this.each(function(){
          jQuery(this)[ jQuery(this).is(":hidden") ? "show" : "hide" ]();
        });
  },
  
  slideDown: function(speed,callback){
    return this.animate({height: "show"}, speed, callback);
  },
  
  slideUp: function(speed,callback){
    return this.animate({height: "hide"}, speed, callback);
  },

  slideToggle: function(speed, callback){
    return this.animate({height: "toggle"}, speed, callback);
  },
  
  fadeIn: function(speed, callback){
    return this.animate({opacity: "show"}, speed, callback);
  },
  
  fadeOut: function(speed, callback){
    return this.animate({opacity: "hide"}, speed, callback);
  },
  
  fadeTo: function(speed,to,callback){
    return this.animate({opacity: to}, speed, callback);
  },
  
  animate: function( prop, speed, easing, callback ) {
    var opt = jQuery.speed(speed, easing, callback);

    return this[ opt.queue === false ? "each" : "queue" ](function(){
      opt = jQuery.extend({}, opt);
      var hidden = jQuery(this).is(":hidden"), self = this;
      
      for ( var p in prop ) {
        if ( prop[p] == "hide" && hidden || prop[p] == "show" && !hidden )
          return jQuery.isFunction(opt.complete) && opt.complete.apply(this);

        if ( p == "height" || p == "width" ) {
          // Store display property
          opt.display = jQuery.css(this, "display");

          // Make sure that nothing sneaks out
          opt.overflow = this.style.overflow;
        }
      }

      if ( opt.overflow != null )
        this.style.overflow = "hidden";

      opt.curAnim = jQuery.extend({}, prop);
      
      jQuery.each( prop, function(name, val){
        var e = new jQuery.fx( self, opt, name );

        if ( /toggle|show|hide/.test(val) )
          e[ val == "toggle" ? hidden ? "show" : "hide" : val ]( prop );
        else {
          var parts = val.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/),
            start = e.cur(true) || 0;

          if ( parts ) {
            var end = parseFloat(parts[2]),
              unit = parts[3] || "px";

            // We need to compute starting value
            if ( unit != "px" ) {
              self.style[ name ] = (end || 1) + unit;
              start = ((end || 1) / e.cur(true)) * start;
              self.style[ name ] = start + unit;
            }

            // If a +=/-= token was provided, we're doing a relative animation
            if ( parts[1] )
              end = ((parts[1] == "-=" ? -1 : 1) * end) + start;

            e.custom( start, end, unit );
          } else
            e.custom( start, val, "" );
        }
      });

      // For JS strict compliance
      return true;
    });
  },
  
  queue: function(type, fn){
    if ( jQuery.isFunction(type) ) {
      fn = type;
      type = "fx";
    }

    if ( !type || (typeof type == "string" && !fn) )
      return queue( this[0], type );

    return this.each(function(){
      if ( fn.constructor == Array )
        queue(this, type, fn);
      else {
        queue(this, type).push( fn );
      
        if ( queue(this, type).length == 1 )
          fn.apply(this);
      }
    });
  },

  stop: function(){
    var timers = jQuery.timers;

    return this.each(function(){
      for ( var i = 0; i < timers.length; i++ )
        if ( timers[i].elem == this )
          timers.splice(i--, 1);
    }).dequeue();
  }

});

var queue = function( elem, type, array ) {
  if ( !elem )
    return;

  var q = jQuery.data( elem, type + "queue" );

  if ( !q || array )
    q = jQuery.data( elem, type + "queue", 
      array ? jQuery.makeArray(array) : [] );

  return q;
};

jQuery.fn.dequeue = function(type){
  type = type || "fx";

  return this.each(function(){
    var q = queue(this, type);

    q.shift();

    if ( q.length )
      q[0].apply( this );
  });
};

jQuery.extend({
  
  speed: function(speed, easing, fn) {
    var opt = speed && speed.constructor == Object ? speed : {
      complete: fn || !fn && easing || 
        jQuery.isFunction( speed ) && speed,
      duration: speed,
      easing: fn && easing || easing && easing.constructor != Function && easing
    };

    opt.duration = (opt.duration && opt.duration.constructor == Number ? 
      opt.duration : 
      { slow: 600, fast: 200 }[opt.duration]) || 400;
  
    // Queueing
    opt.old = opt.complete;
    opt.complete = function(){
      jQuery(this).dequeue();
      if ( jQuery.isFunction( opt.old ) )
        opt.old.apply( this );
    };
  
    return opt;
  },
  
  easing: {
    linear: function( p, n, firstNum, diff ) {
      return firstNum + diff * p;
    },
    swing: function( p, n, firstNum, diff ) {
      return ((-Math.cos(p*Math.PI)/2) + 0.5) * diff + firstNum;
    }
  },
  
  timers: [],

  fx: function( elem, options, prop ){
    this.options = options;
    this.elem = elem;
    this.prop = prop;

    if ( !options.orig )
      options.orig = {};
  }

});

jQuery.fx.prototype = {

  // Simple function for setting a style value
  update: function(){
    if ( this.options.step )
      this.options.step.apply( this.elem, [ this.now, this ] );

    (jQuery.fx.step[this.prop] || jQuery.fx.step._default)( this );

    // Set display property to block for height/width animations
    if ( this.prop == "height" || this.prop == "width" )
      this.elem.style.display = "block";
  },

  // Get the current size
  cur: function(force){
    if ( this.elem[this.prop] != null && this.elem.style[this.prop] == null )
      return this.elem[ this.prop ];

    var r = parseFloat(jQuery.curCSS(this.elem, this.prop, force));
    return r && r > -10000 ? r : parseFloat(jQuery.css(this.elem, this.prop)) || 0;
  },

  // Start an animation from one number to another
  custom: function(from, to, unit){
    this.startTime = (new Date()).getTime();
    this.start = from;
    this.end = to;
    this.unit = unit || this.unit || "px";
    this.now = this.start;
    this.pos = this.state = 0;
    this.update();

    var self = this;
    function t(){
      return self.step();
    }

    t.elem = this.elem;

    jQuery.timers.push(t);

    if ( jQuery.timers.length == 1 ) {
      var timer = setInterval(function(){
        var timers = jQuery.timers;
        
        for ( var i = 0; i < timers.length; i++ )
          if ( !timers[i]() )
            timers.splice(i--, 1);

        if ( !timers.length )
          clearInterval( timer );
      }, 13);
    }
  },

  // Simple 'show' function
  show: function(){
    // Remember where we started, so that we can go back to it later
    this.options.orig[this.prop] = jQuery.attr( this.elem.style, this.prop );
    this.options.show = true;

    // Begin the animation
    this.custom(0, this.cur());

    // Make sure that we start at a small width/height to avoid any
    // flash of content
    if ( this.prop == "width" || this.prop == "height" )
      this.elem.style[this.prop] = "1px";
    
    // Start by showing the element
    jQuery(this.elem).show();
  },

  // Simple 'hide' function
  hide: function(){
    // Remember where we started, so that we can go back to it later
    this.options.orig[this.prop] = jQuery.attr( this.elem.style, this.prop );
    this.options.hide = true;

    // Begin the animation
    this.custom(this.cur(), 0);
  },

  // Each step of an animation
  step: function(){
    var t = (new Date()).getTime();

    if ( t > this.options.duration + this.startTime ) {
      this.now = this.end;
      this.pos = this.state = 1;
      this.update();

      this.options.curAnim[ this.prop ] = true;

      var done = true;
      for ( var i in this.options.curAnim )
        if ( this.options.curAnim[i] !== true )
          done = false;

      if ( done ) {
        if ( this.options.display != null ) {
          // Reset the overflow
          this.elem.style.overflow = this.options.overflow;
        
          // Reset the display
          this.elem.style.display = this.options.display;
          if ( jQuery.css(this.elem, "display") == "none" )
            this.elem.style.display = "block";
        }

        // Hide the element if the "hide" operation was done
        if ( this.options.hide )
          this.elem.style.display = "none";

        // Reset the properties, if the item has been hidden or shown
        if ( this.options.hide || this.options.show )
          for ( var p in this.options.curAnim )
            jQuery.attr(this.elem.style, p, this.options.orig[p]);
      }

      // If a callback was provided, execute it
      if ( done && jQuery.isFunction( this.options.complete ) )
        // Execute the complete function
        this.options.complete.apply( this.elem );

      return false;
    } else {
      var n = t - this.startTime;
      this.state = n / this.options.duration;

      // Perform the easing function, defaults to swing
      this.pos = jQuery.easing[this.options.easing || (jQuery.easing.swing ? "swing" : "linear")](this.state, n, 0, 1, this.options.duration);
      this.now = this.start + ((this.end - this.start) * this.pos);

      // Perform the next step of the animation
      this.update();
    }

    return true;
  }

};

jQuery.fx.step = {
  scrollLeft: function(fx){
    fx.elem.scrollLeft = fx.now;
  },

  scrollTop: function(fx){
    fx.elem.scrollTop = fx.now;
  },

  opacity: function(fx){
    jQuery.attr(fx.elem.style, "opacity", fx.now);
  },

  _default: function(fx){
    fx.elem.style[ fx.prop ] = fx.now + fx.unit;
  }
};
// The Offset Method
// Originally By Brandon Aaron, part of the Dimension Plugin
// http://jquery.com/plugins/project/dimensions
jQuery.fn.offset = function() {
  var left = 0, top = 0, elem = this[0], results;
  
  if ( elem ) with ( jQuery.browser ) {
    var absolute     = jQuery.css(elem, "position") == "absolute", 
        parent       = elem.parentNode, 
        offsetParent = elem.offsetParent, 
        doc          = elem.ownerDocument,
        safari2      = safari && parseInt(version) < 522;
  
    // Use getBoundingClientRect if available
    if ( elem.getBoundingClientRect ) {
      box = elem.getBoundingClientRect();
    
      // Add the document scroll offsets
      add(
        box.left + Math.max(doc.documentElement.scrollLeft, doc.body.scrollLeft),
        box.top  + Math.max(doc.documentElement.scrollTop,  doc.body.scrollTop)
      );
    
      // IE adds the HTML element's border, by default it is medium which is 2px
      // IE 6 and IE 7 quirks mode the border width is overwritable by the following css html { border: 0; }
      // IE 7 standards mode, the border is always 2px
      if ( msie ) {
        var border = jQuery("html").css("borderWidth");
        border = (border == "medium" || jQuery.boxModel && parseInt(version) >= 7) && 2 || border;
        add( -border, -border );
      }
  
    // Otherwise loop through the offsetParents and parentNodes
    } else {
    
      // Initial element offsets
      add( elem.offsetLeft, elem.offsetTop );
    
      // Get parent offsets
      while ( offsetParent ) {
        // Add offsetParent offsets
        add( offsetParent.offsetLeft, offsetParent.offsetTop );
      
        // Mozilla and Safari > 2 does not include the border on offset parents
        // However Mozilla adds the border for table cells
        if ( mozilla && /^t[d|h]$/i.test(parent.tagName) || !safari2 )
          border( offsetParent );
        
        // Safari <= 2 doubles body offsets with an absolutely positioned element or parent
        if ( safari2 && !absolute && jQuery.css(offsetParent, "position") == "absolute" )
          absolute = true;
      
        // Get next offsetParent
        offsetParent = offsetParent.offsetParent;
      }
    
      // Get parent scroll offsets
      while ( parent.tagName && !/^body|html$/i.test(parent.tagName) ) {
        // Work around opera inline/table scrollLeft/Top bug
        if ( !/^inline|table-row.*$/i.test(jQuery.css(parent, "display")) )
          // Subtract parent scroll offsets
          add( -parent.scrollLeft, -parent.scrollTop );
      
        // Mozilla does not add the border for a parent that has overflow != visible
        if ( mozilla && jQuery.css(parent, "overflow") != "visible" )
          border( parent );
      
        // Get next parent
        parent = parent.parentNode;
      }
    
      // Safari doubles body offsets with an absolutely positioned element or parent
      if ( safari2 && absolute )
        add( -doc.body.offsetLeft, -doc.body.offsetTop );
    }

    // Return an object with top and left properties
    results = { top: top, left: left };
  }

  return results;

  function border(elem) {
    add( jQuery.css(elem, "borderLeftWidth"), jQuery.css(elem, "borderTopWidth") );
  }

  function add(l, t) {
    left += parseInt(l) || 0;
    top += parseInt(t) || 0;
  }
};
})();

// *************************************************************************************************
// *************************************** DIMENSIONS **********************************************
// *************************************************************************************************

/* Copyright (c) 2007 Paul Bakaus (paul.bakaus@googlemail.com) and Brandon Aaron (brandon.aaron@gmail.com || http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * $LastChangedDate: 2007-09-11 02:38:31 +0000 (Tue, 11 Sep 2007) $
 * $Rev: 3238 $
 *
 * Version: @VERSION
 *
 * Requires: jQuery 1.2+
 */

(function($){
  
$.dimensions = {
  version: '@VERSION'
};

// Create innerHeight, innerWidth, outerHeight and outerWidth methods
$.each( [ 'Height', 'Width' ], function(i, name){
  
  // innerHeight and innerWidth
  $.fn[ 'inner' + name ] = function() {
    if (!this[0]) return;
    
    var torl = name == 'Height' ? 'Top'    : 'Left',  // top or left
        borr = name == 'Height' ? 'Bottom' : 'Right'; // bottom or right
    
    return this[ name.toLowerCase() ]() + num(this, 'padding' + torl) + num(this, 'padding' + borr);
  };
  
  // outerHeight and outerWidth
  $.fn[ 'outer' + name ] = function(options) {
    if (!this[0]) return;
    
    var torl = name == 'Height' ? 'Top'    : 'Left',  // top or left
        borr = name == 'Height' ? 'Bottom' : 'Right'; // bottom or right
    
    options = $.extend({ margin: false }, options || {});
    
    return this[ name.toLowerCase() ]()
        + num(this, 'border' + torl + 'Width') + num(this, 'border' + borr + 'Width')
        + num(this, 'padding' + torl) + num(this, 'padding' + borr)
        + (options.margin ? (num(this, 'margin' + torl) + num(this, 'margin' + borr)) : 0);
  };
});

// Create scrollLeft and scrollTop methods
$.each( ['Left', 'Top'], function(i, name) {
  $.fn[ 'scroll' + name ] = function(val) {
    if (!this[0]) return;
    
    return val != undefined ?
    
      // Set the scroll offset
      this.each(function() {
        this == window || this == document ?
          window.scrollTo( 
            name == 'Left' ? val : $(window)[ 'scrollLeft' ](),
            name == 'Top'  ? val : $(window)[ 'scrollTop'  ]()
          ) :
          this[ 'scroll' + name ] = val;
      }) :
      
      // Return the scroll offset
      this[0] == window || this[0] == document ?
        self[ (name == 'Left' ? 'pageXOffset' : 'pageYOffset') ] ||
          $.boxModel && document.documentElement[ 'scroll' + name ] ||
          document.body[ 'scroll' + name ] :
        this[0][ 'scroll' + name ];
  };
});


$.fn.extend({
  position: function() {
    var left = 0, top = 0, elem = this[0], offset, parentOffset, offsetParent, results;
    
    if (elem) {
      // Get *real* offsetParent
      offsetParent = this.offsetParent();
      
      // Get correct offsets
      offset       = this.offset();
      parentOffset = offsetParent.offset();
      
      // Subtract element margins
      offset.top  -= num(elem, 'marginTop');
      offset.left -= num(elem, 'marginLeft');
      
      // Add offsetParent borders
      parentOffset.top  += num(offsetParent, 'borderTopWidth');
      parentOffset.left += num(offsetParent, 'borderLeftWidth');
      
      // Subtract the two offsets
      results = {
        top:  offset.top  - parentOffset.top,
        left: offset.left - parentOffset.left
      };
    }
    
    return results;
  },
  
  offsetParent: function() {
    var offsetParent = this[0].offsetParent;
    while ( offsetParent && (!/^body|html$/i.test(offsetParent.tagName) && $.css(offsetParent, 'position') == 'static') )
      offsetParent = offsetParent.offsetParent;
    return $(offsetParent);
  }
});

var num = function(el, prop) {
  return parseInt($.css(el.jquery?el[0]:el,prop))||0;
};

})(jQuery);


// *************************************************************************************************
// *************************************** MOUSE **********************************************
// *************************************************************************************************

(function($) {
  
  //If the UI scope is not availalable, add it
  $.ui = $.ui || {};
  
  //Add methods that are vital for all mouse interaction stuff (plugin registering)
  $.extend($.ui, {
    plugin: {
      add: function(w, c, o, p) {
        var a = $.ui[w].prototype; if(!a.plugins[c]) a.plugins[c] = [];
        a.plugins[c].push([o,p]);
      },
      call: function(instance, name, arguments) {
        var c = instance.plugins[name]; if(!c) return;
        var o = instance.interaction ? instance.interaction.options : instance.options;
        var e = instance.interaction ? instance.interaction.element : instance.element;
        
        for (var i = 0; i < c.length; i++) {
          if (o[c[i][0]]) c[i][1].apply(e, arguments);
        } 
      } 
    }
  });
  
  $.fn.mouseInteractionDestroy = function() {
    this.each(function() {
      if($.data(this, "ui-mouse")) $.data(this, "ui-mouse").destroy();  
    });
  }
  
  $.ui.mouseInteraction = function(el,o) {
  
    if(!o) var o = {};
    this.element = el;
    $.data(this.element, "ui-mouse", this);
    
    this.options = {};
    $.extend(this.options, o);
    $.extend(this.options, {
      handle : o.handle ? ($(o.handle, el)[0] ? $(o.handle, el) : $(el)) : $(el),
      helper: o.helper || 'original',
      preventionDistance: o.preventionDistance || 0,
      dragPrevention: o.dragPrevention ? o.dragPrevention.toLowerCase().split(',') : ['input','textarea','button','select','option'],
      cursorAt: { top: ((o.cursorAt && o.cursorAt.top) ? o.cursorAt.top : 0), left: ((o.cursorAt && o.cursorAt.left) ? o.cursorAt.left : 0), bottom: ((o.cursorAt && o.cursorAt.bottom) ? o.cursorAt.bottom : 0), right: ((o.cursorAt && o.cursorAt.right) ? o.cursorAt.right : 0) },
      cursorAtIgnore: (!o.cursorAt) ? true : false, //Internal property
      appendTo: o.appendTo || 'parent'      
    })
    o = this.options; //Just Lazyness
    
    if(!this.options.nonDestructive && (o.helper == 'clone' || o.helper == 'original')) {

      // Let's save the margins for better reference
      o.margins = {
        top: parseInt($(el).css('marginTop')) || 0,
        left: parseInt($(el).css('marginLeft')) || 0,
        bottom: parseInt($(el).css('marginBottom')) || 0,
        right: parseInt($(el).css('marginRight')) || 0
      };

      // We have to add margins to our cursorAt
      if(o.cursorAt.top != 0) o.cursorAt.top = o.margins.top;
      if(o.cursorAt.left != 0) o.cursorAt.left += o.margins.left;
      if(o.cursorAt.bottom != 0) o.cursorAt.bottom += o.margins.bottom;
      if(o.cursorAt.right != 0) o.cursorAt.right += o.margins.right;
      
      
      if(o.helper == 'original')
        o.wasPositioned = $(el).css('position');
      
    } else {
      o.margins = { top: 0, left: 0, right: 0, bottom: 0 };
    }
    
    var self = this;
    this.mousedownfunc = function(e) { // Bind the mousedown event
      return self.click.apply(self, [e]); 
    }
    o.handle.bind('mousedown', this.mousedownfunc);
    
    //Prevent selection of text when starting the drag in IE
    if($.browser.msie) $(this.element).attr('unselectable', 'on');
    
  }
  
  $.extend($.ui.mouseInteraction.prototype, {
    plugins: {},
    currentTarget: null,
    lastTarget: null,
    timer: null,
    slowMode: false,
    init: false,
    destroy: function() {
      this.options.handle.unbind('mousedown', this.mousedownfunc);
    },
    trigger: function(e) {
      return this.click.apply(this, arguments);
    },
    click: function(e) {

      var o = this.options;
      
      window.focus();
      if(e.which != 1) return true; //only left click starts dragging
    
      // Prevent execution on defined elements
      var targetName = (e.target) ? e.target.nodeName.toLowerCase() : e.srcElement.nodeName.toLowerCase();
      for(var i=0;i<o.dragPrevention.length;i++) {
        if(targetName == o.dragPrevention[i]) return true;
      }

      //Prevent execution on condition
      if(o.startCondition && !o.startCondition.apply(this, [e])) return true;

      var self = this;
      this.mouseup = function(e) { return self.stop.apply(self, [e]); }
      this.mousemove = function(e) { return self.drag.apply(self, [e]); }

      var initFunc = function() { //This function get's called at bottom or after timeout
        $(document).bind('mouseup', self.mouseup);
        $(document).bind('mousemove', self.mousemove);
        self.opos = [e.pageX,e.pageY]; // Get the original mouse position
      }
      
      if(o.preventionTimeout) { //use prevention timeout
        if(this.timer) clearInterval(this.timer);
        this.timer = setTimeout(function() { initFunc(); }, o.preventionTimeout);
        return false;
      }
    
      initFunc();
      return false;
      
    },
    start: function(e) {
      
      var o = this.options; var a = this.element;
      o.co = $(a).offset(); //get the current offset
        
      this.helper = typeof o.helper == 'function' ? $(o.helper.apply(a, [e,this]))[0] : (o.helper == 'clone' ? $(a).clone()[0] : a);

      if(o.appendTo == 'parent') { // Let's see if we have a positioned parent
        var cp = a.parentNode;
        while (cp) {
          if(cp.style && ($(cp).css('position') == 'relative' || $(cp).css('position') == 'absolute')) {
            o.pp = cp;
            o.po = $(cp).offset();
            o.ppOverflow = !!($(o.pp).css('overflow') == 'auto' || $(o.pp).css('overflow') == 'scroll'); //TODO!
            break;  
          }
          cp = cp.parentNode ? cp.parentNode : null;
        };
        
        if(!o.pp) o.po = { top: 0, left: 0 };
      }
      
      this.pos = [this.opos[0],this.opos[1]]; //Use the relative position
      this.rpos = [this.pos[0],this.pos[1]]; //Save the absolute position
      
      if(o.cursorAtIgnore) { // If we want to pick the element where we clicked, we borrow cursorAt and add margins
        o.cursorAt.left = this.pos[0] - o.co.left + o.margins.left;
        o.cursorAt.top = this.pos[1] - o.co.top + o.margins.top;
      }



      if(o.pp) { // If we have a positioned parent, we pick the draggable relative to it
        this.pos[0] -= o.po.left;
        this.pos[1] -= o.po.top;
      }
      
      this.slowMode = (o.cursorAt && (o.cursorAt.top-o.margins.top > 0 || o.cursorAt.bottom-o.margins.bottom > 0) && (o.cursorAt.left-o.margins.left > 0 || o.cursorAt.right-o.margins.right > 0)) ? true : false; //If cursorAt is within the helper, set slowMode to true
      
      if(!o.nonDestructive) $(this.helper).css('position', 'absolute');
      if(o.helper != 'original') $(this.helper).appendTo((o.appendTo == 'parent' ? a.parentNode : o.appendTo)).show();

      // Remap right/bottom properties for cursorAt to left/top
      if(o.cursorAt.right && !o.cursorAt.left) o.cursorAt.left = this.helper.offsetWidth+o.margins.right+o.margins.left - o.cursorAt.right;
      if(o.cursorAt.bottom && !o.cursorAt.top) o.cursorAt.top = this.helper.offsetHeight+o.margins.top+o.margins.bottom - o.cursorAt.bottom;
    
      this.init = true; 

      if(o._start) o._start.apply(a, [this.helper, this.pos, o.cursorAt, this, e]); // Trigger the start callback
      this.helperSize = { width: outerWidth(this.helper), height: outerHeight(this.helper) }; //Set helper size property
      return false;
            
    },
    stop: function(e) {     
      
      var o = this.options; var a = this.element; var self = this;

      $(document).unbind('mouseup', self.mouseup);
      $(document).unbind('mousemove', self.mousemove);

      if(this.init == false) return this.opos = this.pos = null;
      if(o._beforeStop) o._beforeStop.apply(a, [this.helper, this.pos, o.cursorAt, this, e]);

      if(this.helper != a && !o.beQuietAtEnd) { // Remove helper, if it's not the original node
        $(this.helper).remove(); this.helper = null;
      }
      
      if(!o.beQuietAtEnd) {
        //if(o.wasPositioned) $(a).css('position', o.wasPositioned);
        if(o._stop) o._stop.apply(a, [this.helper, this.pos, o.cursorAt, this, e]);
      }

      this.init = false;
      this.opos = this.pos = null;
      return false;
      
    },
    drag: function(e) {

      if (!this.opos || ($.browser.msie && !e.button)) return this.stop.apply(this, [e]); // check for IE mouseup when moving into the document again
      var o = this.options;
      
      this.pos = [e.pageX,e.pageY]; //relative mouse position
      if(this.rpos && this.rpos[0] == this.pos[0] && this.rpos[1] == this.pos[1]) return false;
      this.rpos = [this.pos[0],this.pos[1]]; //absolute mouse position
      
      if(o.pp) { //If we have a positioned parent, use a relative position
        this.pos[0] -= o.po.left;
        this.pos[1] -= o.po.top;  
      }
      
      if( (Math.abs(this.rpos[0]-this.opos[0]) > o.preventionDistance || Math.abs(this.rpos[1]-this.opos[1]) > o.preventionDistance) && this.init == false) //If position is more than x pixels from original position, start dragging
        this.start.apply(this,[e]);     
      else {
        if(this.init == false) return false;
      }
    
      if(o._drag) o._drag.apply(this.element, [this.helper, this.pos, o.cursorAt, this, e]);
      return false;
      
    }
  });

  var num = function(el, prop) {
    return parseInt($.css(el.jquery?el[0]:el,prop))||0;
  };
  function outerWidth(el) {
    var $el = $(el), ow = $el.width();
    for (var i = 0, props = ['borderLeftWidth', 'paddingLeft', 'paddingRight', 'borderRightWidth']; i < props.length; i++)
      ow += num($el, props[i]);
    return ow;
  }
  function outerHeight(el) {
    var $el = $(el), oh = $el.width();
    for (var i = 0, props = ['borderTopWidth', 'paddingTop', 'paddingBottom', 'borderBottomWidth']; i < props.length; i++)
      oh += num($el, props[i]);
    return oh;
  }

 })($);

// *************************************************************************************************
// *************************************** DRAGGABLE **********************************************
// *************************************************************************************************

(function($) {

  //Make nodes selectable by expression
  $.extend($.expr[':'], { draggable: "(' '+a.className+' ').indexOf(' ui-draggable ')" });


  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-draggable")) jQuery.data(this, "ui-draggable")["'+cur+'"](a); }); }');
    $.fn["draggable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };
  
  //get instance method
  $.fn.draggableInstance = function() {
    if($(this[0]).is(".ui-draggable")) return $.data(this[0], "ui-draggable");
    return false;
  };

  $.fn.draggable = function(o) {
    return this.each(function() {
      new $.ui.draggable(this, o);
    });
  }
  
  $.ui.ddmanager = {
    current: null,
    droppables: [],
    prepareOffsets: function(t, e) {
      var dropTop = $.ui.ddmanager.dropTop = [];
      var dropLeft = $.ui.ddmanager.dropLeft;
      var m = $.ui.ddmanager.droppables;
      for (var i = 0; i < m.length; i++) {
        if(m[i].item.disabled) continue;
        m[i].offset = $(m[i].item.element).offset();
        if (t && m[i].item.options.accept(t.element)) //Activate the droppable if used directly from draggables
          m[i].item.activate.call(m[i].item, e);
      }
    },
    fire: function(oDrag, e) {
      
      var oDrops = $.ui.ddmanager.droppables;
      var oOvers = $.grep(oDrops, function(oDrop) {
        
        if (!oDrop.item.disabled && $.ui.intersect(oDrag, oDrop, oDrop.item.options.tolerance))
          oDrop.item.drop.call(oDrop.item, e);
      });
      $.each(oDrops, function(i, oDrop) {
        if (!oDrop.item.disabled && oDrop.item.options.accept(oDrag.element)) {
          oDrop.out = 1; oDrop.over = 0;
          oDrop.item.deactivate.call(oDrop.item, e);
        }
      });
    },
    update: function(oDrag, e) {
      
      if(oDrag.options.refreshPositions) $.ui.ddmanager.prepareOffsets();
      
      var oDrops = $.ui.ddmanager.droppables;
      var oOvers = $.grep(oDrops, function(oDrop) {
        if(oDrop.item.disabled) return false; 
        var isOver = $.ui.intersect(oDrag, oDrop, oDrop.item.options.tolerance)
        if (!isOver && oDrop.over == 1) {
          oDrop.out = 1; oDrop.over = 0;
          oDrop.item.out.call(oDrop.item, e);
        }
        return isOver;
      });
      $.each(oOvers, function(i, oOver) {
        if (oOver.over == 0) {
          oOver.out = 0; oOver.over = 1;
          oOver.item.over.call(oOver.item, e);
        }
      });
    }
  };
  
  $.ui.draggable = function(el, o) {
    
    var options = {};
    $.extend(options, o);
    var self = this;
    $.extend(options, {
      _start: function(h, p, c, t, e) {
        self.start.apply(t, [self, e]); // Trigger the start callback       
      },
      _beforeStop: function(h, p, c, t, e) {
        self.stop.apply(t, [self, e]); // Trigger the start callback
      },
      _drag: function(h, p, c, t, e) {
        self.drag.apply(t, [self, e]); // Trigger the start callback
      },
      startCondition: function(e) {
        return !(e.target.className.indexOf("ui-resizable-handle") != -1 || self.disabled); 
      }     
    });
    
    $.data(el, "ui-draggable", this);
    
    if (options.ghosting == true) options.helper = 'clone'; //legacy option check
    $(el).addClass("ui-draggable");
    this.interaction = new $.ui.mouseInteraction(el, options);
    
  }
  
  $.extend($.ui.draggable.prototype, {
    plugins: {},
    currentTarget: null,
    lastTarget: null,
    destroy: function() {
      $(this.interaction.element).removeClass("ui-draggable").removeClass("ui-draggable-disabled");
      this.interaction.destroy();
    },
    enable: function() {
      $(this.interaction.element).removeClass("ui-draggable-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.interaction.element).addClass("ui-draggable-disabled");
      this.disabled = true;
    },
    prepareCallbackObj: function(self) {
      return {
        helper: self.helper,
        position: { left: self.pos[0], top: self.pos[1] },
        offset: self.options.cursorAt,
        draggable: self,
        options: self.options 
      }     
    },
    start: function(that, e) {
      
      var o = this.options;
      $.ui.ddmanager.current = this;
      
      $.ui.plugin.call(that, 'start', [e, that.prepareCallbackObj(this)]);
      $(this.element).triggerHandler("dragstart", [e, that.prepareCallbackObj(this)], o.start);
      
      if (this.slowMode && $.ui.droppable && !o.dropBehaviour)
        $.ui.ddmanager.prepareOffsets(this, e);
      
      return false;
            
    },
    stop: function(that, e) {     
      
      var o = this.options;
      
      $.ui.plugin.call(that, 'stop', [e, that.prepareCallbackObj(this)]);
      $(this.element).triggerHandler("dragstop", [e, that.prepareCallbackObj(this)], o.stop);

      if (this.slowMode && $.ui.droppable && !o.dropBehaviour) //If cursorAt is within the helper, we must use our drop manager
        $.ui.ddmanager.fire(this, e);

      $.ui.ddmanager.current = null;
      $.ui.ddmanager.last = this;

      return false;
      
    },
    drag: function(that, e) {

      var o = this.options;

      $.ui.ddmanager.update(this, e);

      this.pos = [this.pos[0]-o.cursorAt.left, this.pos[1]-o.cursorAt.top];

      $.ui.plugin.call(that, 'drag', [e, that.prepareCallbackObj(this)]);
      var nv = $(this.element).triggerHandler("drag", [e, that.prepareCallbackObj(this)], o.drag);

      var nl = (nv && nv.left) ? nv.left : this.pos[0];
      var nt = (nv && nv.top) ? nv.top : this.pos[1];
      
      $(this.helper).css('left', nl+'px').css('top', nt+'px'); // Stick the helper to the cursor
      return false;
      
    }
  });

})($);

// *************************************************************************************************
// *************************************** DRAGGABLE.EXT **********************************************
// *************************************************************************************************

/*
 * 'this' -> original element
 * 1. argument: browser event
 * 2.argument: ui object
 */

(function($) {

  $.ui.plugin.add("draggable", "stop", "effect", function(e,ui) {
    var t = ui.helper;
    if(ui.options.effect[1]) {
      if(t != this) {
        ui.options.beQuietAtEnd = true;
        switch(ui.options.effect[1]) {
          case 'fade':
            $(t).fadeOut(300, function() { $(this).remove(); });
            break;
          default:
            $(t).remove();
            break;  
        }
      }
    }
  });
  
  $.ui.plugin.add("draggable", "start", "effect", function(e,ui) {
    if(ui.options.effect[0]) {
      switch(ui.options.effect[0]) {
        case 'fade':
          $(ui.helper).hide().fadeIn(300);
          break;
      }
    }
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "start", "cursor", function(e,ui) {
    var t = $('body');
    if (t.css("cursor")) ui.options.ocursor = t.css("cursor");
    t.css("cursor", ui.options.cursor);
  });

  $.ui.plugin.add("draggable", "stop", "cursor", function(e,ui) {
    if (ui.options.ocursor) $('body').css("cursor", ui.options.ocursor);
  });

//----------------------------------------------------------------
  
  $.ui.plugin.add("draggable", "start", "zIndex", function(e,ui) {
    var t = $(ui.helper);
    if(t.css("zIndex")) ui.options.ozIndex = t.css("zIndex");
    t.css('zIndex', ui.options.zIndex);
  });
  
  $.ui.plugin.add("draggable", "stop", "zIndex", function(e,ui) {
    if(ui.options.ozIndex) $(ui.helper).css('zIndex', ui.options.ozIndex);
  });


//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "start", "opacity", function(e,ui) {
    var t = $(ui.helper);
    if(t.css("opacity")) ui.options.oopacity = t.css("opacity");
    t.css('opacity', ui.options.opacity);
  });
  
  $.ui.plugin.add("draggable", "stop", "opacity", function(e,ui) {
    if(ui.options.oopacity) $(ui.helper).css('opacity', ui.options.oopacity);
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "stop", "revert", function(e,ui) {
  
    var o = ui.options;
    var rpos = { left: 0, top: 0 };
    o.beQuietAtEnd = true;

    if(ui.helper != this) {

      rpos = $(ui.draggable.sorthelper || this).offset({ border: false });

      var nl = rpos.left-o.po.left-o.margins.left;
      var nt = rpos.top-o.po.top-o.margins.top;

    } else {
      var nl = o.co.left - (o.po ? o.po.left : 0);
      var nt = o.co.top - (o.po ? o.po.top : 0);
    }
    
    var self = ui.draggable;

    $(ui.helper).animate({
      left: nl,
      top: nt
    }, 500, function() {
      
      if(o.wasPositioned) $(self.element).css('position', o.wasPositioned);
      if(o.stop) o.stop.apply(self.element, [self.helper, self.pos, [o.co.left - o.po.left,o.co.top - o.po.top],self]);
      
      if(self.helper != self.element) window.setTimeout(function() { $(self.helper).remove(); }, 0); //Using setTimeout because of strange flickering in Firefox
      
    });
    
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "start", "iframeFix", function(e,ui) {

    var o = ui.options;
    if(!ui.draggable.slowMode) { // Make clones on top of iframes (only if we are not in slowMode)
      if(o.iframeFix.constructor == Array) {
        for(var i=0;i<o.iframeFix.length;i++) {
          var co = $(o.iframeFix[i]).offset({ border: false });
          $("<div class='DragDropIframeFix' style='background: #fff;'></div>").css("width", $(o.iframeFix[i])[0].offsetWidth+"px").css("height", $(o.iframeFix[i])[0].offsetHeight+"px").css("position", "absolute").css("opacity", "0.001").css("z-index", "1000").css("top", co.top+"px").css("left", co.left+"px").appendTo("body");
        }   
      } else {
        $("iframe").each(function() {         
          var co = $(this).offset({ border: false });
          $("<div class='DragDropIframeFix' style='background: #fff;'></div>").css("width", this.offsetWidth+"px").css("height", this.offsetHeight+"px").css("position", "absolute").css("opacity", "0.001").css("z-index", "1000").css("top", co.top+"px").css("left", co.left+"px").appendTo("body");
        });             
      }   
    }

  });
  
  $.ui.plugin.add("draggable","stop", "iframeFix", function(e,ui) {
    if(ui.options.iframeFix) $("div.DragDropIframeFix").each(function() { this.parentNode.removeChild(this); }); //Remove frame helpers 
  });
    
//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "start", "containment", function(e,ui) {

    var o = ui.options;

    if(!o.cursorAtIgnore || o.containment.left != undefined || o.containment.constructor == Array) return;
    if(o.containment == 'parent') o.containment = this.parentNode;


    if(o.containment == 'document') {
      o.containment = [
        0-o.margins.left,
        0-o.margins.top,
        $(document).width()-o.margins.right,
        ($(document).height() || document.body.parentNode.scrollHeight)-o.margins.bottom
      ];
    } else { //I'm a node, so compute top/left/right/bottom
      var ce = $(o.containment)[0];
      var co = $(o.containment).offset({ border: false });

      o.containment = [
        co.left-o.margins.left,
        co.top-o.margins.top,
        co.left+(ce.offsetWidth || ce.scrollWidth)-o.margins.right,
        co.top+(ce.offsetHeight || ce.scrollHeight)-o.margins.bottom
      ];
    }

  });
  
  $.ui.plugin.add("draggable", "drag", "containment", function(e,ui) {
    
    var o = ui.options;
    if(!o.cursorAtIgnore) return;
      
    var h = $(ui.helper);
    var c = o.containment;
    if(c.constructor == Array) {
      
      if((ui.draggable.pos[0] < c[0]-o.po.left)) ui.draggable.pos[0] = c[0]-o.po.left;
      if((ui.draggable.pos[1] < c[1]-o.po.top)) ui.draggable.pos[1] = c[1]-o.po.top;
      if(ui.draggable.pos[0]+h[0].offsetWidth > c[2]-o.po.left) ui.draggable.pos[0] = c[2]-o.po.left-h[0].offsetWidth;
      if(ui.draggable.pos[1]+h[0].offsetHeight > c[3]-o.po.top) ui.draggable.pos[1] = c[3]-o.po.top-h[0].offsetHeight;
      
    } else {

      if(c.left && (ui.draggable.pos[0] < c.left)) ui.draggable.pos[0] = c.left;
      if(c.top && (ui.draggable.pos[1] < c.top)) ui.draggable.pos[1] = c.top;

      var p = $(o.pp);
      if(c.right && ui.draggable.pos[0]+h[0].offsetWidth > p[0].offsetWidth-c.right) ui.draggable.pos[0] = (p[0].offsetWidth-c.right)-h[0].offsetWidth;
      if(c.bottom && ui.draggable.pos[1]+h[0].offsetHeight > p[0].offsetHeight-c.bottom) ui.draggable.pos[1] = (p[0].offsetHeight-c.bottom)-h[0].offsetHeight;
      
    }

    
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "drag", "grid", function(e,ui) {
    var o = ui.options;
    if(!o.cursorAtIgnore) return;
    ui.draggable.pos[0] = o.co.left + o.margins.left - o.po.left + Math.round((ui.draggable.pos[0] - o.co.left - o.margins.left + o.po.left) / o.grid[0]) * o.grid[0];
    ui.draggable.pos[1] = o.co.top + o.margins.top - o.po.top + Math.round((ui.draggable.pos[1] - o.co.top - o.margins.top + o.po.top) / o.grid[1]) * o.grid[1];
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "drag", "axis", function(e,ui) {
    var o = ui.options;
    if(!o.cursorAtIgnore) return;
    if(o.constraint) o.axis = o.constraint; //Legacy check
    o.axis ? ( o.axis == 'x' ? ui.draggable.pos[1] = o.co.top - o.margins.top - o.po.top : ui.draggable.pos[0] = o.co.left - o.margins.left - o.po.left ) : null;
  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "drag", "scroll", function(e,ui) {

    var o = ui.options;
    o.scrollSensitivity = o.scrollSensitivity || 20;
    o.scrollSpeed   = o.scrollSpeed || 20;

    if(o.pp && o.ppOverflow) { // If we have a positioned parent, we only scroll in this one
      // TODO: Extremely strange issues are waiting here..handle with care
    } else {
      if((ui.draggable.rpos[1] - $(window).height()) - $(document).scrollTop() > -o.scrollSensitivity) window.scrollBy(0,o.scrollSpeed);
      if(ui.draggable.rpos[1] - $(document).scrollTop() < o.scrollSensitivity) window.scrollBy(0,-o.scrollSpeed);
      if((ui.draggable.rpos[0] - $(window).width()) - $(document).scrollLeft() > -o.scrollSensitivity) window.scrollBy(o.scrollSpeed,0);
      if(ui.draggable.rpos[0] - $(document).scrollLeft() < o.scrollSensitivity) window.scrollBy(-o.scrollSpeed,0);
    }

  });

//----------------------------------------------------------------

  $.ui.plugin.add("draggable", "drag", "wrapHelper", function(e,ui) {

    var o = ui.options;
    if(o.cursorAtIgnore) return;
    var t = ui.helper;

    if(!o.pp || !o.ppOverflow) {
      var wx = $(window).width() - ($.browser.mozilla ? 20 : 0);
      var sx = $(document).scrollLeft();
      
      var wy = $(window).height();
      var sy = $(document).scrollTop(); 
    } else {
      var wx = o.pp.offsetWidth + o.po.left - 20;
      var sx = o.pp.scrollLeft;
      
      var wy = o.pp.offsetHeight + o.po.top - 20;
      var sy = o.pp.scrollTop;            
    }

    ui.draggable.pos[0] -= ((ui.draggable.rpos[0]-o.cursorAt.left - wx + t.offsetWidth+o.margins.right) - sx > 0 || (ui.draggable.rpos[0]-o.cursorAt.left+o.margins.left) - sx < 0) ? (t.offsetWidth+o.margins.left+o.margins.right - o.cursorAt.left * 2) : 0;
    
    ui.draggable.pos[1] -= ((ui.draggable.rpos[1]-o.cursorAt.top - wy + t.offsetHeight+o.margins.bottom) - sy > 0 || (ui.draggable.rpos[1]-o.cursorAt.top+o.margins.top) - sy < 0) ? (t.offsetHeight+o.margins.top+o.margins.bottom - o.cursorAt.top * 2) : 0;

  });

})(jQuery);


// *************************************************************************************************
// *************************************** DROPPABLE **********************************************
// *************************************************************************************************

(function($) {

  //Make nodes selectable by expression
  $.extend($.expr[':'], { droppable: "(' '+a.className+' ').indexOf(' ui-droppable ')" });

  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-droppable")) jQuery.data(this, "ui-droppable")["'+cur+'"](a); }); }');
    $.fn["droppable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };
  
  //get instance method
  $.fn.droppableInstance = function() {
    if($(this[0]).is(".ui-droppable")) return $.data(this[0], "ui-droppable");
    return false;
  };

  $.fn.droppable = function(o) {
    return this.each(function() {
      new $.ui.droppable(this,o);
    });
  }
  
  $.ui.droppable = function(el,o) {

    if(!o) var o = {};      
    this.element = el; if($.browser.msie) el.droppable = 1;
    $.data(el, "ui-droppable", this);
    
    this.options = {};
    $.extend(this.options, o);
    
    var accept = o.accept;
    $.extend(this.options, {
      accept: o.accept && o.accept.constructor == Function ? o.accept : function(d) {
        return $(d).is(accept); 
      },
      tolerance: o.tolerance || 'intersect'
    });
    o = this.options;
    var self = this;
    
    this.mouseBindings = [function(e) { return self.move.apply(self, [e]); },function(e) { return self.drop.apply(self, [e]); }];
    $(this.element).bind("mousemove", this.mouseBindings[0]);
    $(this.element).bind("mouseup", this.mouseBindings[1]);
    
    $.ui.ddmanager.droppables.push({ item: this, over: 0, out: 1 }); // Add the reference and positions to the manager
    $(this.element).addClass("ui-droppable");
      
  };
  
  $.extend($.ui.droppable.prototype, {
    plugins: {},
    prepareCallbackObj: function(c) {
      return {
        draggable: c,
        droppable: this,
        element: c.element,
        helper: c.helper,
        options: this.options 
      }     
    },
    destroy: function() {
      $(this.element).removeClass("ui-droppable").removeClass("ui-droppable-disabled");
      $(this.element).unbind("mousemove", this.mouseBindings[0]);
      $(this.element).unbind("mouseup", this.mouseBindings[1]);
      
      for(var i=0;i<$.ui.ddmanager.droppables.length;i++) {
        if($.ui.ddmanager.droppables[i].item == this) $.ui.ddmanager.droppables.splice(i,1);
      }
    },
    enable: function() {
      $(this.element).removeClass("ui-droppable-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.element).addClass("ui-droppable-disabled");
      this.disabled = true;
    },
    move: function(e) {

      if(!$.ui.ddmanager.current) return;

      var o = this.options;
      var c = $.ui.ddmanager.current;
      
      /* Save current target, if no last target given */
      var findCurrentTarget = function(e) {
        if(e.currentTarget) return e.currentTarget;
        var el = e.srcElement; 
        do { if(el.droppable) return el; el = el.parentNode; } while (el); //This is only used in IE! references in DOM are evil!
      }
      if(c && o.accept(c.element)) c.currentTarget = findCurrentTarget(e);
      
      c.drag.apply(c, [e]);
      e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
      
    },
    over: function(e) {

      var c = $.ui.ddmanager.current;
      if (!c || c.element == this.element) return; // Bail if draggable and droppable are same element
      
      var o = this.options;
      if (o.accept(c.element)) {
        $.ui.plugin.call(this, 'over', [e, this.prepareCallbackObj(c)]);
        $(this.element).triggerHandler("dropover", [e, this.prepareCallbackObj(c)], o.over);
      }
      
    },
    out: function(e) {

      var c = $.ui.ddmanager.current;
      if (!c || c.element == this.element) return; // Bail if draggable and droppable are same element

      var o = this.options;
      if (o.accept(c.element)) {
        $.ui.plugin.call(this, 'out', [e, this.prepareCallbackObj(c)]);
        $(this.element).triggerHandler("dropout", [e, this.prepareCallbackObj(c)], o.out);
      }
      
    },
    drop: function(e) {

      var c = $.ui.ddmanager.current;
      if (!c || c.element == this.element) return; // Bail if draggable and droppable are same element
      
      var o = this.options;
      if(o.accept(c.element)) { // Fire callback
        if(o.greedy && !c.slowMode) {
          if(c.currentTarget == this.element) {
            $.ui.plugin.call(this, 'drop', [e, {
              draggable: c,
              droppable: this,
              element: c.element,
              helper: c.helper  
            }]);
            $(this.element).triggerHandler("drop", [e, {
              draggable: c,
              droppable: this,
              element: c.element,
              helper: c.helper  
            }], o.drop);
          }
        } else {
          $.ui.plugin.call(this, 'drop', [e, this.prepareCallbackObj(c)]);
          $(this.element).triggerHandler("drop", [e, this.prepareCallbackObj(c)], o.drop);
        }
      }
      
    },
    activate: function(e) {
      var c = $.ui.ddmanager.current;
      $.ui.plugin.call(this, 'activate', [e, this.prepareCallbackObj(c)]);
      if(c) $(this.element).triggerHandler("dropactivate", [e, this.prepareCallbackObj(c)], this.options.activate); 
    },
    deactivate: function(e) {
      var c = $.ui.ddmanager.current;
      $.ui.plugin.call(this, 'deactivate', [e, this.prepareCallbackObj(c)]);
      if(c) $(this.element).triggerHandler("dropdeactivate", [e, this.prepareCallbackObj(c)], this.options.deactivate);
    }
  });
  
  $.ui.intersect = function(oDrag, oDrop, toleranceMode) {
    if (!oDrop.offset)
      return false;
    var x1 = oDrag.rpos[0] - oDrag.options.cursorAt.left + oDrag.options.margins.left, x2 = x1 + oDrag.helperSize.width,
        y1 = oDrag.rpos[1] - oDrag.options.cursorAt.top + oDrag.options.margins.top, y2 = y1 + oDrag.helperSize.height;
    var l = oDrop.offset.left, r = l + oDrop.item.element.offsetWidth, 
        t = oDrop.offset.top,  b = t + oDrop.item.element.offsetHeight;
    switch (toleranceMode) {
      case 'fit':
        return (   l < x1 && x2 < r
          && t < y1 && y2 < b);
        break;
      case 'intersect':
        return (   l < x1 + (oDrag.helperSize.width  / 2)        // Right Half
          &&     x2 - (oDrag.helperSize.width  / 2) < r    // Left Half
          && t < y1 + (oDrag.helperSize.height / 2)        // Bottom Half
          &&     y2 - (oDrag.helperSize.height / 2) < b ); // Top Half
        break;
      case 'pointer':
        return (   l < oDrag.rpos[0] && oDrag.rpos[0] < r
          && t < oDrag.rpos[1] && oDrag.rpos[1] < b);
        break;
      case 'touch':
        return (   (l < x1 && x1 < r && t < y1 && y1 < b)    // Top-Left Corner
          || (l < x1 && x1 < r && t < y2 && y2 < b)    // Bottom-Left Corner
          || (l < x2 && x2 < r && t < y1 && y1 < b)    // Top-Right Corner
          || (l < x2 && x2 < r && t < y2 && y2 < b) ); // Bottom-Right Corner
        break;
      default:
        return false;
        break;
    }
  }
  
})($);

// *************************************************************************************************
// *************************************** DROPPABLE.EXT **********************************************
// *************************************************************************************************

(function($) {
  
  // options.activeClass
  $.ui.plugin.add("droppable", "activate", "activeClass", function(e,ui) {
    $(this).addClass(ui.options.activeClass);
  });
  $.ui.plugin.add("droppable", "deactivate", "activeClass", function(e,ui) {
    $(this).removeClass(ui.options.activeClass);
  });
  $.ui.plugin.add("droppable", "drop", "activeClass", function(e,ui) {
    $(this).removeClass(ui.options.activeClass);
  });

  // options.hoverClass
  $.ui.plugin.add("droppable", "over", "hoverClass", function(e,ui) {
    $(this).addClass(ui.options.hoverClass);
  });
  $.ui.plugin.add("droppable", "out", "hoverClass", function(e,ui) {
    $(this).removeClass(ui.options.hoverClass);
  });
  $.ui.plugin.add("droppable", "drop", "hoverClass", function(e,ui) {
    $(this).removeClass(ui.options.hoverClass);
  });

})(jQuery);

// *************************************************************************************************
// *************************************** RESIZABLE  **********************************************
// *************************************************************************************************

(function($) {

  //Make nodes selectable by expression
  $.extend($.expr[':'], { resizable: "(' '+a.className+' ').indexOf(' ui-resizable ')" });

  
  $.fn.resizable = function(o) {
    return this.each(function() {
      if(!$(this).is(".ui-resizable")) new $.ui.resizable(this,o);  
    });
  }

  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-resizable")) jQuery.data(this, "ui-resizable")["'+cur+'"](a); if(jQuery(this.parentNode).is(".ui-resizable")) jQuery.data(this, "ui-resizable")["'+cur+'"](a); }); }');
    $.fn["resizable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };
  
  //get instance method
  $.fn.resizableInstance = function() {
    if($(this[0]).is(".ui-resizable") || $(this[0].parentNode).is(".ui-resizable")) return $.data(this[0], "ui-resizable");
    return false;
  };
  
  
  $.ui.resizable = function(el,o) {
    
    var options = {}; o = o || {}; $.extend(options, o); //Extend and copy options
    this.element = el; var self = this; //Do bindings
    $.data(this.element, "ui-resizable", this);
    
    if(options.proxy) {
      var helper = function(e,that) {
        var helper = $('<div></div>').css({
          width: $(this).width(),
          height: $(this).height(),
          position: 'absolute',
          left: that.options.co.left,
          top: that.options.co.top
        }).addClass(that.options.proxy);
        return helper;
      } 
    } else {
      var helper = "original";  
    }
    
    //Destructive mode wraps the original element
    if(el.nodeName.match(/textarea|input|select|button|img/i)) options.destructive = true;
    if(options.destructive) {
      
      $(el).wrap('<div class="ui-wrapper"  style="position: relative; width: '+$(el).outerWidth()+'px; height: '+$(el).outerHeight()+';"></div>');
      var oel = el;
      el = el.parentNode; this.element = el;
      
      //Move margins to the wrapper
      $(el).css({ marginLeft: $(oel).css("marginLeft"), marginTop: $(oel).css("marginTop"), marginRight: $(oel).css("marginRight"), marginBottom: $(oel).css("marginBottom")});
      $(oel).css({ marginLeft: 0, marginTop: 0, marginRight: 0, marginBottom: 0});
      
      o.proportionallyResize = o.proportionallyResize || [];
      o.proportionallyResize.push(oel);
      
      var b = [parseInt($(oel).css('borderTopWidth')),parseInt($(oel).css('borderRightWidth')),parseInt($(oel).css('borderBottomWidth')),parseInt($(oel).css('borderLeftWidth'))];
    } else {
      var b = [0,0,0,0];  
    }
    
    if(options.destructive || !$(".ui-resizable-handle",el).length) {
      //Adding handles (disabled not so common ones)
      var t = function(a,b) { $(el).append("<div class='ui-resizable-"+a+" ui-resizable-handle' style='"+b+"'></div>"); };
      //t('n','top: '+b[0]+'px;');
      t('e','right: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
      t('s','bottom: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
      //t('w','left: '+b[3]+'px;');
      t('se','bottom: '+b[2]+'px; right: '+b[1]+'px;'+(options.zIndex ? 'z-index: '+options.zIndex+';' : ''));
      //t('sw','bottom: '+b[2]+'px; left: '+b[3]+'px;');
      //t('ne','top: '+b[0]+'px; right: '+b[1]+'px;');
      //t('nw','top: '+b[0]+'px; left: '+b[3]+'px;');
    }
    
    
    
    //If other elements should be modified, we have to copy that array
    options.modifyThese = [];
    if(o.proportionallyResize) {
      options.proportionallyResize = o.proportionallyResize.slice(0);
      var propRes = options.proportionallyResize;

      for(var i in propRes) {
        
        if(propRes[i].constructor == String)
          propRes[i] = $(propRes[i], el);
        
        if(!$(propRes[i]).length) continue;
        
        
        var x = $(propRes[i]).width() - $(el).width();
        var y = $(propRes[i]).height() - $(el).height();
        options.modifyThese.push([$(propRes[i]),x,y]);
      }

    }
    
    options.handles = {};
    if(!o.handles) o.handles = { n: '.ui-resizable-n', e: '.ui-resizable-e', s: '.ui-resizable-s', w: '.ui-resizable-w', se: '.ui-resizable-se', sw: '.ui-resizable-sw', ne: '.ui-resizable-ne', nw: '.ui-resizable-nw' };
    
    for(var i in o.handles) { options.handles[i] = o.handles[i]; } //Copying the object
    
    for(var i in options.handles) {
      
      if(options.handles[i].constructor == String)
        options.handles[i] = $(options.handles[i], el);
      
      if(!$(options.handles[i]).length) continue;
        
      $(options.handles[i]).bind('mousedown', function(e) {
        self.interaction.options.axis = this.resizeAxis;
      })[0].resizeAxis = i;
      
    }
    
    //If we want to auto hide the elements
    if(o.autohide)
      $(this.element).addClass("ui-resizable-autohide").hover(function() { $(this).removeClass("ui-resizable-autohide"); }, function() { if(self.interaction.options.autohide && !self.interaction.init) $(this).addClass("ui-resizable-autohide"); });
  

    $.extend(options, {
      helper: helper,
      nonDestructive: true,
      dragPrevention: 'input,button,select',
      minHeight: options.minHeight || 50,
      minWidth: options.minWidth || 100,
      startCondition: function(e) {
        if(self.disabled) return false;
        for(var i in options.handles) {
          if($(options.handles[i])[0] == e.target) return true;
        }
        return false;
      },
      _start: function(h,p,c,t,e) {
        self.start.apply(t, [self, e]); // Trigger the start callback       
      },
      _beforeStop: function(h,p,c,t,e) {
        self.stop.apply(t, [self, e]); // Trigger the stop callback
      },
      _drag: function(h,p,c,t,e) {
        self.drag.apply(t, [self, e]); // Trigger the start callback
      }     
    });
    
    //Initialize mouse interaction
    this.interaction = new $.ui.mouseInteraction(el,options);
    
    //Add the class for themeing
    $(this.element).addClass("ui-resizable");
    
  }
  
  $.extend($.ui.resizable.prototype, {
    plugins: {},
    prepareCallbackObj: function(self) {
      return {
        helper: self.helper,
        resizable: self,
        axis: self.options.axis,
        options: self.options
      }     
    },
    destroy: function() {
      $(this.element).removeClass("ui-resizable").removeClass("ui-resizable-disabled");
      this.interaction.destroy();
    },
    enable: function() {
      $(this.element).removeClass("ui-resizable-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.element).addClass("ui-resizable-disabled");
      this.disabled = true;
    },
    start: function(that, e) {
      this.options.originalSize = [$(this.element).width(),$(this.element).height()];
      this.options.originalPosition = $(this.element).css("position");
      this.options.originalPositionValues = $(this.element).position();

      this.options.modifyThese.push([$(this.helper),0,0]);
      
      $(that.element).triggerHandler("resizestart", [e, that.prepareCallbackObj(this)], this.options.start);      
      return false;
    },
    stop: function(that, e) {     
      
      var o = this.options;

      $(that.element).triggerHandler("resizestop", [e, that.prepareCallbackObj(this)], this.options.stop);  

      if(o.proxy) {
        $(this.element).css({
          width: $(this.helper).width(),
          height: $(this.helper).height()
        });
        
        if(o.originalPosition == "absolute" || o.originalPosition == "fixed") {
          $(this.element).css({
            top: $(this.helper).css("top"),
            left: $(this.helper).css("left")
          });         
        }
      }
      return false;
      
    },
    drag: function(that, e) {

      var o = this.options;
      var rel = (o.originalPosition != "absolute" && o.originalPosition != "fixed");
      var co = rel ? o.co : this.options.originalPositionValues;
      var p = o.originalSize;

      this.pos = rel ? [this.rpos[0]-o.cursorAt.left, this.rpos[1]-o.cursorAt.top] : [this.pos[0]-o.cursorAt.left, this.pos[1]-o.cursorAt.top];

      var nw = p[0] + (this.pos[0] - co.left);
      var nh = p[1] + (this.pos[1] - co.top);
    
      if(o.axis) {
        switch(o.axis) {
          case 'e':
            nh = p[1];
            break;
          case 's':
            nw = p[0];
            break;
          case 'n':
          case 'ne':

            
            if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
              return false;
            
            if(o.axis == 'n') nw = p[0];
            var mod = (this.pos[1] - co.top); nh = nh - (mod*2);
            mod = nh <= o.minHeight ? p[1] - o.minHeight : (nh >= o.maxHeight ? 0-(o.maxHeight-p[1]) : mod);
            $(this.helper).css('top', co.top + mod);
            break;
            
          case 'w':
          case 'sw':

            if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
              return false;
            
            if(o.axis == 'w') nh = p[1];
            var mod = (this.pos[0] - co.left); nw = nw - (mod*2);
            mod = nw <= o.minWidth ? p[0] - o.minWidth : (nw >= o.maxWidth ? 0-(o.maxWidth-p[0]) : mod);
            $(this.helper).css('left', co.left + mod);
            break;
            
          case 'nw':
            
            if(!o.proxy && (o.originalPosition != "absolute" && o.originalPosition != "fixed"))
              return false;
  
            var modx = (this.pos[0] - co.left); nw = nw - (modx*2);
            modx = nw <= o.minWidth ? p[0] - o.minWidth : (nw >= o.maxWidth ? 0-(o.maxWidth-p[0]) : modx);
            
            var mody = (this.pos[1] - co.top); nh = nh - (mody*2);
            mody = nh <= o.minHeight ? p[1] - o.minHeight : (nh >= o.maxHeight ? 0-(o.maxHeight-p[1]) : mody);

            $(this.helper).css({
              left: co.left + modx,
              top: co.top + mody
            });
            
            break;
        } 
      }

      if(e.shiftKey) nh = nw * (p[1]/p[0]);
      
      if(o.minWidth) nw = nw <= o.minWidth ? o.minWidth : nw;
      if(o.minHeight) nh = nh <= o.minHeight ? o.minHeight : nh;
      
      if(o.maxWidth) nw = nw >= o.maxWidth ? o.maxWidth : nw;
      if(o.maxHeight) nh = nh >= o.maxHeight ? o.maxHeight : nh;
      
      if(e.shiftKey) nh = nw * (p[1]/p[0]);

      var modifier = $(that.element).triggerHandler("resize", [e, that.prepareCallbackObj(this)], o.resize);
      if(!modifier) modifier = {};
      
      for(var i in this.options.modifyThese) {
        var c = this.options.modifyThese[i];
        c[0].css({
          width: modifier.width ? modifier.width+c[1] : nw+c[1],
          height: modifier.height ? modifier.height+c[2] : nh+c[2]
        });
      }
      return false;
      
    }
  });

})($);

// *************************************************************************************************
// *************************************** DIALOG  **********************************************
// *************************************************************************************************

(function($)
{
  //If the UI scope is not availalable, add it
  $.ui = $.ui || {};

  $.fn.dialog = function(o) {
    return this.each(function() {
      if (!$(this).is(".ui-dialog")) new $.ui.dialog(this, o);
    });
  }
  $.fn.dialogOpen = function() {
    return this.each(function() {
      var contentEl;
      if ($(this).parents(".ui-dialog").length) contentEl = this;
      if (!contentEl && $(this).is(".ui-dialog")) contentEl = $('.ui-dialog-content', this)[0];
      $.ui.dialogOpen(contentEl)
    });
  }
  $.fn.dialogClose = function() {
    return this.each(function() {
      var contentEl;
      if ($(this).parents(".ui-dialog").length) contentEl = this;
      if (!contentEl && $(this).is(".ui-dialog")) contentEl = $('.ui-dialog-content', this)[0];
      $.ui.dialogClose(contentEl);
    });
  }

  $.ui.dialog = function(el, o) {
    
    var options = {
      width: 300,
      height: 200,
      position: 'center',
      buttons: [],
      modal: false,
      drag: true,
      resize: true,
      shadow: false // It's quite slow
    };
    var o = o || {}; $.extend(options, o); //Extend and copy options
    this.element = el; var self = this; //Do bindings
    $.data(this.element, "ui-dialog", this);

    var uiDialogContent = $(el).addClass('ui-dialog-content')
      .wrap(document.createElement('div'))
      .wrap(document.createElement('div'));
    var uiDialogContainer = uiDialogContent.parent().addClass('ui-dialog-container').css({position: 'relative'});
    var uiDialog = uiDialogContainer.parent()
      .addClass('ui-dialog').addClass(uiDialogContent.attr('className'))
      .css({position: 'absolute', width: options.width, height: options.height});
    
        if (options.modal == false && options.resize == true) {
        uiDialog.append("<div class='ui-resizable-n ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-s ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-e ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-w ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-ne ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-se ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-sw ui-resizable-handle'></div>")
                .append("<div class='ui-resizable-nw ui-resizable-handle'></div>");
      
        uiDialog.resizable();
    }

    uiDialogContainer.prepend('<div class="ui-dialog-titlebar"></div>');
    var uiDialogTitlebar = $('.ui-dialog-titlebar', uiDialogContainer);
    var title = (options.title) ? options.title : (uiDialogContent.attr('title')) ? uiDialogContent.attr('title') : '';
    uiDialogTitlebar.append('<span class="ui-dialog-title">' + title + '</span>');
    uiDialogTitlebar.append('<div class="ui-dialog-titlebar-close"></div>');
    $('.ui-dialog-titlebar-close', uiDialogTitlebar)
      .hover(function() { $(this).addClass('ui-dialog-titlebar-close-hover'); }, 
             function() { $(this).removeClass('ui-dialog-titlebar-close-hover'); })
      .mousedown(function(ev) {
        ev.stopPropagation();
      })
      .click(function() {
        self.close();
      });
    var l = 0;
    $.each(options.buttons, function() { l = 1; return false; });
        if (l == 1) {
        uiDialog.append('<div class="ui-dialog-buttonpane"></div>');
        var uiDialogButtonPane = $('.ui-dialog-buttonpane', uiDialog);
        $.each(options.buttons, function(name, value) {
          var btn = $(document.createElement('button')).text(name).click(value);
          uiDialogButtonPane.append(btn);
        });
    }
        
        if (options.modal == false && options.drag == true) {
        uiDialog.draggable({ handle: '.ui-dialog-titlebar' });
        }
        
    this.open = function() {
      var wnd = $(window), top = 0, left = 0;
      switch (options.position) {
        case 'center':
          top = (wnd.height() / 2) - (uiDialog.height() / 2);
          left = (wnd.width() / 2) - (uiDialog.width() / 2);
          break;
        case 'left':
            top = (wnd.height() / 2) - (uiDialog.height() / 2);
            left = 0;
            break;
        case 'top':
              top = 0;
          left = (wnd.width() / 2) - (uiDialog.width() / 2);
          break;
      }
      uiDialog.css({top: top, left: left});
      uiDialog.appendTo('body').show();
    };

    this.close = function() {
      uiDialog.hide();
    };

    uiDialog.show();
    this.open();
        if (options.shadow && $.fn.shadow != undefined) {
            uiDialog.shadow();
        }
  }

  $.ui.dialogOpen = function(el) {
    $.data(el, "ui-dialog").open();
  }

  $.ui.dialogClose = function(el) {
    $.data(el, "ui-dialog").close();
  }

})(jQuery);


// *************************************************************************************************
// *************************************** ACCORDION  **********************************************
// *************************************************************************************************
/*
 * Accordion 1.5 - jQuery menu widget
 *
 * Copyright (c) 2007 Jörn Zaefferer, Frank Marcia
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-accordion/
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 *
 */

(function($) {

$.ui = $.ui || {}

$.ui.accordion = {};
$.extend($.ui.accordion, {
  defaults: {
    selectedClass: "selected",
    alwaysOpen: true,
    animated: 'slide',
    event: "click",
    header: "a"
  },
  animations: {
    slide: function(settings, additions) {
      settings = $.extend({
        easing: "swing",
        duration: 300
      }, settings, additions);
      if ( !settings.toHide.size() ) {
        settings.toShow.animate({height: "show"}, {
          duration: settings.duration,
          easing: settings.easing,
          complete: settings.finished
        });
        return;
      }
      var hideHeight = settings.toHide.height(),
        showHeight = settings.toShow.height(),
        difference = showHeight / hideHeight;
      settings.toShow.css({ height: 0, overflow: 'hidden' }).show();
      settings.toHide.filter(":hidden").each(settings.finished).end().filter(":visible").animate({height:"hide"},{
        step: function(n){
          settings.toShow.height(Math.ceil( (hideHeight - (n)) * difference ));
        },
        duration: settings.duration,
        easing: settings.easing,
        complete: settings.finished
      });
    },
    bounceslide: function(settings) {
      this.slide(settings, {
        easing: settings.down ? "bounceout" : "swing",
        duration: settings.down ? 1000 : 200
      });
    },
    easeslide: function(settings) {
      this.slide(settings, {
        easing: "easeinout",
        duration: 700
      })
    }
  }
});

$.fn.extend({
  nextUntil: function(expr) {
      var match = [];
  
      // We need to figure out which elements to push onto the array
      this.each(function(){
          // Traverse through the sibling nodes
          for( var i = this.nextSibling; i; i = i.nextSibling ) {
              // Make sure that we're only dealing with elements
              if ( i.nodeType != 1 ) continue;
  
              // If we find a match then we need to stop
              if ( $.filter( expr, [i] ).r.length ) break;
  
              // Otherwise, add it on to the stack
              match.push( i );
          }
      });
  
      return this.pushStack( match );
  },
  // the plugin method itself
  accordion: function(settings) {
    if ( !this.length )
      return this;
  
    // setup configuration
    settings = $.extend({}, $.ui.accordion.defaults, settings);
    
    if ( settings.navigation ) {
      var current = this.find("a").filter(function() { return this.href == location.href; });
      if ( current.length ) {
        if ( current.filter(settings.header).length ) {
          settings.active = current;
        } else {
          settings.active = current.parent().parent().prev();
          current.addClass("current");
        }
      }
    }
    
    // calculate active if not specified, using the first header
    var container = this,
      headers = container.find(settings.header),
      active = findActive(settings.active),
      running = 0;

    if ( settings.fillSpace ) {
      var maxHeight = this.parent().height();
      headers.each(function() {
        maxHeight -= $(this).outerHeight();
      });
      var maxPadding = 0;
      headers.nextUntil(settings.header).each(function() {
        maxPadding = Math.max(maxPadding, $(this).innerHeight() - $(this).height());
      }).height(maxHeight - maxPadding);
    } else if ( settings.autoheight ) {
      var maxHeight = 0;
      headers.nextUntil(settings.header).each(function() {
        maxHeight = Math.max(maxHeight, $(this).height());
      }).height(maxHeight);
    }

    headers
      .not(active || "")
      .nextUntil(settings.header)
      .hide();
    active.parent().andSelf().addClass(settings.selectedClass);
    
    
    function findActive(selector) {
      return selector != undefined
        ? typeof selector == "number"
          ? headers.filter(":eq(" + selector + ")")
          : headers.not(headers.not(selector))
        : selector === false
          ? $("<div>")
          : headers.filter(":eq(0)");
    }
    
    function toggle(toShow, toHide, data, clickedActive, down) {
      var finished = function(cancel) {
        running = cancel ? 0 : --running;
        if ( running )
          return;
        // trigger custom change event
        container.trigger("change", data);
      };
      
      // count elements to animate
      running = toHide.size() == 0 ? toShow.size() : toHide.size();
      
      if ( settings.animated ) {
        if ( !settings.alwaysOpen && clickedActive ) {
          toShow.slideToggle(settings.animated);
          finished(true);
        } else {
          $.ui.accordion.animations[settings.animated]({
            toShow: toShow,
            toHide: toHide,
            finished: finished,
            down: down
          });
        }
      } else {
        if ( !settings.alwaysOpen && clickedActive ) {
          toShow.toggle();
        } else {
          toHide.hide();
          toShow.show();
        }
        finished(true);
      }
    }
    
    function clickHandler(event) {
      // called only when using activate(false) to close all parts programmatically
      if ( !event.target && !settings.alwaysOpen ) {
        active.toggleClass(settings.selectedClass);
        var toHide = active.nextUntil(settings.header);
        var toShow = active = $([]);
        toggle( toShow, toHide );
        return;
      }
      // get the click target
      var clicked = $(event.target);
      
      // due to the event delegation model, we have to check if one
      // of the parent elements is our actual header, and find that
      if ( clicked.parents(settings.header).length )
        while ( !clicked.is(settings.header) )
          clicked = clicked.parent();
      
      var clickedActive = clicked[0] == active[0];
      
      // if animations are still active, or the active header is the target, ignore click
      if(running || (settings.alwaysOpen && clickedActive) || !clicked.is(settings.header))
        return;

      // switch classes
      active.parent().andSelf().toggleClass(settings.selectedClass);
      if ( !clickedActive ) {
        clicked.parent().andSelf().addClass(settings.selectedClass);
      }

      // find elements to show and hide
      var toShow = clicked.nextUntil(settings.header),
        toHide = active.nextUntil(settings.header),
        data = [clicked, active, toShow, toHide],
        down = headers.index( active[0] ) > headers.index( clicked[0] );
      
      active = clickedActive ? $([]) : clicked;
      toggle( toShow, toHide, data, clickedActive, down );

      return !toShow.length;
    };
    function activateHandler(event, index) {
      // IE manages to call activateHandler on normal clicks
      if ( arguments.length == 1 )
        return;
      // call clickHandler with custom event
      clickHandler({
        target: findActive(index)[0]
      });
    };

    return container
      .bind(settings.event, clickHandler)
      .bind("activate", activateHandler);
  },
  activate: function(index) {
    return this.trigger('activate', [index]);
  }
});

})(jQuery);

// *************************************************************************************************
// *************************************** CALENDAR   **********************************************
// *************************************************************************************************

/* jQuery Calendar v2.7
   Written by Marc Grabanski (m@marcgrabanski.com) and enhanced by Keith Wood (kbwood@iprimus.com.au).

   Copyright (c) 2007 Marc Grabanski (http://marcgrabanski.com/code/jquery-calendar)
   Dual licensed under the GPL (http://www.gnu.org/licenses/gpl-3.0.txt) and 
   CC (http://creativecommons.org/licenses/by/3.0/) licenses. "Share or Remix it but please Attribute the authors."
   Date: 09-03-2007  */

(function($) {

/* PopUp Calendar manager.
   Use the singleton instance of this class, popUpCal, to interact with the calendar.
   Settings for (groups of) calendars are maintained in an instance object
   (PopUpCalInstance), allowing multiple different settings on the same page. */
function PopUpCal() {
  this._nextId = 0; // Next ID for a calendar instance
  this._inst = []; // List of instances indexed by ID
  this._curInst = null; // The current instance in use
  this._disabledInputs = []; // List of calendar inputs that have been disabled
  this._popUpShowing = false; // True if the popup calendar is showing , false if not
  this._inDialog = false; // True if showing within a "dialog", false if not
  this.regional = []; // Available regional settings, indexed by language code
  this.regional[''] = { // Default regional settings
    clearText: 'Clear', // Display text for clear link
    closeText: 'Close', // Display text for close link
    prevText: '&lt;Prev', // Display text for previous month link
    nextText: 'Next&gt;', // Display text for next month link
    currentText: 'Today', // Display text for current month link
    dayNames: ['Su','Mo','Tu','We','Th','Fr','Sa'], // Names of days starting at Sunday
    monthNames: ['January','February','March','April','May','June',
      'July','August','September','October','November','December'], // Names of months
    dateFormat: 'DMY/' // First three are day, month, year in the required order,
      // fourth (optional) is the separator, e.g. US would be 'MDY/', ISO would be 'YMD-'
  };
  this._defaults = { // Global defaults for all the calendar instances
    autoPopUp: 'focus', // 'focus' for popup on focus,
      // 'button' for trigger button, or 'both' for either
    defaultDate: null, // Used when field is blank: actual date,
      // +/-number for offset from today, null for today
    appendText: '', // Display text following the input box, e.g. showing the format
    buttonText: '...', // Text for trigger button
    buttonImage: '', // URL for trigger button image
    buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
    closeAtTop: true, // True to have the clear/close at the top,
      // false to have them at the bottom
    hideIfNoPrevNext: false, // True to hide next/previous month links
      // if not applicable, false to just disable them
    changeMonth: true, // True if month can be selected directly, false if only prev/next
    changeYear: true, // True if year can be selected directly, false if only prev/next
    yearRange: '-10:+10', // Range of years to display in drop-down,
      // either relative to current year (-nn:+nn) or absolute (nnnn:nnnn)
    firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
    changeFirstDay: true, // True to click on day name to change, false to remain as set
    showOtherMonths: false, // True to show dates in other months, false to leave blank
    minDate: null, // The earliest selectable date, or null for no limit
    maxDate: null, // The latest selectable date, or null for no limit
    speed: 'medium', // Speed of display/closure
    customDate: null, // Function that takes a date and returns an array with
      // [0] = true if selectable, false if not,
      // [1] = custom CSS class name(s) or '', e.g. popUpCal.noWeekends
    fieldSettings: null, // Function that takes an input field and
      // returns a set of custom settings for the calendar
    onSelect: null // Define a callback function when a date is selected
  };
  $.extend(this._defaults, this.regional['']);
  this._calendarDiv = $('<div id="calendar_div"></div>');
  $(document.body).append(this._calendarDiv);
  $(document.body).mousedown(this._checkExternalClick);
}

$.extend(PopUpCal.prototype, {
  /* Class name added to elements to indicate already configured with a calendar. */
  markerClassName: 'hasCalendar',
  
  /* Register a new calendar instance - with custom settings. */
  _register: function(inst) {
    var id = this._nextId++;
    this._inst[id] = inst;
    return id;
  },

  /* Retrieve a particular calendar instance based on its ID. */
  _getInst: function(id) {
    return this._inst[id] || id;
  },

  /* Override the default settings for all instances of the calendar. 
     @param  settings  object - the new settings to use as defaults (anonymous object)
     @return void */
  setDefaults: function(settings) {
    extendRemove(this._defaults, settings || {});
  },

  /* Handle keystrokes. */
  _doKeyDown: function(e) {
    var inst = popUpCal._getInst(this._calId);
    if (popUpCal._popUpShowing) {
      switch (e.keyCode) {
        case 9:  popUpCal.hideCalendar(inst, '');
            break; // hide on tab out
        case 13: popUpCal._selectDate(inst);
            break; // select the value on enter
        case 27: popUpCal.hideCalendar(inst, inst._get('speed'));
            break; // hide on escape
        case 33: popUpCal._adjustDate(inst, -1, (e.ctrlKey ? 'Y' : 'M'));
            break; // previous month/year on page up/+ ctrl
        case 34: popUpCal._adjustDate(inst, +1, (e.ctrlKey ? 'Y' : 'M'));
            break; // next month/year on page down/+ ctrl
        case 35: if (e.ctrlKey) popUpCal._clearDate(inst);
            break; // clear on ctrl+end
        case 36: if (e.ctrlKey) popUpCal._gotoToday(inst);
            break; // current on ctrl+home
        case 37: if (e.ctrlKey) popUpCal._adjustDate(inst, -1, 'D');
            break; // -1 day on ctrl+left
        case 38: if (e.ctrlKey) popUpCal._adjustDate(inst, -7, 'D');
            break; // -1 week on ctrl+up
        case 39: if (e.ctrlKey) popUpCal._adjustDate(inst, +1, 'D');
            break; // +1 day on ctrl+right
        case 40: if (e.ctrlKey) popUpCal._adjustDate(inst, +7, 'D');
            break; // +1 week on ctrl+down
      }
    }
    else if (e.keyCode == 36 && e.ctrlKey) { // display the calendar on ctrl+home
      popUpCal.showFor(this);
    }
  },

  /* Filter entered characters. */
  _doKeyPress: function(e) {
    var inst = popUpCal._getInst(this._calId);
    var chr = String.fromCharCode(e.charCode == undefined ? e.keyCode : e.charCode);
    return (chr < ' ' || chr == inst._get('dateFormat').charAt(3) ||
      (chr >= '0' && chr <= '9')); // only allow numbers and separator
  },

  /* Attach the calendar to an input field. */
  _connectCalendar: function(target, inst) {
    var input = $(target);
    if (this._hasClass(input, this.markerClassName)) {
      return;
    }
    var appendText = inst._get('appendText');
    if (appendText) {
      input.after('<span class="calendar_append">' + appendText + '</span>');
    }
    var autoPopUp = inst._get('autoPopUp');
    if (autoPopUp == 'focus' || autoPopUp == 'both') { // pop-up calendar when in the marked field
      input.focus(this.showFor);
    }
    if (autoPopUp == 'button' || autoPopUp == 'both') { // pop-up calendar when button clicked
      var buttonText = inst._get('buttonText');
      var buttonImage = inst._get('buttonImage');
      var buttonImageOnly = inst._get('buttonImageOnly');
      var trigger = $(buttonImageOnly ? '<img class="calendar_trigger" src="' +
        buttonImage + '" alt="' + buttonText + '" title="' + buttonText + '"/>' :
        '<button type="button" class="calendar_trigger">' + (buttonImage != '' ?
        '<img src="' + buttonImage + '" alt="' + buttonText + '" title="' + buttonText + '"/>' :
        buttonText) + '</button>');
      input.wrap('<span class="calendar_wrap"></span>').after(trigger);
      trigger.click(this.showFor);
    }
    input.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress);
    input[0]._calId = inst._id;
  },

  /* Attach an inline calendar to a div. */
  _inlineCalendar: function(target, inst) {
    var input = $(target);
    if (this._hasClass(input, this.markerClassName)) {
      return;
    }
    input.addClass(this.markerClassName).append(inst._calendarDiv);
    input[0]._calId = inst._id;
  },

  /* Does this element have a particular class? */
  _hasClass: function(element, className) {
    var classes = element.attr('class');
    return (classes && classes.indexOf(className) > -1);
  },

  /* Pop-up the calendar in a "dialog" box.
     @param  dateText  string - the initial date to display (in the current format)
     @param  onSelect  function - the function(dateText) to call when a date is selected
     @param  settings  object - update the dialog calendar instance's settings (anonymous object)
     @param  pos       int[2] - coordinates for the dialog's position within the screen
      leave empty for default (screen centre)
     @return void */
  dialogCalendar: function(dateText, onSelect, settings, pos) {
    var inst = this._dialogInst; // internal instance
    if (!inst) {
      inst = this._dialogInst = new PopUpCalInstance({}, false);
      this._dialogInput = $('<input type="text" size="1" style="position: absolute; top: -100px;"/>');
      this._dialogInput.keydown(this._doKeyDown);
      $('body').append(this._dialogInput);
      this._dialogInput[0]._calId = inst._id;
    }
    extendRemove(inst._settings, settings || {});
    this._dialogInput.val(dateText);
    
    /*  Cross Browser Positioning */
    if (self.innerHeight) { // all except Explorer
      windowWidth = self.innerWidth;
      windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
      windowWidth = document.documentElement.clientWidth;
      windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
      windowWidth = document.body.clientWidth;
      windowHeight = document.body.clientHeight;
    } 
    this._pos = pos || // should use actual width/height below
      [(windowWidth / 2) - 100, (windowHeight / 2) - 100];

    // move input on screen for focus, but hidden behind dialog
    this._dialogInput.css('left', this._pos[0] + 'px').css('top', this._pos[1] + 'px');
    inst._settings.onSelect = onSelect;
    this._inDialog = true;
    this._calendarDiv.addClass('calendar_dialog');
    this.showFor(this._dialogInput[0]);
    if ($.blockUI) {
      $.blockUI(this._calendarDiv);
    }
  },

  /* Enable the input field(s) for entry.
     @param  inputs  element/object - single input field or jQuery collection of input fields
     @return void */
  enableFor: function(inputs) {
    inputs = (inputs.jquery ? inputs : $(inputs));
    inputs.each(function() {
      this.disabled = false;
      $('../button.calendar_trigger', this).each(function() { this.disabled = false; });
      $('../img.calendar_trigger', this).css({opacity:'1.0',cursor:''});
      var $this = this;
      popUpCal._disabledInputs = $.map(popUpCal._disabledInputs,
        function(value) { return (value == $this ? null : value); }); // delete entry
    });
  },

  /* Disable the input field(s) from entry.
     @param  inputs  element/object - single input field or jQuery collection of input fields
     @return void */
  disableFor: function(inputs) {
    inputs = (inputs.jquery ? inputs : $(inputs));
    inputs.each(function() {
      this.disabled = true;
      $('../button.calendar_trigger', this).each(function() { this.disabled = true; });
      $('../img.calendar_trigger', this).css({opacity:'0.5',cursor:'default'});
      var $this = this;
      popUpCal._disabledInputs = $.map(popUpCal._disabledInputs,
        function(value) { return (value == $this ? null : value); }); // delete entry
      popUpCal._disabledInputs[popUpCal._disabledInputs.length] = this;
    });
  },

  /* Update the settings for a calendar attached to an input field or division.
     @param  control   element - the input field or div/span attached to the calendar or
                       string - the ID or other jQuery selector of the input field
     @param  settings  object - the new settings to update
     @return void */
  reconfigureFor: function(control, settings) {
    control = (typeof control == 'string' ? $(control)[0] : control);
    var inst = this._getInst(control._calId);
    if (inst) {
      extendRemove(inst._settings, settings || {});
      this._updateCalendar(inst);
    }
  },

  /* Set the date for a calendar attached to an input field or division.
     @param  control  element - the input field or div/span attached to the calendar
     @param  date     Date - the new date
     @return void */
  setDateFor: function(control, date) {
    var inst = this._getInst(control._calId);
    if (inst) {
      inst._setDate(date);
    }
  },

  /* Retrieve the date for a calendar attached to an input field or division.
     @param  control  element - the input field or div/span attached to the calendar
     @return Date - the current date */
  getDateFor: function(control) {
    var inst = this._getInst(control._calId);
    return (inst ? inst._getDate() : null);
  },

  /* Pop-up the calendar for a given input field.
     @param  target  element - the input field attached to the calendar
     @return void */
  showFor: function(target) {
    var input = (target.nodeName && target.nodeName.toLowerCase() == 'input' ? target : this);
    if (input.nodeName.toLowerCase() != 'input') { // find from button/image trigger
      input = $('input', input.parentNode)[0];
    }
    if (popUpCal._lastInput == input) { // already here
      return;
    }
    for (var i = 0; i < popUpCal._disabledInputs.length; i++) {  // check not disabled
      if (popUpCal._disabledInputs[i] == input) {
        return;
      }
    }
    var inst = popUpCal._getInst(input._calId);
    var fieldSettings = inst._get('fieldSettings');
    extendRemove(inst._settings, (fieldSettings ? fieldSettings(input) : {}));
    popUpCal.hideCalendar(inst, '');
    popUpCal._lastInput = input;
    inst._setDateFromField(input);
    if (popUpCal._inDialog) { // hide cursor
      input.value = '';
    }
    if (!popUpCal._pos) { // position below input
      popUpCal._pos = popUpCal._findPos(input);
      popUpCal._pos[1] += input.offsetHeight;
    }
    inst._calendarDiv.css('position', (popUpCal._inDialog && $.blockUI ? 'static' : 'absolute')).
      css('left', popUpCal._pos[0] + 'px').css('top', popUpCal._pos[1] + 'px');
    popUpCal._pos = null;
    popUpCal._showCalendar(inst);
  },

  /* Construct and display the calendar. */
  _showCalendar: function(id) {
    var inst = this._getInst(id);
    popUpCal._updateCalendar(inst);
    if (!inst._inline) {
      var speed = inst._get('speed');
      inst._calendarDiv.show(speed, function() {
        popUpCal._popUpShowing = true;
        popUpCal._afterShow(inst);
      });
      if (speed == '') {
        popUpCal._popUpShowing = true;
        popUpCal._afterShow(inst);
      }
      if (inst._input[0].type != 'hidden') {
        inst._input[0].focus();
      }
      this._curInst = inst;
    }
  },

  /* Generate the calendar content. */
  _updateCalendar: function(inst) {
    inst._calendarDiv.empty().append(inst._generateCalendar());
    if (inst._input && inst._input[0].type != 'hidden') {
      inst._input[0].focus();
    }
  },

  /* Tidy up after displaying the calendar. */
  _afterShow: function(inst) {
    if ($.browser.msie) { // fix IE < 7 select problems
      $('#calendar_cover').css({width: inst._calendarDiv[0].offsetWidth + 4,
        height: inst._calendarDiv[0].offsetHeight + 4});
    }
    // re-position on screen if necessary
    var calDiv = inst._calendarDiv[0];
    var pos = popUpCal._findPos(inst._input[0]);
    // Get browser width and X value (IE6+, FF, Safari, Opera)
    if( typeof( window.innerWidth ) == 'number' ) {
      browserWidth = window.innerWidth;
    } else {
      browserWidth = document.documentElement.clientWidth;
    }
    if ( document.documentElement && (document.documentElement.scrollLeft)) {
      browserX = document.documentElement.scrollLeft; 
    } else {
      browserX = document.body.scrollLeft;
    }
    // Reposition calendar if outside the browser window.
    if ((calDiv.offsetLeft + calDiv.offsetWidth) >
        (browserWidth + browserX) ) {
      inst._calendarDiv.css('left', (pos[0] + inst._input[0].offsetWidth - calDiv.offsetWidth) + 'px');
    }
    // Get browser height and Y value (IE6+, FF, Safari, Opera)
    if( typeof( window.innerHeight ) == 'number' ) {
      browserHeight = window.innerHeight;
    } else {
      browserHeight = document.documentElement.clientHeight;
    }
    if ( document.documentElement && (document.documentElement.scrollTop)) {
      browserTopY = document.documentElement.scrollTop;
    } else {
      browserTopY = document.body.scrollTop;
    }
    // Reposition calendar if outside the browser window.
    if ((calDiv.offsetTop + calDiv.offsetHeight) >
        (browserTopY + browserHeight) ) {
      inst._calendarDiv.css('top', (pos[1] - calDiv.offsetHeight) + 'px');
    }
  },

  /* Hide the calendar from view.
     @param  id     string/object - the ID of the current calendar instance,
      or the instance itself
     @param  speed  string - the speed at which to close the calendar
     @return void */
  hideCalendar: function(id, speed) {
    var inst = this._getInst(id);
    if (popUpCal._popUpShowing) {
      speed = (speed != null ? speed : inst._get('speed'));
      inst._calendarDiv.hide(speed, function() {
        popUpCal._tidyDialog(inst);
      });
      if (speed == '') {
        popUpCal._tidyDialog(inst);
      }
      popUpCal._popUpShowing = false;
      popUpCal._lastInput = null;
      inst._settings.prompt = null;
      if (popUpCal._inDialog) {
        popUpCal._dialogInput.css('position', 'absolute').
          css('left', '0px').css('top', '-100px');
        if ($.blockUI) {
          $.unblockUI();
          $('body').append(this._calendarDiv);
        }
      }
      popUpCal._inDialog = false;
    }
    popUpCal._curInst = null;
  },

  /* Tidy up after a dialog display. */
  _tidyDialog: function(inst) {
    inst._calendarDiv.removeClass('calendar_dialog');
    $('.calendar_prompt', inst._calendarDiv).remove();
  },

  /* Close calendar if clicked elsewhere. */
  _checkExternalClick: function(event) {
    if (!popUpCal._curInst) {
      return;
    }
    var target = $(event.target);
    if( (target.parents("#calendar_div").length == 0)
      && (target.attr('class') != 'calendar_trigger')
      && popUpCal._popUpShowing 
      && !(popUpCal._inDialog && $.blockUI) )
    {
      popUpCal.hideCalendar(popUpCal._curInst, '');
    }
  },

  /* Adjust one of the date sub-fields. */
  _adjustDate: function(id, offset, period) {
    var inst = this._getInst(id);
    inst._adjustDate(offset, period);
    this._updateCalendar(inst);
  },

  /* Action for current link. */
  _gotoToday: function(id) {
    var date = new Date();
    var inst = this._getInst(id);
    inst._selectedDay = date.getDate();
    inst._selectedMonth = date.getMonth();
    inst._selectedYear = date.getFullYear();
    this._adjustDate(inst);
  },

  /* Action for selecting a new month/year. */
  _selectMonthYear: function(id, select, period) {
    var inst = this._getInst(id);
    inst._selectingMonthYear = false;
    inst[period == 'M' ? '_selectedMonth' : '_selectedYear'] =
      select.options[select.selectedIndex].value - 0;
    this._adjustDate(inst);
  },

  /* Restore input focus after not changing month/year. */
  _clickMonthYear: function(id) {
    var inst = this._getInst(id);
    if (inst._input && inst._selectingMonthYear && !$.browser.msie) {
      inst._input[0].focus();
    }
    inst._selectingMonthYear = !inst._selectingMonthYear;
  },

  /* Action for changing the first week day. */
  _changeFirstDay: function(id, a) {
    var inst = this._getInst(id);
    var dayNames = inst._get('dayNames');
    var value = a.firstChild.nodeValue;
    for (var i = 0; i < 7; i++) {
      if (dayNames[i] == value) {
        inst._settings.firstDay = i;
        break;
      }
    }
    this._updateCalendar(inst);
  },

  /* Action for selecting a day. */
  _selectDay: function(id, td) {
    var inst = this._getInst(id);
    inst._selectedDay = $("a", td).html();
    this._selectDate(id);
  },

  /* Erase the input field and hide the calendar. */
  _clearDate: function(id) {
    this._selectDate(id, '');
  },

  /* Update the input field with the selected date. */
  _selectDate: function(id, dateStr) {
    var inst = this._getInst(id);
    dateStr = (dateStr != null ? dateStr : inst._formatDate());
    if (inst._input) {
      inst._input.val(dateStr);
    }
    var onSelect = inst._get('onSelect');
    if (onSelect) {
      onSelect(dateStr, inst);  // trigger custom callback
    }
    else {
      inst._input.trigger('change'); // fire the change event
    }
    if (inst._inline) {
      this._updateCalendar(inst);
    }
    else {
      this.hideCalendar(inst, inst._get('speed'));
    }
  },

  /* Set as customDate function to prevent selection of weekends.
     @param  date  Date - the date to customise
     @return [boolean, string] - is this date selectable?, what is its CSS class? */
  noWeekends: function(date) {
    var day = date.getDay();
    return [(day > 0 && day < 6), ''];
  },

  /* Find an object's position on the screen. */
  _findPos: function(obj) {
    while (obj && (obj.type == 'hidden' || obj.nodeType != 1)) {
      obj = obj.nextSibling;
    }
    var curleft = curtop = 0;
    if (obj && obj.offsetParent) {
      curleft = obj.offsetLeft;
      curtop = obj.offsetTop;
      while (obj = obj.offsetParent) {
        var origcurleft = curleft;
        curleft += obj.offsetLeft;
        if (curleft < 0) {
          curleft = origcurleft;
        }
        curtop += obj.offsetTop;
      }
    }
    return [curleft,curtop];
  }
});

/* Individualised settings for calendars applied to one or more related inputs.
   Instances are managed and manipulated through the PopUpCal manager. */
function PopUpCalInstance(settings, inline) {
  this._id = popUpCal._register(this);
  this._selectedDay = 0;
  this._selectedMonth = 0; // 0-11
  this._selectedYear = 0; // 4-digit year
  this._input = null; // The attached input field
  this._inline = inline; // True if showing inline, false if used in a popup
  this._calendarDiv = (!inline ? popUpCal._calendarDiv :
    $('<div id="calendar_div_' + this._id + '" class="calendar_inline"></div>'));
  // customise the calendar object - uses manager defaults if not overridden
  this._settings = extendRemove({}, settings || {}); // clone
  if (inline) {
    this._setDate(this._getDefaultDate());
  }
}

$.extend(PopUpCalInstance.prototype, {
  /* Get a setting value, defaulting if necessary. */
  _get: function(name) {
    return (this._settings[name] != null ? this._settings[name] : popUpCal._defaults[name]);
  },

  /* Parse existing date and initialise calendar. */
  _setDateFromField: function(input) {
    this._input = $(input);
    var dateFormat = this._get('dateFormat');
    var currentDate = this._input.val().split(dateFormat.charAt(3));
    if (currentDate.length == 3) {
      this._currentDay = parseInt(currentDate[dateFormat.indexOf('D')], 10);
      this._currentMonth = parseInt(currentDate[dateFormat.indexOf('M')], 10) - 1;
      this._currentYear = parseInt(currentDate[dateFormat.indexOf('Y')], 10);
    }
    else {
      var date = this._getDefaultDate();
      this._currentDay = date.getDate();
      this._currentMonth = date.getMonth();
      this._currentYear = date.getFullYear();
    }
    this._selectedDay = this._currentDay;
    this._selectedMonth = this._currentMonth;
    this._selectedYear = this._currentYear;
    this._adjustDate();
  },
  
  /* Retrieve the default date shown on opening. */
  _getDefaultDate: function() {
    var offsetDate = function(offset) {
      var date = new Date();
      date.setDate(date.getDate() + offset);
      return date;
    };
    var defaultDate = this._get('defaultDate');
    return (defaultDate == null ? new Date() :
      (typeof defaultDate == 'number' ? offsetDate(defaultDate) : defaultDate));
  },

  /* Set the date directly. */
  _setDate: function(date) {
    this._selectedDay = this._currentDay = date.getDate();
    this._selectedMonth = this._currentMonth = date.getMonth();
    this._selectedYear = this._currentYear = date.getFullYear();
    this._adjustDate();
  },

  /* Retrieve the date directly. */
  _getDate: function() {
    return new Date(this._currentYear, this._currentMonth, this._currentDay);
  },

  /* Generate the HTML for the current state of the calendar. */
  _generateCalendar: function() {
    var today = new Date();
    today = new Date(today.getFullYear(), today.getMonth(), today.getDate()); // clear time
    // build the calendar HTML
    var controls = '<div class="calendar_control">' +
      '<a class="calendar_clear" onclick="popUpCal._clearDate(' + this._id + ');">' +
      this._get('clearText') + '</a>' +
      '<a class="calendar_close" onclick="popUpCal.hideCalendar(' + this._id + ');">' +
      this._get('closeText') + '</a></div>';
    var prompt = this._get('prompt');
    var closeAtTop = this._get('closeAtTop');
    var hideIfNoPrevNext = this._get('hideIfNoPrevNext');
    // controls and links
    var html = (prompt ? '<div class="calendar_prompt">' + prompt + '</div>' : '') +
      (closeAtTop && !this._inline ? controls : '') + '<div class="calendar_links">' +
      (this._canAdjustMonth(-1) ? '<a class="calendar_prev" ' +
      'onclick="popUpCal._adjustDate(' + this._id + ', -1, \'M\');">' + this._get('prevText') + '</a>' :
      (hideIfNoPrevNext ? '' : '<label class="calendar_prev">' + this._get('prevText') + '</label>')) +
      (this._isInRange(today) ? '<a class="calendar_current" ' +
      'onclick="popUpCal._gotoToday(' + this._id + ');">' + this._get('currentText') + '</a>' : '') +
      (this._canAdjustMonth(+1) ? '<a class="calendar_next" ' +
      'onclick="popUpCal._adjustDate(' + this._id + ', +1, \'M\');">' + this._get('nextText') + '</a>' :
      (hideIfNoPrevNext ? '' : '<label class="calendar_next">' + this._get('nextText') + '</label>')) +
      '</div><div class="calendar_header">';
    var minDate = this._get('minDate');
    var maxDate = this._get('maxDate');
    // month selection
    var monthNames = this._get('monthNames');
    if (!this._get('changeMonth')) {
      html += monthNames[this._selectedMonth] + '&nbsp;';
    }
    else {
      var inMinYear = (minDate && minDate.getFullYear() == this._selectedYear);
      var inMaxYear = (maxDate && maxDate.getFullYear() == this._selectedYear);
      html += '<select class="calendar_newMonth" ' +
        'onchange="popUpCal._selectMonthYear(' + this._id + ', this, \'M\');" ' +
        'onclick="popUpCal._clickMonthYear(' + this._id + ');">';
      for (var month = 0; month < 12; month++) {
        if ((!inMinYear || month >= minDate.getMonth()) &&
            (!inMaxYear || month <= maxDate.getMonth())) {
          html += '<option value="' + month + '"' +
            (month == this._selectedMonth ? ' selected="selected"' : '') +
            '>' + monthNames[month] + '</option>';
        }
      }
      html += '</select>';
    }
    // year selection
    if (!this._get('changeYear')) {
      html += this._selectedYear;
    }
    else {
      // determine range of years to display
      var years = this._get('yearRange').split(':');
      var year = 0;
      var endYear = 0;
      if (years.length != 2) {
        year = this._selectedYear - 10;
        endYear = this._selectedYear + 10;
      }
      else if (years[0].charAt(0) == '+' || years[0].charAt(0) == '-') {
        year = this._selectedYear + parseInt(years[0], 10);
        endYear = this._selectedYear + parseInt(years[1], 10);
      }
      else {
        year = parseInt(years[0], 10);
        endYear = parseInt(years[1], 10);
      }
      year = (minDate ? Math.max(year, minDate.getFullYear()) : year);
      endYear = (maxDate ? Math.min(endYear, maxDate.getFullYear()) : endYear);
      html += '<select class="calendar_newYear" onchange="popUpCal._selectMonthYear(' +
        this._id + ', this, \'Y\');" ' + 'onclick="popUpCal._clickMonthYear(' +
        this._id + ');">';
      for (; year <= endYear; year++) {
        html += '<option value="' + year + '"' +
          (year == this._selectedYear ? ' selected="selected"' : '') +
          '>' + year + '</option>';
      }
      html += '</select>';
    }
    html += '</div><table class="calendar" cellpadding="0" cellspacing="0"><thead>' +
      '<tr class="calendar_titleRow">';
    var firstDay = this._get('firstDay');
    var changeFirstDay = this._get('changeFirstDay');
    var dayNames = this._get('dayNames');
    for (var dow = 0; dow < 7; dow++) { // days of the week
      html += '<td>' + (!changeFirstDay ? '' : '<a onclick="popUpCal._changeFirstDay(' +
        this._id + ', this);">') + dayNames[(dow + firstDay) % 7] +
        (changeFirstDay ? '</a>' : '') + '</td>';
    }
    html += '</tr></thead><tbody>';
    var daysInMonth = this._getDaysInMonth(this._selectedYear, this._selectedMonth);
    this._selectedDay = Math.min(this._selectedDay, daysInMonth);
    var leadDays = (this._getFirstDayOfMonth(this._selectedYear, this._selectedMonth) - firstDay + 7) % 7;
    var currentDate = new Date(this._currentYear, this._currentMonth, this._currentDay);
    var selectedDate = new Date(this._selectedYear, this._selectedMonth, this._selectedDay);
    var printDate = new Date(this._selectedYear, this._selectedMonth, 1 - leadDays);
    var numRows = Math.ceil((leadDays + daysInMonth) / 7); // calculate the number of rows to generate
    var customDate = this._get('customDate');
    var showOtherMonths = this._get('showOtherMonths');
    for (var row = 0; row < numRows; row++) { // create calendar rows
      html += '<tr class="calendar_daysRow">';
      for (var dow = 0; dow < 7; dow++) { // create calendar days
        var customSettings = (customDate ? customDate(printDate) : [true, '']);
        var otherMonth = (printDate.getMonth() != this._selectedMonth);
        var unselectable = otherMonth || !customSettings[0] ||
          (minDate && printDate < minDate) || (maxDate && printDate > maxDate);
        html += '<td class="calendar_daysCell' +
          ((dow + firstDay + 6) % 7 >= 5 ? ' calendar_weekEndCell' : '') + // highlight weekends
          (otherMonth ? ' calendar_otherMonth' : '') + // highlight days from other months
          (printDate.getTime() == selectedDate.getTime() ? ' calendar_daysCellOver' : '') + // highlight selected day
          (unselectable ? ' calendar_unselectable' : '') +  // highlight unselectable days
          (otherMonth && !showOtherMonths ? '' : ' ' + customSettings[1] + // highlight custom dates
          (printDate.getTime() == currentDate.getTime() ? ' calendar_currentDay' : // highlight current day
          (printDate.getTime() == today.getTime() ? ' calendar_today' : ''))) + '"' + // highlight today (if different)
          (unselectable ? '' : ' onmouseover="$ui(this).addClass(\'calendar_daysCellOver\');"' +
          ' onmouseout="$ui(this).removeClass(\'calendar_daysCellOver\');"' +
          ' onclick="popUpCal._selectDay(' + this._id + ', this);"') + '>' + // actions
          (otherMonth ? (showOtherMonths ? printDate.getDate() : '&nbsp;') : // display for other months
          (unselectable ? printDate.getDate() : '<a>' + printDate.getDate() + '</a>')) + '</td>'; // display for this month
        printDate.setDate(printDate.getDate() + 1);
      }
      html += '</tr>';
    }
    html += '</tbody></table>' + (!closeAtTop && !this._inline ? controls : '') +
      '<div style="clear: both;"></div>' + (!$.browser.msie ? '' :
      '<!--[if lte IE 6.5]><iframe src="javascript:false;" class="calendar_cover"></iframe><![endif]-->');
    return html;
  },

  /* Adjust one of the date sub-fields. */
  _adjustDate: function(offset, period) {
    var date = new Date(this._selectedYear + (period == 'Y' ? offset : 0),
      this._selectedMonth + (period == 'M' ? offset : 0),
      this._selectedDay + (period == 'D' ? offset : 0));
    // ensure it is within the bounds set
    var minDate = this._get('minDate');
    var maxDate = this._get('maxDate');
    date = (minDate && date < minDate ? minDate : date);
    date = (maxDate && date > maxDate ? maxDate : date);
    this._selectedDay = date.getDate();
    this._selectedMonth = date.getMonth();
    this._selectedYear = date.getFullYear();
  },

  /* Find the number of days in a given month. */
  _getDaysInMonth: function(year, month) {
    return 32 - new Date(year, month, 32).getDate();
  },

  /* Find the day of the week of the first of a month. */
  _getFirstDayOfMonth: function(year, month) {
    return new Date(year, month, 1).getDay();
  },

  /* Determines if we should allow a "next/prev" month display change. */
  _canAdjustMonth: function(offset) {
    var date = new Date(this._selectedYear, this._selectedMonth + offset, 1);
    if (offset < 0) {
      date.setDate(this._getDaysInMonth(date.getFullYear(), date.getMonth()));
    }
    return this._isInRange(date);
  },

  /* Is the given date in the accepted range? */
  _isInRange: function(date) {
    var minDate = this._get('minDate');
    var maxDate = this._get('maxDate');
    return ((!minDate || date >= minDate) && (!maxDate || date <= maxDate));
  },

  /* Format the given date for display. */
  _formatDate: function() {
    var day = this._currentDay = this._selectedDay;
    var month = this._currentMonth = this._selectedMonth;
    var year = this._currentYear = this._selectedYear;
    month++; // adjust javascript month
    var dateFormat = this._get('dateFormat');
    var dateString = '';
    for (var i = 0; i < 3; i++) {
      dateString += dateFormat.charAt(3) +
        (dateFormat.charAt(i) == 'D' ? (day < 10 ? '0' : '') + day :
        (dateFormat.charAt(i) == 'M' ? (month < 10 ? '0' : '') + month :
        (dateFormat.charAt(i) == 'Y' ? year : '?')));
    }
    return dateString.substring(dateFormat.charAt(3) ? 1 : 0);
  }
});

/* jQuery extend now ignores nulls! */
function extendRemove(target, props) {
  $.extend(target, props);
  for (var name in props) {
    if (props[name] == null) {
      target[name] = null;
    }
  }
  return target;
}

/* Attach the calendar to a jQuery selection.
   @param  settings  object - the new settings to use for this calendar instance (anonymous)
   @return jQuery object - for chaining further calls */
$.fn.calendar = function(settings) {
  return this.each(function() {
    // check for settings on the control itself - in namespace 'cal:'
    var inlineSettings = null;
    for (attrName in popUpCal._defaults) {
      var attrValue = this.getAttribute('cal:' + attrName);
      if (attrValue) {
        inlineSettings = inlineSettings || {};
        try {
          inlineSettings[attrName] = eval(attrValue);
        }
        catch (err) {
          inlineSettings[attrName] = attrValue;
        }
      }
    }
    var nodeName = this.nodeName.toLowerCase();
    if (nodeName == 'input') {
      var instSettings = (inlineSettings ? $.extend($.extend({}, settings || {}),
        inlineSettings || {}) : settings); // clone and customise
      var inst = (inst && !inlineSettings ? inst :
        new PopUpCalInstance(instSettings, false));
      popUpCal._connectCalendar(this, inst);
    } 
    else if (nodeName == 'div' || nodeName == 'span') {
      var instSettings = $.extend($.extend({}, settings || {}),
        inlineSettings || {}); // clone and customise
      var inst = new PopUpCalInstance(instSettings, true);
      popUpCal._inlineCalendar(this, inst);
    }
  });
};

/* Initialise the calendar. */
$(document).ready(function() {
   popUpCal = new PopUpCal(); // singleton instance
});

})($);

// *************************************************************************************************
// *************************************** MAGNIFIER  **********************************************
// *************************************************************************************************

(function($) {

  //If the UI scope is not availalable, add it
  $.ui = $.ui || {};
  
  //Make nodes selectable by expression
  $.extend($.expr[':'], { magnifier: "(' '+a.className+' ').indexOf(' ui-magnifier ')" });

  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable,reset".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-magnifier")) jQuery.data(this, "ui-magnifier")["'+cur+'"](a); }); }');
    $.fn["magnifier"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };

  //get instance method
  $.fn.magnifierInstance = function() {
    if($(this[0]).is(".ui-magnifier")) return $.data(this[0], "ui-magnifier");
    return false;
  };
  
  $.fn.magnifier = function(options) {
    return this.each(function() {
      new $.ui.magnifier(this,options); 
    });
  };
  
  $.ui.magnifier = function(el,options) {
    
    var self = this; this.items = []; this.element = el;
    this.options = options || {}; var o = this.options;
    $.data(el, "ui-magnifier", this);
    $(el).addClass("ui-magnifier");
    
    o.distance = o.distance || 150;
    o.magnification = o.magnification || 2;
    o.baseline = o.baseline || 0;
    o.verticalLine =  o.verticalLine != undefined ? o.verticalLine : -0.5;
    
    this.pp = $(el).offset({ border: false });
    
    $('> *', el).each(function() {
      var co = $(this).offset({ border: false });
      if(self.options.overlap) var cp = $(this).position();
      self.items.push([this, co, [$(this).width(),$(this).height()], (cp || null)]);
      
      if(o.opacity)
        $(this).css('opacity', o.opacity.min);  
    });
  
    if(o.overlap) {
      for(var i=0;i<this.items.length;i++) {
        //Absolute stuff
        $(this.items[i][0]).css({
          position: "absolute",
          top: this.items[i][3].top,
          left: this.items[i][3].left
        });
      };
    }
    
    this.moveEvent = function(e) { if(!self.disabled) self.magnify.apply(self, [e]); }
    $(document).bind("mousemove", this.moveEvent);
    
    if(o.click) { //If onclick callback is available

      this.clickEvent = function(e) { if(!self.disabled) o.click.apply(this, [e, { options: self.options, current: self.current[0], currentOffset: self.current[1] }]); }
      $(el).bind('click', this.clickEvent);
    }
    
  }
  
  $.extend($.ui.magnifier.prototype, {
    destroy: function() {
      $(this.element).removeClass("ui-magnifier").removeClass("ui-magnifier-disabled");
      $(document).unbind("mousemove", this.moveEvent);
      if(this.clickEvent) $(this.element).unbind("click", this.clickEvent);
    },
    enable: function() {
      $(this.element).removeClass("ui-magnifier-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.element).addClass("ui-magnifier-disabled");
      this.reset();
      this.disabled = true;
    },
    reset: function(e) {
      
      var o = this.options;
      var c;
      var distance = 1;
      
      for(var i=0;i<this.items.length;i++) {
  
        c = this.items[i];
        
        $(c[0]).css({
          width: c[2][0],
          height: c[2][1],
          top: (c[3] ? c[3].top : 0),
          left: (c[3] ? c[3].left : 0)
        });
        
        if(o.opacity)
          $(c[0]).css('opacity', o.opacity.min);
          
        if(o.zIndex)
          $(c[0]).css("z-index", "");
        
      }
          
    },
    magnify: function(e) {
      var p = [e.pageX,e.pageY];
      var o = this.options;
      var c;
      this.current = this.items[0];
      var distance = 1;
  
      //Compute the parents distance, because we don't need to fire anything if we are not near the parent
  
      var overlap = ((p[0] > this.pp.left-o.distance && p[0] < this.pp.left + this.element.offsetWidth + o.distance) && (p[1] > this.pp.top-o.distance && p[1] < this.pp.top + this.element.offsetHeight + o.distance));
      if(!overlap) return false;
  
      
      for(var i=0;i<this.items.length;i++) {
        c = this.items[i];
        
        var olddistance = distance;
        if(!o.axis) {
          distance = Math.sqrt(
              Math.pow(p[0] - ((c[3] ? this.pp.left : c[1].left) + parseInt(c[0].style.left)) - (c[0].offsetWidth/2), 2)
            + Math.pow(p[1] - ((c[3] ? this.pp.top  : c[1].top ) + parseInt(c[0].style.top )) - (c[0].offsetHeight/2), 2)
          );
        } else {
          if(o.axis == "y") {
            distance = Math.abs(p[1] - ((c[3] ? this.pp.top  : c[1].top ) + parseInt(c[0].style.top )) - (c[0].offsetHeight/2));
          } else {
            distance = Math.abs(p[0] - ((c[3] ? this.pp.left : c[1].left) + parseInt(c[0].style.left)) - (c[0].offsetWidth/2));
          }     
        }
        
        if(distance < o.distance) {
  
          this.current = distance < olddistance ? this.items[i] : this.current;
          
          if(!o.axis || o.axis != "y") {
            $(c[0]).css({
              width: c[2][0]+ (c[2][0] * (o.magnification-1)) - (((distance/o.distance)*c[2][0]) * (o.magnification-1)),
              left: (c[3] ? (c[3].left + o.verticalLine * ((c[2][1] * (o.magnification-1)) - (((distance/o.distance)*c[2][1]) * (o.magnification-1)))) : 0)
            });
          }
          
          if(!o.axis || o.axis != "x") {
            $(c[0]).css({
              height: c[2][1]+ (c[2][1] * (o.magnification-1)) - (((distance/o.distance)*c[2][1]) * (o.magnification-1)),
              top: (c[3] ? c[3].top : 0) + (o.baseline-0.5) * ((c[2][0] * (o.magnification-1)) - (((distance/o.distance)*c[2][0]) * (o.magnification-1)))
            });         
          }
          
          if(o.opacity)
            $(c[0]).css('opacity', o.opacity.max-(distance/o.distance) < o.opacity.min ? o.opacity.min : o.opacity.max-(distance/o.distance));
          
        } else {
          
          $(c[0]).css({
            width: c[2][0],
            height: c[2][1],
            top: (c[3] ? c[3].top : 0),
            left: (c[3] ? c[3].left : 0)
          });
          
          if(o.opacity)
            $(c[0]).css('opacity', o.opacity.min);
                
        }
        
        if(o.zIndex)
          $(c[0]).css("z-index", "");
  
      }
      
      if(this.options.zIndex)
        $(this.current[0]).css("z-index", this.options.zIndex);
      
    }
  });

})($);

// *************************************************************************************************
// *************************************** SHADOW     **********************************************
// *************************************************************************************************

(function($) {

  //If the UI scope is not available, add it
  $.ui = $.ui || {};
  
  //Make nodes selectable by expression
  $.extend($.expr[':'], { shadowed: "(' '+a.className+' ').indexOf(' ui-shadowed ')" });
  
  $.fn.shadowEnable = function() { if($(this[0]).next().is(".ui-shadow")) $(this[0]).next().show(); }
  $.fn.shadowDisable = function() { if($(this[0]).next().is(".ui-shadow")) $(this[0]).next().hide(); }
  
  $.fn.shadow = function(options) {
    
    options = options || {};
    options.offset = options.offset ? options.offset : 0;
    options.opacity = options.opacity ? options.opacity : 0.2;
    
    return this.each(function() {
      
      var cur = $(this);
      
      //Create a shadow element
      var shadow = $("<div class='ui-shadow'></div>"); cur.after(shadow);
      
      //Figure the base height and width
      var baseWidth = cur.outerWidth();
      var baseHeight = cur.outerHeight();
      
      //get the offset
      var position = cur.position();
      
      //Append smooth corners
      $('<div class="ui-shadow-color ui-shadow-layer-1"></div>').css({ opacity: options.opacity-0.05, left: 5+options.offset, top: 5+options.offset, width: baseWidth+1, height: baseHeight+1 }).appendTo(shadow);
      $('<div class="ui-shadow-color ui-shadow-layer-2"></div>').css({ opacity: options.opacity-0.1, left: 7+options.offset, top: 7+options.offset, width: baseWidth, height: baseHeight-3 }).appendTo(shadow);
      $('<div class="ui-shadow-color ui-shadow-layer-3"></div>').css({ opacity: options.opacity-0.1, left: 7+options.offset, top: 7+options.offset, width: baseWidth-3, height: baseHeight }).appendTo(shadow);
      $('<div class="ui-shadow-color ui-shadow-layer-4"></div>').css({ opacity: options.opacity, left: 6+options.offset, top: 6+options.offset, width: baseWidth-1, height: baseHeight-1 }).appendTo(shadow);
      
      //If we have a color, use it
      if(options.color)
        $("div.ui-shadow-color", shadow).css("background-color", options.color);
      
      //Determine the stack order (attention: the zIndex will get one higher!)
      if(!cur.css("zIndex") || cur.css("zIndex") == "auto") {
        var stack = 0;
        cur.css("position", (cur.css("position") == "static" ? "relative" : cur.css("position"))).css("z-index", "1");
      } else {
        var stack = parseInt(cur.css("zIndex"));
        cur.css("zIndex", stack+1);
      }
      
      //Copy the original z-index and position to the clone
      //alert(shadow); If you insert this alert, opera will time correctly!!
      shadow.css({
        position: "absolute",
        zIndex: stack,
        left: position.left,
        top: position.top,
        width: baseWidth,
        height: baseHeight,
        marginLeft: cur.css("marginLeft"),
        marginRight: cur.css("marginRight"),
        marginBottom: cur.css("marginBottom"),
        marginTop: cur.css("marginTop")
      });
      
      
      function rearrangeShadow(el,sh) {
        var $el = $(el);
        $(sh).css($el.position());
        $(sh).children().css({ height: $el.outerHeight()+"px", width: $el.outerWidth()+"px" });
      }
      
      if($.browser.msie) {
        //Add dynamic css expressions
        shadow[0].style.setExpression("left","parseInt(jQuery(this.previousSibling).css('left'))+'px' || jQuery(this.previousSibling).position().left");
        shadow[0].style.setExpression("top","parseInt(jQuery(this.previousSibling).css('top'))+'px' || jQuery(this.previousSibling).position().top");
      } else {
        //Bind events for good browsers
        this.addEventListener("DOMAttrModified",function() { rearrangeShadow(this,shadow); },false);
      }

        
    });
  };
  

})($);

// *************************************************************************************************
// *************************************** SLIDER     **********************************************
// *************************************************************************************************

(function($) {

  
  //Web Forms 2.0
  window.webforms = 1;
  if(window['webforms']) {
    $(document).ready(function() {
      
      $("input").each(function() {
        if(this.getAttribute("type") == "range") {
          var cur = $(this);
          var slider = $("<div class='ui-slider'></div>").css({ width: cur.innerWidth()+"px", height: cur.innerHeight()+"px" }).insertAfter(cur);
          var handle = $("<div class='ui-slider-handle'></div>").appendTo(slider);


          slider.css({
            "position": cur.css("position") == "absolute" ? "absolute" : "relative",
            "left": cur.css("left"),
            "right": cur.css("right"),
            "zIndex": cur.css("zIndex"),
            "float": cur.css("float"),
            "clear": cur.css("clear")
          });
          cur.css({ position: "absolute", opacity: 0, top: "-1000px", left: "-1000px" });
          
          slider.slider({
            maxValue: cur.attr("max"),
            minValue: cur.attr("min"),
            startValue: this.getAttribute("value"),
            stepping: cur.attr("step"),
            change: function(e, ui) { cur[0].value = ui.value; cur[0].setAttribute("value", ui.value); },
          });
          
          slider = slider.sliderInstance();
          
          cur.bind("keydown", function(e) {
            var o = slider.interaction.options;
            switch(e.keyCode) {
              case 37:
                slider.moveTo(slider.interaction.curValue+o.minValue-(o.stepping || 1));
                break;
              case 39:
                slider.moveTo(slider.interaction.curValue+o.minValue+(o.stepping || 1));
                break;  
            }
            if(e.keyCode != 9) return false;
          });
          
        };  
      });
        
    });
  }

  //Make nodes selectable by expression
  $.extend($.expr[':'], { slider: "(' '+a.className+' ').indexOf(' ui-slider ')" });
  
  $.fn.slider = function(o) {
    return this.each(function() {
      new $.ui.slider(this, o);
    });
  }
  
  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable,moveTo".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-slider")) jQuery.data(this, "ui-slider")["'+cur+'"](a); }); }');
    $.fn["slider"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };
  
  //get instance method
  $.fn.sliderInstance = function() {
    if($(this[0]).is(".ui-slider")) return $.data(this[0], "ui-slider");
    return false;
  };
  
  $.ui.slider = function(el, o) {
    
    var options = {};
    o = o || {};
    $.extend(options, o);
    $.extend(options, {
      axis: o.axis || (el.offsetWidth < el.offsetHeight ? 'vertical' : 'horizontal'),
      maxValue: parseInt(o.maxValue) || 100,
      minValue: parseInt(o.minValue) || 0,
      startValue: parseInt(o.startValue) || 0,
      _start: function(h, p, c, t, e) {
        self.start.apply(t, [self, e]); // Trigger the start callback       
      },
      _beforeStop: function(h, p, c, t, e) {
        self.stop.apply(t, [self, e]); // Trigger the start callback
      },
      _drag: function(h, p, c, t, e) {
        self.drag.apply(t, [self, e]); // Trigger the start callback
      },
      startCondition: function() {
        return !self.disabled;
      }     
    });

    var self = this;
    var o = options;
    $.data(el, "ui-slider", this);
    o.stepping = parseInt(o.stepping) || (o.steps ? o.maxValue/o.steps : 0);
    o.realValue = (o.maxValue - o.minValue);


    this.handle = options.handle ? $(options.handle, el) : $('.ui-slider-handle', el);
    if(this.handle.length == 1) {
      this.interaction = new $.ui.mouseInteraction(this.handle[0], options);
      this.multipleHandles = false;
    } else {
      this.interactions = [];
      this.handle.each(function() {
        self.interactions.push(new $.ui.mouseInteraction(this, options));
      });
      this.multipleHandles = true;
    }
    
    this.element = el;
    $(this.element).addClass("ui-slider");
    
    
    if(o.axis == 'horizontal') {
      this.parentSize = $(this.element).outerWidth() - this.handle.outerWidth();
      this.prop = 'left';
    }
    
    if(o.axis == 'vertical') {
      this.parentSize = $(this.element).outerHeight() - this.handle.outerHeight();
      this.prop = 'top';
    }
    
    if(!this.multipleHandles) {
      $(el).bind('click', function(e) { self.click.apply(self, [e]); });
      if(!isNaN(o.startValue)) this.moveTo(o.startValue,options.realValue, null, false);
    }
    
  }
  
  $.extend($.ui.slider.prototype, {
    currentTarget: null,
    lastTarget: null,
    destroy: function() {
      $(this.element).removeClass("ui-slider").removeClass("ui-slider-disabled");
      this.interaction.destroy();
    },
    enable: function() {
      $(this.element).removeClass("ui-slider-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.element).addClass("ui-slider-disabled");
      this.disabled = true;
    },
    nonvalidRange: function(self) {

      for(var i=0;i<this.interactions.length;i++) {
        if(self == this.interactions[i]) {
          if(this.interactions[i-1]) {
            if(this.interactions[i-1].curValue > this.interactions[i].curValue) return this.interactions[i-1].curValue;
          }
          
          if(this.interactions[i+1]) {
            if(this.interactions[i+1].curValue < this.interactions[i].curValue) return this.interactions[i+1].curValue;
          }
        }
      }
      
      return false;
      
    },
    prepareCallbackObj: function(self,m) {
      
      var cur = this;
      var func = function() {
        var retVal = [];
        for(var i=0;i<cur.interactions.length;i++) {
          retVal.push((cur.interactions[i].curValue || 0)+self.options.minValue);
        }
        return retVal;
      };
      
      return {
        handle: self.helper,
        pixel: m,
        value: self.curValue+self.options.minValue,
        values: this.multipleHandles ? func() : self.curValue+self.options.minValue,
        slider: self  
      }     
    },
    click: function(e) {
      var o = this.interaction.options;
      var pointer = [e.pageX,e.pageY];
      var offset = $(this.interaction.element).offsetParent().offset({ border: false });
      if(this.interaction.element == e.target || this.disabled) return;
      
      this.interaction.pickValue = this.interaction.curValue;
      this.drag.apply(this.interaction, [this, e, [pointer[0]-offset.left-this.handle[0].offsetWidth/2,pointer[1]-offset.top-this.handle[0].offsetHeight/2]]);
      
      if(this.interaction.pickValue != this.interaction.curValue)
        $(this.element).triggerHandler("slidechange", [e, this.prepareCallbackObj(this.interaction)], o.change);
        
    },
    start: function(that, e) {
      
      var o = this.options;
      $(that.element).triggerHandler("slidestart", [e, that.prepareCallbackObj(this)], o.start);
      this.pickValue = this.curValue;
      
      return false;
            
    },
    stop: function(that, e) {     
      
      var o = this.options;
      $(that.element).triggerHandler("slidestop", [e, that.prepareCallbackObj(this)], o.stop);
      if(this.pickValue != this.curValue) $(that.element).triggerHandler("slidechange", [e, that.prepareCallbackObj(this)], o.change);

      return false;
      
    },
    drag: function(that, e, pos) {

      var o = this.options;
      this.pos = pos || [this.pos[0]-this.element.offsetWidth/2, this.pos[1]-this.element.offsetHeight/2];
      
      if(o.axis == 'horizontal') var m = this.pos[0];
      if(o.axis == 'vertical')   var m = this.pos[1];
      
      
      var p = that.parentSize;
      var prop = that.prop;

      if(m < 0) m = 0;
      if(m > p) m = p;

      this.curValue = (Math.round((m/p)*o.realValue));
      if(o.stepping) {
        this.curValue = Math.round(this.curValue/o.stepping)*o.stepping;
        m = ((this.curValue)/o.realValue) * p;
      }
      
      if(that.interactions) {
        nonvalidRange = that.nonvalidRange(this);
        if(nonvalidRange) {
          this.curValue = nonvalidRange;
          m = ((this.curValue)/o.realValue) * p;
        }
      }
      
      $(this.element).css(prop, m+'px');
      $(that.element).triggerHandler("slide", [e, that.prepareCallbackObj(this,m)], o.slide);
      return false;
      
    },
    moveTo: function(value,scale,changeslide,p) { // renamed from goto to moveTo as goto is reserved javascript word
      
      if(this.multipleHandles) return false; //TODO: Multiple handle moveTo function
      
      var o = this.interaction.options;
      var offset = $(this.interaction.element).offsetParent().offset({ border: false });
      this.interaction.pickValue = this.interaction.curValue;
      value = value-o.minValue;
      
      var modifier = scale || o.realValue;
      
      var p = this.parentSize;
      var prop = this.prop;
      
      m = Math.round(((value)/modifier) * p);

      if(m < 0) m = 0;
      if(m > p) m = p;
      
      this.interaction.curValue = (Math.round((m/p)*o.realValue));
      if(o.stepping) {
        this.interaction.curValue = Math.round(this.interaction.curValue/o.stepping)*o.stepping;
        m = ((this.interaction.curValue)/o.realValue) * p;
      }

      $(this.interaction.element).css(prop, m+'px');
      
      if(!changeslide && this.interaction.pickValue != this.interaction.curValue && !p)
        $(this.element).triggerHandler("slidechange", [e, this.prepareCallbackObj(this.interaction)], o.change);
      
      if(changeslide)
        $(this.element).triggerHandler("slide", [e, this.prepareCallbackObj(this.interaction)], o.slide);

    }
  });

})($);

// *************************************************************************************************
// *************************************** SORTABLE    *********************************************
// *************************************************************************************************

if (window.Node && Node.prototype && !Node.prototype.contains) {
  Node.prototype.contains = function (arg) {
    return !!(this.compareDocumentPosition(arg) & 16)
  }
}

(function($) {

  //Make nodes selectable by expression
  $.extend($.expr[':'], { sortable: "(' '+a.className+' ').indexOf(' ui-sortable ')" });

  $.fn.sortable = function(o) {
    return this.each(function() {
      new $.ui.sortable(this,o);  
    });
  }
  
  //Macros for external methods that support chaining
  var methods = "destroy,enable,disable,refresh".split(",");
  for(var i=0;i<methods.length;i++) {
    var cur = methods[i], f;
    eval('f = function() { var a = arguments; return this.each(function() { if(jQuery(this).is(".ui-sortable")) jQuery.data(this, "ui-sortable")["'+cur+'"](a); }); }');
    $.fn["sortable"+cur.substr(0,1).toUpperCase()+cur.substr(1)] = f;
  };
  
  //get instance method
  $.fn.sortableInstance = function() {
    if($(this[0]).is(".ui-sortable")) return $.data(this[0], "ui-sortable");
    return false;
  };
  
  $.ui.sortable = function(el,o) {
  
    this.element = el;
    this.set = [];
    var options = {};
    var self = this;
    $.data(this.element, "ui-sortable", this);
    $(el).addClass("ui-sortable");
    
    $.extend(options, o);
    $.extend(options, {
      items: options.items || '> li',
      smooth: options.smooth != undefined ? options.smooth : true,
      helper: 'clone',
      containment: options.containment ? (options.containment == 'sortable' ? el : options.containment) : null,
      zIndex: options.zIndex || 1000,
      _start: function(h,p,c,t,e) {
        self.start.apply(t, [self, e]); // Trigger the onStart callback       
      },
      _beforeStop: function(h,p,c,t,e) {
        self.stop.apply(t, [self, e]); // Trigger the onStart callback
      },
      _drag: function(h,p,c,t,e) {
        self.drag.apply(t, [self, e]); // Trigger the onStart callback
      },
      startCondition: function() {
        return !self.disabled;  
      }     
    });
    
    //Get the items
    var items = $(options.items, el);
    
    //Let's determine the floating mode
    options.floating = /left|right/.test(items.css('float'));
    
    //Let's determine the parent's offset
    if($(el).css('position') == 'static') $(el).css('position', 'relative');
    options.offset = $(el).offset({ border: false });

    items.each(function() {
      new $.ui.mouseInteraction(this,options);
    });
    
    //Add current items to the set
    items.each(function() {
      self.set.push([this,null]);
    });
    
    this.options = options;
  }
  
  $.extend($.ui.sortable.prototype, {
    plugins: {},
    currentTarget: null,
    lastTarget: null,
    prepareCallbackObj: function(self, that) {
      return {
        helper: self.helper,
        position: { left: self.pos[0], top: self.pos[1] },
        offset: self.options.cursorAt,
        draggable: self,
        current: that,
        options: self.options
      }     
    },
    refresh: function() {

      //Get the items
      var self = this;
      var items = $(this.options.items, this.element);

      var unique = [];
      items.each(function() {
        old = false;
        for(var i=0;i<self.set.length;i++) { if(self.set[i][0] == this) old = true; }
        if(!old) unique.push(this);
      });
      
      for(var i=0;i<unique.length;i++) {
        new $.ui.mouseInteraction(unique[i],self.options);
      }
      
      //Add current items to the set
      this.set = [];
      items.each(function() {
        self.set.push([this,null]);
      });
      
    },
    destroy: function() {
      $(this.element).removeClass("ui-sortable").removeClass("ui-sortable-disabled");
      $(this.options.items, this.element).mouseInteractionDestroy();
      
    },
    enable: function() {
      $(this.element).removeClass("ui-sortable-disabled");
      this.disabled = false;
    },
    disable: function() {
      $(this.element).addClass("ui-sortable-disabled");
      this.disabled = true;
    },
    start: function(that, e) {
      
      var o = this.options;

      if(o.hoverClass) {
        that.helper = $('<div class="'+o.hoverClass+'"></div>').appendTo('body').css({
          height: this.element.offsetHeight+'px',
          width: this.element.offsetWidth+'px',
          position: 'absolute'  
        });
      }
      
      if(o.zIndex) {
        if($(this.helper).css("zIndex")) o.ozIndex = $(this.helper).css("zIndex");
        $(this.helper).css('zIndex', o.zIndex);
      }
      
      that.firstSibling = $(this.element).prev()[0];
        
      $(this.element).triggerHandler("sortstart", [e, that.prepareCallbackObj(this)], o.start);
      $(this.element).css('visibility', 'hidden');
      
      return false;
            
    },
    stop: function(that, e) {     
      
      var o = this.options;
      var self = this;
      

      if(o.smooth) {
        var os = $(this.element).offset();
        o.beQuietAtEnd = true;
        $(this.helper).animate({ left: os.left - o.po.left, top: os.top - o.po.top }, 500, stopIt);
      } else {
        stopIt();
      }
        
      function stopIt() {

        $(self.element).css('visibility', 'visible');
        if(that.helper) that.helper.remove();
        if(self.helper != self.element) $(self.helper).remove(); 

        if(o.ozIndex)
          $(self.helper).css('zIndex', o.ozIndex);
          
          
        //Let's see if the position in DOM has changed
        if($(self.element).prev()[0] != that.firstSibling) {
          //$(self.element).triggerHandler("sortupdate", [e, that.prepareCallbackObj(self, that)], o.update);
        }       

      }
      

      return false;
      
    },
    drag: function(that, e) {

      var o = this.options;

      this.pos = [this.pos[0]-(o.cursorAt.left ? o.cursorAt.left : 0), this.pos[1]-(o.cursorAt.top ? o.cursorAt.top : 0)];
      var nv =  $(this.element).triggerHandler("sort", [e, that.prepareCallbackObj(this)], o.sort);
      var nl = (nv && nv.left) ? nv.left :  this.pos[0];
      var nt = (nv && nv.top) ? nv.top :  this.pos[1];
      

      var m = that.set;
      var p = this.pos[1];
      
      for(var i=0;i<m.length;i++) {
        
        var ci = $(m[i][0]); var cio = m[i][0];
        if(this.element.contains(cio)) continue;
        var cO = ci.offset(); //TODO: Caching
        cO = { top: cO.top, left: cO.left };
        
        var mb = function(e) { if(true || o.lba != cio) { ci.before(e); o.lba = cio; } }
        var ma = function(e) { if(true || o.laa != cio) { ci.after(e); o.laa = cio; } }
        
        if(o.floating) {
          
          var overlap = ((cO.left - (this.pos[0]+(this.options.po ? this.options.po.left : 0)))/this.helper.offsetWidth);

          if(!(cO.top < this.pos[1]+(this.options.po ? this.options.po.top : 0) + cio.offsetHeight/2 && cO.top + cio.offsetHeight > this.pos[1]+(this.options.po ? this.options.po.top : 0) + cio.offsetHeight/2)) continue;                
          
        } else {

          var overlap = ((cO.top - (this.pos[1]+(this.options.po ? this.options.po.top : 0)))/this.helper.offsetHeight);

          if(!(cO.left < this.pos[0]+(this.options.po ? this.options.po.left : 0) + cio.offsetWidth/2 && cO.left + cio.offsetWidth > this.pos[0]+(this.options.po ? this.options.po.left : 0) + cio.offsetWidth/2)) continue;

        }
        
        if(overlap >= 0 && overlap <= 0.5) { //Overlapping at top
          ci.prev().length ? ma(this.element) : mb(this.element);
        }

        if(overlap < 0 && overlap > -0.5) { //Overlapping at bottom
          ci.next()[0] == this.element ? mb(this.element) : ma(this.element);
        }

      }
      
      //Let's see if the position in DOM has changed
      if($(this.element).prev()[0] != that.lastSibling) {
        $(this.element).triggerHandler("sortchange", [e, that.prepareCallbackObj(this, that)], this.options.change);
        that.lastSibling = $(this.element).prev()[0]; 
      }

      if(that.helper) { //reposition helper if available
        var to = $(this.element).offset();
        that.helper.css({
          top: to.top+'px',
          left: to.left+'px'  
        });
      } 
      
      $(this.helper).css('left', nl+'px').css('top', nt+'px'); // Stick the helper to the cursor
      return false;
      
    }
  });

 })($);

// *************************************************************************************************
// *************************************** TABLESORTER *********************************************
// *************************************************************************************************

/*
 * 
 * TableSorter 2.0 - Client-side table sorting with ease!
 * Version 2.0
 * @requires jQuery v1.1.3
 * 
 * Copyright (c) 2007 Christian Bach
 * Examples and docs at: http://tablesorter.com
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * 
 */
/**
 *
 * @description Create a sortable table with multi-column sorting capabilitys
 * 
 * @example $('#table').tablesorter();
 * @desc Create a simple tablesorter interface.
 *
 * @example $('#table').tablesorter({ sortList:[[0,0],[1,0]] });
 * @desc Create a tablesorter interface and sort on the first and secound column in ascending order.
 * 
 * @example $('#table').tablesorter({ headers: { 0: { sorter: false}, 1: {sorter: false} } });
 * @desc Create a tablesorter interface and disableing the first and secound column headers.
 * 
 * @example $('#table').tablesorter({ 0: {sorter:"integer"}, 1: {sorter:"currency"} });
 * @desc Create a tablesorter interface and set a column parser for the first and secound column.
 * 
 * 
 * @param Object settings An object literal containing key/value pairs to provide optional settings.
 * 
 * @option String cssHeader (optional)      A string of the class name to be appended to sortable tr elements in the thead of the table. 
 *                        Default value: "header"
 * 
 * @option String cssAsc (optional)       A string of the class name to be appended to sortable tr elements in the thead on a ascending sort. 
 *                        Default value: "headerSortUp"
 * 
 * @option String cssDesc (optional)      A string of the class name to be appended to sortable tr elements in the thead on a descending sort. 
 *                        Default value: "headerSortDown"
 * 
 * @option String sortInitialOrder (optional)   A string of the inital sorting order can be asc or desc. 
 *                        Default value: "asc"
 * 
 * @option String sortMultisortKey (optional)   A string of the multi-column sort key. 
 *                        Default value: "shiftKey"
 * 
 * @option String textExtraction (optional)   A string of the text-extraction method to use. 
 *                        For complex html structures inside td cell set this option to "complex", 
 *                        on large tables the complex option can be slow. 
 *                        Default value: "simple"
 * 
 * @option Object headers (optional)      An array containing the forces sorting rules. 
 *                        This option let's you specify a default sorting rule. 
 *                        Default value: null
 * 
 * @option Array sortList (optional)      An array containing the forces sorting rules. 
 *                        This option let's you specify a default sorting rule. 
 *                        Default value: null
 * 
 * @option Array sortForce (optional)       An array containing the forces sorting rules. 
 *                        This option let's you specify a default sorting rule. 
 *                        Default value: null
 *  
 * 
 * @option Boolean widthFixed (optional)    Boolean flag indicating if tablesorter should apply fixed widths to the table columns.
 *                        This is usefull when using the pager companion plugin.
 *                        This options requires the dimension jquery plugin.
 *                        Default value: false
 *
 * @option Boolean cancelSelection (optional)   Boolean flag indicating if tablesorter should cancel selection of the table headers text.
 *                        Default value: true
 * 
 * @type jQuery
 *
 * @name tablesorter
 * 
 * @cat Plugins/Tablesorter
 * 
 * @author Christian Bach/christian.bach@polyester.se
 */

(function($) {
  $.extend({
    tablesorter: new function() {
      
      var parsers = [], widgets = [];
      
      this.defaults = {
        cssHeader: "header",
        cssAsc: "headerSortUp",
        cssDesc: "headerSortDown",
        sortInitialOrder: "asc",
        sortMultiSortKey: "shiftKey",
        sortForce: null,
        textExtraction: "simple",
        parsers: {}, 
        widgets: [],    
        widgetZebra: {css: ["even","odd"]},
        headers: {},
        widthFixed: false,
        cancelSelection: true,
        sortList: [],
        headerList: [],
        dateFormat: "us",
        debug: false
      };
      
      /* debuging utils */
      function benchmark(label,stamp) {
        log(label + "," + (new Date().getTime() - stamp.getTime()) + "ms");
      }
      
      function log(s) {
        if (typeof console != "undefined" && typeof console.debug != "undefined") {
          console.log(s);
        } else {
          alert(s);
        }
      }
            
      /* parsers utils */
      function buildParserCache(table,$headers) {
        
        if(table.config.debug) { var parsersDebug = ""; }
        
        var list = [], cells = table.tBodies[0].rows[0].cells, l = cells.length;
        
        for (var i=0;i < l; i++) {
          var p = false;
          
          if($.meta && ($($headers[i]).data() && $($headers[i]).data().sorter)  ) {
          
            p = getParserById($($headers[i]).data().sorter);  
          
          } else if((table.config.headers[i] && table.config.headers[i].sorter)) {

            p = getParserById(table.config.headers[i].sorter);
          }
          if(!p) {
            p = detectParserForColumn(table.config,cells[i]);
          }

          if(table.config.debug) { parsersDebug += "column:" + i + " parser:" +p.id + "\n"; }

          list.push(p);
        }

        if(table.config.debug) { log(parsersDebug); }

        return list;
      };
      
      function detectParserForColumn(config,node) {
        var l = parsers.length;
        for(var i=1; i < l; i++) {
          if(parsers[i].is($.trim(getElementText(config,node)))) {
            return parsers[i];
          }
        }
        
        // 0 is always the generic parser (text)
        return parsers[0];
      }
      
      function getParserById(name) {
        var l = parsers.length;
        for(var i=0; i < l; i++) {
          if(parsers[i].id.toLowerCase() == name.toLowerCase()) { 
            return parsers[i];
          }
        }
        return false;
      }
      
      /* utils */
      function buildCache(table) {
        
        if(table.config.debug) { var cacheTime = new Date(); }
        
        var totalRows = (table.tBodies[0] && table.tBodies[0].rows.length) || 0,
          totalCells = table.tBodies[0].rows[0].cells.length,
          parsers = table.config.parsers, 
          cache = {row: [], normalized: []};
        
          for (var i=0;i < totalRows; ++i) {
          
            /** Add the table data to main data array */
            var c = table.tBodies[0].rows[i], cols = [];
          
            cache.row.push($(c));
            
            for(var j=0; j < totalCells; ++j) {
              cols.push(parsers[j].format(getElementText(table.config,c.cells[j]),table,c.cells[j])); 
            }
                        
            cols.push(i); // add position for rowCache
            cache.normalized.push(cols);
            cols = null;
          };
        
        if(table.config.debug) { benchmark("Building cache for " + totalRows + " rows:", cacheTime); }
        
        return cache;
      };
      
      function getElementText(config,node) {
        
        if(!node) return "";
                
        var t = "";
        
        
        if(typeof(config.textExtraction) == "function") {
          t = config.textExtraction(node);
        } else if(config.textExtraction == "complex") { 
          t = $(node).text();
        } else {
          if(node.childNodes[0] && node.childNodes[0].hasChildNodes()) {
            t = node.childNodes[0].innerHTML;
          } else {
            t = node.innerHTML;
          }
        }
        return t;
      }
      
      function appendToTable(table,cache) {
        
        if(table.config.debug) {var appendTime = new Date()}
        
        var c = cache, 
          r = c.row, 
          n= c.normalized, 
          totalRows = n.length, 
          checkCell = (n[0].length-1), 
          tableBody = $("tbody:first",table).empty();
          rows = [];
        
        for (var i=0;i < totalRows; i++) {
            rows.push(r[n[i][checkCell]]);
            if(table.config.appender == null) {
              tableBody.append(r[n[i][checkCell]]);
            }
        } 

        if(table.config.appender != null) {
          table.config.appender(table,rows);  
        }
        
        rows = null;
        
        //apply table widgets
        applyWidget(table);
        
        if(table.config.debug) { benchmark("Rebuilt table:", appendTime); }
      
      };
      
      function buildHeaders(table) {
        
        if(table.config.debug) { var time = new Date(); }
        
        var meta = ($.meta) ? true : false, tableHeadersRows = [];
      
        for(var i = 0; i < table.tHead.rows.length; i++) { tableHeadersRows[i]=0; };
        
        $tableHeaders = $(checkCellColSpan(table, tableHeadersRows, 0,table.tHead.rows[0].cells.length));
    
        $tableHeaders.each(function(index) {
              
          this.count = 0;
          this.column = index;
          this.order = formatSortingOrder(table.config.sortInitialOrder);
          
          if(checkHeaderMetadata(this) || checkHeaderOptions(table,index)) this.sortDisabled = true;
          
          if(!this.sortDisabled) {
            $(this).addClass(table.config.cssHeader);
          }
          
          // add cell to headerList
          table.config.headerList[index]= this;
        });
        
        if(table.config.debug) { benchmark("Built headers:", time); log($tableHeaders); }
        
        return $tableHeaders;
        
      };
            
        function checkCellColSpan(table, headerArr, row) {
                var arr = [], r = table.tHead.rows, c = r[row].cells;
        
        for(var i=headerArr[row]; i < c.length; i++) {
          var cell = c[i];
          
          if ( cell.colSpan > 1) { 
            arr = arr.concat(checkCellColSpan(table, headerArr,row+cell.rowSpan));
          } else  {
            if(table.tHead.length == 1 || (cell.rowSpan > 1 || !r[row+1])) {
              arr.push(cell);
            }
            headerArr[row] = (i+row);
          }
        }
        return arr;
      };
      
      function checkHeaderMetadata(cell) {
        if(($.meta) && ($(cell).data().sorter === false)) { return true; };
        return false;
      }
      
      function checkHeaderOptions(table,i) {  
        if((table.config.headers[i]) && (table.config.headers[i].sorter === false)) { return true; };
        return false;
      }
      
      function applyWidget(table) {
        var c = table.config.widgets;
        var l = c.length;
        for(var i=0; i < l; i++) {
          
          getWidgetById(c[i]).format(table);
        }
        
      }
      
      function getWidgetById(name) {
        var l = widgets.length;
        for(var i=0; i < l; i++) {
          if(widgets[i].id.toLowerCase() == name.toLowerCase() ) {
            return widgets[i]; 
          }
        }
      };
      
      function formatSortingOrder(v) {
        
        if(typeof(v) != "Number") {
          i = (v.toLowerCase() == "desc") ? 1 : 0;
        } else {
          i = (v == (0 || 1)) ? v : 0;
        }
        return i;
      }
      
      function isValueInArray(v, a) {
        var l = a.length;
        for(var i=0; i < l; i++) {
          if(a[i][0] == v) {
            return true;  
          }
        }
        return false;
      }
        
      function setHeadersCss(table,$headers, list, css) {
        // remove all header information
        $headers.removeClass(css[0]).removeClass(css[1]);
        
        var h = [];
        $headers.each(function(offset) {
            if(!this.sortDisabled) {
              h[this.column] = $(this);         
            }
        });

        var l = list.length; 
        for(var i=0; i < l; i++) {
          h[list[i][0]].addClass(css[list[i][1]]);
        }
      }
      
      function fixColumnWidth(table,$headers) {
        var c = table.config;
        if(c.widthFixed) {
          var colgroup = $('<colgroup>');
          $("tbody:first tr:first td",table).each(function() {
            
            colgroup.append($('<col>').css('width',$(this).width()));
          
          });
          $(table).prepend(colgroup);
        };
      }
      
      function updateHeaderSortCount(table,sortList) {
        var c = table.config, l = sortList.length;
        for(var i=0; i < l; i++) {
          var s = sortList[i], o = c.headerList[s[0]];
          o.count = s[1];
          o.count++;
        }
      }
      
      /* sorting methods */
      function multisort(table,sortList,cache) {
        
        if(table.config.debug) { var sortTime = new Date(); }
        
        var dynamicExp = "var sortWrapper = function(a,b) {", l = sortList.length;
          
        for(var i=0; i < l; i++) {
          
          var c = sortList[i][0];
          var order = sortList[i][1];
          var s = (getCachedSortType(table.config.parsers,c) == "text") ? ((order == 0) ? "sortText" : "sortTextDesc") : ((order == 0) ? "sortNumeric" : "sortNumericDesc");
          
          var e = "e" + i;
          
          dynamicExp += "var " + e + " = " + s + "(a[" + c + "],b[" + c + "]); ";
          dynamicExp += "if(" + e + ") { return " + e + "; } ";
          dynamicExp += "else { ";
        }
          
        for(var i=0; i < l; i++) {
          dynamicExp += "}; ";
        }
        
        dynamicExp += "return 0; "; 
        dynamicExp += "}; ";  
        
        eval(dynamicExp);
        
        cache.normalized.sort(sortWrapper);
        
        if(table.config.debug) { benchmark("Sorting on " + sortList.toString() + " and dir " + order+ " time:", sortTime); }
        
        return cache;
      };
      
      function sortText(a,b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      };
      
      function sortTextDesc(a,b) {
        return ((b < a) ? -1 : ((b > a) ? 1 : 0));
      };  
      
      function sortNumeric(a,b) {
        return a-b;
      };
      
      function sortNumericDesc(a,b) {
        return b-a;
      };
      
      function getCachedSortType(parsers,i) {
        return parsers[i].type;
      };
      
      /* public methods */
      this.construct = function(settings) {

        return this.each(function() {
          
          
          
          
          var $this, $document,$headers, cache, config, shiftDown = 0, sortOrder;
          
          this.config = {};
          
          config = $.extend(this.config, $.tablesorter.defaults, settings);
          
          if(!this.tHead || !this.tBodies) return true;
          
          // store common expression for speed          
          $this = $(this);
          
          // build headers
          $headers = buildHeaders(this);
          
          // try to auto detect column type, and store in tables config
          this.config.parsers = buildParserCache(this,$headers);
          
          
          // build the cache for the tbody cells
          cache = buildCache(this);
          
          // get the css class names, could be done else where.
          var sortCSS = [config.cssDesc,config.cssAsc];
          
          // fixate columns if the users supplies the fixedWidth option
          fixColumnWidth(this);
          
          // apply event handling to headers
          // this is to big, perhaps break it out?
          $headers.click(function(e) {
            if(!this.sortDisabled) {
              // store exp, for speed
              var $cell = $(this);
  
              // get current column index
              var i = this.column;
              
              // get current column sort order
              this.order = this.count++ % 2;
              
              
              
              // user only whants to sort on one column
              if(!e[config.sortMultiSortKey]) {
                
                // flush the sort list
                config.sortList = [];
                
                if(config.sortForce != null) {
                  var a = config.sortForce; 
                  for(var j=0; j < a.length; j++) {   
                    config.sortList.push(a[j]); 
                  }
                }
                
                // add column to sort list
                config.sortList.push([i,this.order]);
              
              // multi column sorting 
              } else {
                // the user has clicked on an all ready sortet column.
                if(isValueInArray(i,config.sortList)) {  
                  
                  // revers the sorting direction for all tables.
                  for(var j=0; j < config.sortList.length; j++) {
                    var s = config.sortList[j], o = config.headerList[s[0]];
                    if(s[0] == i) {
                      o.count = s[1];
                      o.count++;
                      s[1] = o.count % 2;
                    }
                  } 
                } else {
                  // add column to sort list array
                  config.sortList.push([i,this.order]);
                }
              };
              
              //set css for headers
              setHeadersCss($this[0],$headers,config.sortList,sortCSS);
              
              // sort the table and append it to the dom
              appendToTable($this[0],multisort($this[0],config.sortList,cache));
              
              // stop normal event by returning false
              return false;
            }
          // cancel selection 
          }).mousedown(function() {
            if(config.cancelSelection) {
              this.onselectstart = function() {return false};
              //alert(this.onselectstart);
              return false;
            }
          });
          
          // apply easy methods that trigger binded events
          $this.bind("update",function() {
            
            // rebuild the cache map
            cache = buildCache(this);
            
          }).bind("sorton",function(e,list) {
            
            // update and store the sortlist
            var sortList = config.sortList = list;
            
            // update header count index
            updateHeaderSortCount(this,sortList);
            
            //set css for headers
            setHeadersCss(this,$headers,sortList,sortCSS);
            
            // sort the table and append it to the dom
            appendToTable(this,multisort(this,sortList,cache));
            
          }).bind("appendCache",function() {
            
            appendToTable(this,cache);
          
          }).bind("applyWidgetId",function(e,id) {
            
            getWidgetById(id).format(this);
            
          });
          
          if($.meta && ($(this).data() && $(this).data().sortlist)) {
            config.sortList = $(this).data().sortlist;
          }
          // if user has supplied a sort list to constructor.
          if(config.sortList.length > 0) {
            $this.trigger("sorton",[config.sortList]);  
          }
          
          // apply widgets
          applyWidget(this);
        });
      };
      
      this.addParser = function(parser) {
        var l = parsers.length, a = true;
        for(var i=0; i < l; i++) {
          if(parsers[i].id.toLowerCase() == parser.id.toLowerCase()) {
            a = false;
          }
        }
        if(a) { parsers.push(parser); };
      };
      
      this.addWidget = function(widget) {
        widgets.push(widget);
      };
      
      this.formatFloat = function(s) {

        var i = parseFloat(s);
        return (isNaN(i)) ? 0 : i;
      };
      this.formatInt = function(s) {
        var i = parseInt(s);
        return (isNaN(i)) ? 0 : i;
      };
      
    }
  });
  
  // extend plugin scope
  $.fn.extend({
        tablesorter: $.tablesorter.construct
  });
  
  // add default parsers
  $.tablesorter.addParser({
    id: "text",
    is: function(s) {
      return true;
    },
    format: function(s) {
      return $.trim(s.toLowerCase());
    },
    type: "text"
  });
  
  $.tablesorter.addParser({
    id: "integer",
    is: function(s) {
      return s.match(new RegExp(/^\d+$/));
    },
    format: function(s) {
      return $.tablesorter.formatInt(s);
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "currency",
    is: function(s) {
      return /^[£$€?.]/.test(s);
    },
    format: function(s) {
      return $.tablesorter.formatFloat(s.replace(new RegExp(/[^0-9.]/g),""));
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "integer",
    is: function(s) {
      return /^\d+$/.test(s);
    },
    format: function(s) {
      return $.tablesorter.formatFloat(s);
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "floating",
    is: function(s) {
      return s.match(new RegExp(/^(\+|-)?[0-9]+\.[0-9]+((E|e)(\+|-)?[0-9]+)?$/));
    },
    format: function(s) {
      return $.tablesorter.formatFloat(s.replace(new RegExp(/,/),""));
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "ipAddress",
    is: function(s) {
      return /^\d{2,3}[\.]\d{2,3}[\.]\d{2,3}[\.]\d{2,3}$/.test(s);
    },
    format: function(s) {
      var a = s.split(".");
      var r = "";
      for (var i = 0, item; item = a[i]; i++) {
         if(item.length == 2) {
          r += "0" + item;
         } else {
          r += item;
         }
      }
      return $.tablesorter.formatFloat(s);
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "url",
    is: function(s) {
      return /^(https?|ftp|file):\/\/$/.test(s);
    },
    format: function(s) {
      return jQuery.trim(s.replace(new RegExp(/(https?|ftp|file):\/\//),''));
    },
    type: "text"
  });
  
  $.tablesorter.addParser({
    id: "isoDate",
    is: function(s) {
      return /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(s);
    },
    format: function(s) {
      return $.tablesorter.formatFloat((s != "") ? new Date(s.replace(new RegExp(/-/g),"/")).getTime() : "0");
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "percent",
    is: function(s) {
      return /^\d{1,3}%$/.test(s);
    },
    format: function(s) {
      return $.tablesorter.formatFloat(s.replace(new RegExp(/%/g),""));
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "usLongDate",
    is: function(s) {
      return /^[A-Za-z]{3,10}\.? [0-9]{1,2}, ([0-9]{4}|\'?[0-9]{2}) (([0-2]?[0-9]:[0-5][0-9])|([0-1]?[0-9]:[0-5][0-9]\s(AM|PM)))$/.test(s);
    },
    format: function(s) {
      return $.tablesorter.formatFloat(new Date(s).getTime());
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
    id: "shortDate",
    is: function(s) {
      return /\d{1,2}[\/-]\d{1,2}[\/-]\d{2,4}/.test(s);
    },
    format: function(s,table) {
      var c = table.config;
      s = s.replace(new RegExp(/-/g),"/");
      if(c.dateFormat == "us") {
        /** reformat the string in ISO format */
        s = s.replace(new RegExp(/(\d{1,2})[\/-](\d{1,2})[\/-](\d{4})/), "$3/$1/$2");
      } else if(c.dateFormat == "uk") {
        /** reformat the string in ISO format */
        s = s.replace(new RegExp(/(\d{1,2})[\/-](\d{1,2})[\/-](\d{4})/), "$3/$2/$1");
      } else if(c.dateFormat == "dd/mm/yy" || c.dateFormat == "dd-mm-yy") {
        s = s.replace(new RegExp(/(\d{1,2})[\/-](\d{1,2})[\/-](\d{2})/), "$1/$2/$3"); 
      }
      return $.tablesorter.formatFloat(new Date(s).getTime());
    },
    type: "numeric"
  });
  
  $.tablesorter.addParser({
      id: "time",
      is: function(s) {
          return /^(([0-2]?[0-9]:[0-5][0-9])|([0-1]?[0-9]:[0-5][0-9]\s(am|pm)))$/.test(s);
      },
      format: function(s) {
          return $.tablesorter.formatFloat(new Date("2000/01/01 " + s).getTime());
      },
    type: "numeric"
  });
  
  
  $.tablesorter.addParser({
      id: "metadata",
      is: function(s) {
          return false;
      },
      format: function(s,table,cell) {
      var c = table.config, p = (!c.parserMetadataName) ? 'sortValue' : c.parserMetadataName;
          return $(cell).data()[p];
      },
    type: "numeric"
  });
  
  // add default widgets
  $.tablesorter.addWidget({
    id: "zebra",
    format: function(table) {
      $("> tbody:first/tr:visible:even",table).removeClass(table.config.widgetZebra.css[1]).addClass(table.config.widgetZebra.css[0]);
      $("> tbody:first/tr:visible:odd",table).removeClass(table.config.widgetZebra.css[0]).addClass(table.config.widgetZebra.css[1]);
    }
  });
  
})(jQuery);

// *************************************************************************************************
// *************************************** TABS  **********************************************
// *************************************************************************************************

/*
 * Tabs 3 - New Wave Tabs
 *
 * Copyright (c) 2007 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 */

(function($) {

    // if the UI scope is not availalable, add it
    $.ui = $.ui || {};

    // tabs initialization
    $.fn.tabs = function(initial, options) {
        if (initial && initial.constructor == Object) { // shift arguments
            options = initial;
            initial = null;
        }
        options = options || {};

        initial = initial && initial.constructor == Number && --initial || 0;

        return this.each(function() {
            new $.ui.tabs(this, $.extend(options, { initial: initial }));
        });
    };

    // other chainable tabs methods
    $.each(['Add', 'Remove', 'Enable', 'Disable', 'Click', 'Load'], function(i, method) {
        $.fn['tabs' + method] = function() {
            var args = arguments;
            return this.each(function() {
                var instance = $.ui.tabs.getInstance(this);
                instance[method.toLowerCase()].apply(instance, args);
            });
        };
    });
    $.fn.tabsSelected = function() {
        var selected = -1;
        if (this[0]) {
            var instance = $.ui.tabs.getInstance(this[0]),
                $lis = $('li', this);
            selected = $lis.index( $lis.filter('.' + instance.options.selectedClass)[0] );
        }
        return selected >= 0 ? ++selected : -1;
    };

    // tabs class
    $.ui.tabs = function(el, options) {

        this.source = el;

        this.options = $.extend({

            // basic setup
            initial: 0,
            event: 'click',
            disabled: [],
            // TODO bookmarkable: $.ajaxHistory ? true : false,
            unselected: false,
            unselect: options.unselected ? true : false,

            // Ajax
            spinner: 'Loading&#8230;',
            cache: false,
            idPrefix: 'tab-',

            // animations
            /*fxFade: null,
            fxSlide: null,
            fxShow: null,
            fxHide: null,*/
            fxSpeed: 'normal',
            /*fxShowSpeed: null,
            fxHideSpeed: null,*/

            // callbacks
            add: function() {},
            remove: function() {},
            enable: function() {},
            disable: function() {},
            click: function() {},
            hide: function() {},
            show: function() {},
            load: function() {},

            // CSS classes
            navClass: 'ui-tabs-nav',
            selectedClass: 'ui-tabs-selected',
            disabledClass: 'ui-tabs-disabled',
            containerClass: 'ui-tabs-container',
            hideClass: 'ui-tabs-hide',
            loadingClass: 'ui-tabs-loading'

        }, options);

        this.tabify(true);

        // save instance for later
        var uuid = 'tabs' + $.ui.tabs.prototype.count++;
        $.ui.tabs.instances[uuid] = this;
        $.data(el, 'tabsUUID', uuid);

    };

    // static
    $.ui.tabs.instances = {};
    $.ui.tabs.getInstance = function(el) {
        return $.ui.tabs.instances[$.data(el, 'tabsUUID')];
    };

    // instance methods
    $.extend($.ui.tabs.prototype, {
        count: 0,
        tabify: function(init) {

            this.$tabs = $('a:first-child', this.source);
            this.$containers = $([]);

            var self = this, o = this.options;
            
            this.$tabs.each(function(i, a) {
                // inline tab
                if (a.hash && a.hash.replace('#', '')) { // safari 2 reports '#' for an empty hash
                    self.$containers = self.$containers.add(a.hash);
                }
                // remote tab
                else {
                    $.data(a, 'href', a.href);
                    var id = a.title && a.title.replace(/\s/g, '_') || o.idPrefix + (self.count + 1) + '-' + (i + 1);
                    a.href = '#' + id;
                    self.$containers = self.$containers.add(
                        $('#' + id)[0] || $('<div id="' + id + '" class="' + o.containerClass + '"></div>')
                            .insertAfter( self.$containers[i - 1] || self.source )
                    );
                }
            });

            if (init) {

                // Try to retrieve initial tab from fragment identifier in url if present,
                // otherwise try to find selected class attribute on <li>.
                this.$tabs.each(function(i, a) {
                    if (location.hash) {
                        if (a.hash == location.hash) {
                            o.initial = i;
                            // prevent page scroll to fragment
                            //if (($.browser.msie || $.browser.opera) && !o.remote) {
                            if ($.browser.msie || $.browser.opera) {
                                var $toShow = $(location.hash), toShowId = $toShow.attr('id');
                                $toShow.attr('id', '');
                                setTimeout(function() {
                                    $toShow.attr('id', toShowId); // restore id
                                }, 500);
                            }
                            scrollTo(0, 0);
                            return false; // break
                        }
                    } else if ( $(a).parents('li:eq(0)').is('li.' + o.selectedClass) ) {
                        o.initial = i;
                        return false; // break
                    }
                });

                // attach necessary classes for styling if not present
                $(this.source).is('.' + o.navClass) || $(this.source).addClass(o.navClass);
                this.$containers.each(function() {
                    var $this = $(this);
                    $this.is('.' + o.containerClass) || $this.addClass(o.containerClass);
                });

                // highlight tab
                var $lis = $('li', this.source);
                this.$containers.addClass(o.hideClass);
                $lis.removeClass(o.selectedClass);
                if (!o.unselected) {
                    this.$containers.slice(o.initial, o.initial + 1).show();
                    $lis.slice(o.initial, o.initial + 1).addClass(o.selectedClass);
                }

                // load if remote tab
                if ($.data(this.$tabs[o.initial], 'href')) {
                    this.load(o.initial + 1, $.data(this.$tabs[o.initial], 'href'));
                    if (o.cache) {
                        $.removeData(this.$tabs[o.initial], 'href'); // if loaded once do not load them again
                    }
                }

                // disabled tabs
                for (var i = 0, position; position = o.disabled[i]; i++) {
                    this.disable(position);
                }

            }

            // setup animations
            var showAnim = {}, showSpeed = o.fxShowSpeed || o.fxSpeed,
                hideAnim = {}, hideSpeed = o.fxHideSpeed || o.fxSpeed;
            if (o.fxSlide || o.fxFade) {
                if (o.fxSlide) {
                    showAnim['height'] = 'show';
                    hideAnim['height'] = 'hide';
                }
                if (o.fxFade) {
                    showAnim['opacity'] = 'show';
                    hideAnim['opacity'] = 'hide';
                }
            } else {
                if (o.fxShow) {
                    showAnim = o.fxShow;
                } else { // use some kind of animation to prevent browser scrolling to the tab
                    showAnim['min-width'] = 0; // avoid opacity, causes flicker in Firefox
                    showSpeed = 1; // as little as 1 is sufficient
                }
                if (o.fxHide) {
                    hideAnim = o.fxHide;
                } else { // use some kind of animation to prevent browser scrolling to the tab
                    hideAnim['min-width'] = 0; // avoid opacity, causes flicker in Firefox
                    hideSpeed = 1; // as little as 1 is sufficient
                }
            }

            // reset some styles to maintain print style sheets etc.
            var resetCSS = { display: '', overflow: '', height: '' };
            if (!$.browser.msie) { // not in IE to prevent ClearType font issue
                resetCSS['opacity'] = '';
            }

            // Hide a tab, animation prevents browser scrolling to fragment,
            // $show is optional.
            function hideTab(clicked, $hide, $show) {
                $hide.animate(hideAnim, hideSpeed, function() { //
                    $hide.addClass(o.hideClass).css(resetCSS); // maintain flexible height and accessibility in print etc.
                    if ($.browser.msie) {
                        $hide[0].style.filter = '';
                    }
                    o.hide(clicked, $hide[0], $show && $show[0] || null);
                    if ($show) {
                        showTab(clicked, $show, $hide);
                    }
                });
            }

            // Show a tab, animation prevents browser scrolling to fragment,
            // $hide is optional
            function showTab(clicked, $show, $hide) {
                if (!(o.fxSlide || o.fxFade || o.fxShow)) {
                    $show.css('display', 'block'); // prevent occasionally occuring flicker in Firefox cause by gap between showing and hiding the tab containers
                }
                $show.animate(showAnim, showSpeed, function() {
                    $show.removeClass(o.hideClass).css(resetCSS); // maintain flexible height and accessibility in print etc.
                    if ($.browser.msie) {
                        $show[0].style.filter = '';
                    }
                    o.show(clicked, $show[0], $hide && $hide[0] || null);
                });
            }

            // switch a tab
            function switchTab(clicked, $hide, $show) {
                /*if (o.bookmarkable && trueClick) { // add to history only if true click occured, not a triggered click
                    $.ajaxHistory.update(clicked.hash);
                }*/
                $(clicked).parents('li:eq(0)').addClass(o.selectedClass)
                    .siblings().removeClass(o.selectedClass);
                hideTab(clicked, $hide, $show);
            }

            // tab click handler
            function tabClick(e) {

                //var trueClick = e.clientX; // add to history only if true click occured, not a triggered click
                var $li = $(this).parents('li:eq(0)'),
                    $hide = self.$containers.filter(':visible'),
                    $show = $(this.hash);

                // If tab is already selected and not unselectable or tab disabled or click callback returns false stop here.
                // Check if click handler returns false last so that it is not executed for a disabled tab!
                if (($li.is('.' + o.selectedClass) && !o.unselect) || $li.is('.' + o.disabledClass)
                    || o.click(this, $show[0], $hide[0]) === false) {
                    this.blur();
                    return false;
                }
                    
                // if tab may be closed
                if (o.unselect) {
                    if ($li.is('.' + o.selectedClass)) {
                        $li.removeClass(o.selectedClass);
                        self.$containers.stop();
                        hideTab(this, $hide);
                        this.blur();
                        return false;
                    } else if (!$hide.length) {
                        $li.addClass(o.selectedClass);
                        self.$containers.stop();
                        showTab(this, $show);
                        this.blur();
                        return false;
                    }
                }

                // stop possibly running animations
                self.$containers.stop();

                // show new tab
                if ($show.length) {

                    // prevent scrollbar scrolling to 0 and than back in IE7, happens only if bookmarking/history is enabled
                    /*if ($.browser.msie && o.bookmarkable) {
                        var showId = this.hash.replace('#', '');
                        $show.attr('id', '');
                        setTimeout(function() {
                            $show.attr('id', showId); // restore id
                        }, 0);
                    }*/

                    if ($.data(this, 'href')) { // remote tab
                        var a = this;
                        self.load(self.$tabs.index(this) + 1, $.data(this, 'href'), function() {
                            switchTab(a, $hide, $show);
                        });
                        if (o.cache) {
                            $.removeData(this, 'href'); // if loaded once do not load them again
                        }
                    } else {
                        switchTab(this, $hide, $show);
                    }

                    // Set scrollbar to saved position - need to use timeout with 0 to prevent browser scroll to target of hash
                    /*var scrollX = window.pageXOffset || document.documentElement && document.documentElement.scrollLeft || document.body.scrollLeft || 0;
                    var scrollY = window.pageYOffset || document.documentElement && document.documentElement.scrollTop || document.body.scrollTop || 0;
                    setTimeout(function() {
                        scrollTo(scrollX, scrollY);
                    }, 0);*/

                } else {
                    throw 'jQuery UI Tabs: Mismatching fragment identifier.';
                }

                this.blur(); // prevent IE from keeping other link focussed when using the back button

                //return o.bookmarkable && !!trueClick; // convert trueClick == undefined to Boolean required in IE
                return false;

            }

            // attach click event, avoid duplicates from former tabifying
            this.$tabs.unbind(o.event, tabClick).bind(o.event, tabClick);

        },
        add: function(url, text, position) {
            if (url && text) {
                var o = this.options;
                position = position || this.$tabs.length; // append by default
                if (position >= this.$tabs.length) {
                    var method = 'insertAfter';
                    position = this.$tabs.length;
                } else {
                    var method = 'insertBefore';
                }
                if (url.indexOf('#') == 0) { // ajax container is created by tabify automatically
                    var $container = $(url);
                    // try to find an existing element before creating a new one
                    ($container.length && $container || $('<div id="' + url.replace('#', '') + '" class="' + o.containerClass + ' ' + o.hideClass + '"></div>'))
                        [method](this.$containers[position - 1]);
                }
                $('<li><a href="' + url + '"><span>' + text + '</span></a></li>')
                    [method](this.$tabs.slice(position - 1, position).parents('li:eq(0)'));
                this.tabify();
                o.add(this.$tabs[position - 1], this.$containers[position - 1]); // callback
            } else {
                throw 'jQuery UI Tabs: Not enough arguments to add tab.';
            }
        },
        remove: function(position) {
            if (position && position.constructor == Number) {
                var $removedTab = this.$tabs.slice(position - 1, position).parents('li:eq(0)').remove();
                var $removedContainer = this.$containers.slice(position - 1, position).remove();
                this.tabify();
                this.options.remove($removedTab[0], $removedContainer[0]); // callback
            }
        },
        enable: function(position) {
            var $li = this.$tabs.slice(position - 1, position).parents('li:eq(0)'), o = this.options;
            $li.removeClass(o.disabledClass);
            if ($.browser.safari) { // fix disappearing tab after enabling in Safari... TODO check Safari 3
                $li.animate({ opacity: 1 }, 1, function() {
                    $li.css({ opacity: '' });
                });
            }
            o.enable(this.$tabs[position - 1], this.$containers[position - 1]); // callback
        },
        disable: function(position) {
            var $li = this.$tabs.slice(position - 1, position).parents('li:eq(0)'), o = this.options;
            if ($.browser.safari) { // fix opacity of tab after disabling in Safari... TODO check Safari 3
                $li.animate({ opacity: 0 }, 1, function() {
                   $li.css({ opacity: '' });
                });
            }
            $li.addClass(this.options.disabledClass);
            o.disable(this.$tabs[position - 1], this.$containers[position - 1]); // callback
        },
        click: function(position) {
            this.$tabs.slice(position - 1, position).trigger('click');
        },
        load: function(position, url, callback) {
            var self = this,
                o = this.options,
                $a = this.$tabs.slice(position - 1, position).addClass(o.loadingClass),
                $span = $('span', $a),
                text = $span.html();

            // shift arguments
            if (url && url.constructor == Function) {
                callback = url;
            }

            // set new URL
            if (url) {
                $.data($a[0], 'href', url);
            }

            // load
            if (o.spinner) {
                $span.html('<em>' + o.spinner + '</em>');
            }
            setTimeout(function() { // timeout is again required in IE, "wait" for id being restored
                $($a[0].hash).load(url, function() {
                    if (o.spinner) {
                        $span.html(text);
                    }
                    $a.removeClass(o.loadingClass);
                    // This callback is required because the switch has to take place after loading
                    // has completed.
                    if (callback && callback.constructor == Function) {
                        callback();
                    }
                    o.load(self.$tabs[position - 1], self.$containers[position - 1]); // callback
                });
            }, 0);
        }
    });

})(jQuery);


// *****************************************************************
// *** DONE WITH UI ************************************************
// *****************************************************************

window.$ui = jQuery.noConflict(true);

